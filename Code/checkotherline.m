function n = checkotherline(neighbours,i,tki,k,handles,n)
neighbours2 = handles.points.(pointname(neighbours(i))).neighbours;
    for j = 1 : numel(neighbours2)
        if (neighbours2(j) ~= k) && all(handles.beams.(beamname(neighbours(i),neighbours2(j))).t == tki)
            foundnew = false;
            if isfield(handles.points.(pointname(neighbours2(j))),'tvec')
            for p = 1 : numel(handles.points.(pointname(neighbours2(j))).tvec(1,:))
                if all(abs(handles.points.(pointname(neighbours2(j))).tvec(:,p)) == abs(tki))
                    n = n + 1;
                    foundnew = true;
                end
            end
            end
        if not(foundnew)
        n = n + checkotherline(neighbours2,j,tki,neighbours(i),handles,n);
        end
        end
    end
end