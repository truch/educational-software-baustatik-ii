function result = contpath(i,pnr,handles)
neighbours = handles.points.(pointname(i)).neighbours;
handles.points.(pointname(i)).notdone = [{}];
handles.paths.(pathname(pnr)) = [handles.paths.(pathname(pnr)) i];
for j = 1 : numel(neighbours)
    beamtodo = [{}];
    if not(isdone(beamname(i,neighbours(j)),handles))
        beamtodo = beamname(i,neighbours(j));
        break;
    end
end
if isempty(beamtodo) %No other beam at this point (This beam is the end of a path)
    if not(numel(handles.beamnames) == numel(handles.beamdone))
        for pnri = 1 : numel(fieldnames(handles.paths))
        for j = 1 : numel(handles.paths.(pathname(pnri))) %Search along the current path for beams that aren't done yet
            if not(isempty(handles.points.(pointname(handles.paths.(pathname(pnri))(j))).notdone))
                newstart = handles.paths.(pathname(pnri))(j);
                break;
            else newstart = [];
            end
        end
        if not(isempty(newstart))
            break;
        end
        end
        if not(isempty(newstart))
        handles = initpath(newstart,pnr + 1,handles);
        end
    end
elseif numel(handles.paths.(pathname(pnr)))>2 && handles.points.(pointname(i)).fix
    for j = 2 : numel(handles.paths.(pathname(pnr)))
        psifound = false;
        if handles.beams.(beamname(handles.paths.(pathname(pnr))(j-1),handles.paths.(pathname(pnr))(j))).haspsi %Kinematic boundary condition found 
        psifound = true;
        break;
        end
    end
    if psifound % Path is here finished
        if not(numel(handles.beamnames) == numel(handles.beamdone))
        for pnri = 1 : numel(fieldnames(handles.paths))
        for j = 1 : numel(handles.paths.(pathname(pnri))) %Search along the current path for beams that aren't done yet
            if not(isempty(handles.points.(pointname(handles.paths.(pathname(pnri))(j))).notdone))
                newstart = handles.paths.(pathname(pnri))(j);
                break;
            else newstart = [];
            end
        end
        if not(isempty(newstart))
            break;
        end
        end
        if not(isempty(newstart))
        handles = initpath(newstart,pnr + 1,handles);
        end
        end
    end
else
    for j = 1 : numel(neighbours) %Find first neighbour whose beam(i x neighbour) is not yet done
        if not(isdone(beamname(i,neighbours(j)),handles))
            k = neighbours(j);
            break;
        end
    end
    handles.beamdone = [handles.beamdone; {beamname(i,k)}];
    for j = 1 : numel(neighbours) %Remember all beams which are not done yet
        if not(isdone(beamname(i,neighbours(j)),handles))
            handles.points.(pointname(i)).notdone = [handles.points.(pointname(i)).notdone, {beamname(i,neighbours(j))}];
        end
    end
    handles = contpath(k,pnr,handles);
end
result = handles;
end