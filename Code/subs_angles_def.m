function result = subs_angles_def(solangles,names,handles)
for p = 1 : numel(handles.pointnames)
   current = handles.points.(char(handles.pointnames(p))).pointnr;
   neighbours = handles.points.(char(handles.pointnames(p))).neighbours;
   for n = 1 : numel(neighbours)
       for i = 1 : numel(names)
           syms (char(names(i)))
           handles.points.(pointname(current)).(pointname(neighbours(n))).M = ...
               subs(handles.points.(pointname(current)).(pointname(neighbours(n))).M,...
               eval(char(names(i))),solangles.(char(names(i))));
       end
   end
end
result = handles;
end