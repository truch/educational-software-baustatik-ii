function [p1, p2] = readpsi(psiname)
%psiname must be a string and must be of the format \mathrm{psi2x}_{3}
xpos = findstr('x',psiname);
ulinepos = findstr('_',psiname);
p1str = psiname(12 : xpos-1);
p2str = psiname(ulinepos + 2 : numel(psiname)-1);
p1 = str2double(p1str);
p2 = str2double(p2str);
end