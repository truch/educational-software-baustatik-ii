function h = dograph(Fig, h, type, M, V, sort)
% V = true --> plot shear force
% V = false --> don't plot shear force
%type --> 0: determined system; 1: unit force; 2: total moment
%sort: pointload / distributedload / moment

set(0, 'currentfigure', Fig);

distfromprev = 25;
distfromnext = 30;

height0 = 100;
height1 = 40;
height2 = 100;
heightpl = 0.04672897*h.screendim(4) + 29.626168;
width1 = 205;
width = 0.09328358*h.screendim(3) + 56.7164179;
widthpl = 0.05597015*h.screendim(3) + 64.0298507;

Mmax = [];
syms x
M(x) = M;
sh = h.S.sh;
sm = h.S.sm;
l = h.S.l;
h.Mloadequal0 = [];
h.Mmaxm = 0;
h.Mmaxpl_l = [];
h.Mmaxpl_r = [];

M000(x) = piecewise(x>=0 & x<=l, 0);

if type == 0
    Vatot = h.S.Vatotdet; Vbtot = h.S.Vbtotdet;
else
    Vatot = h.S.Vatotindet; Vbtot = h.S.Vbtotindet;
end


if type == 2, colorM = [0.2 0.5 1]; height = height2; elseif type == 0, colorM = [0.5 0.8 1]; height = height0; else, colorM = 'blue'; end

if V, if type == 2, colorV = [0.2 0.7 0.1]; else, colorV = [0.8 1 0.5]; end; end

if strcmp(sort, 'pointload') && ~h.superposition
    %plot point loads on static determinated system
    m = M(x);
            for i=1:numel(h.beam.F(:,1))
                M(x) = m(i);
                h.number = 1 + h.number;
                h.name = strcat(sort,num2str(h.number));    
                h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels');
                h.obj.(h.name).Position = [h.width/2-150/2, h.lastbottompos - distfromprev - heightpl, widthpl, heightpl];

                if rem(i,2) == 0
                    h.obj.(h.name).Position(1) = 3*h.width/4-2*150/3;
                elseif rem(i,2) == 1 && i<numel(m)
                    h.obj.(h.name).Position(1) = h.width/4-150/3;
                end
                
                if isequal(M(x), M000(x))
                    h.Mloadequal0 = [h.Mloadequal0; 1];
                else
                    h.Mloadequal0 = [h.Mloadequal0; 0];
                end
                
                if isequal(M(x), M000(x)) && h.load == 1
                    h.allfuncequal0 = true;
                elseif isequal(M(x), M000(x))
                    h.onefuncequal0 = true;
                else
                    h.allfuncequal0 = false;
                    h.onefuncequal0 = false;
                end
                
                if h.obj.(h.name).Position(2) < 10
                    set(h.(h.panel).Children,'Visible','Off');
                    set(h.(h.panel),'Visible','Off');
                    h.panelnr = h.panelnr + 1;
                    h.newpanelnr = h.panelnr;
                    h.panel = strcat('panel',num2str(h.panelnr));
                    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
                    h.lastnronpage = h.number - 1;
                    h.obj.(h.name).Parent = h.(h.panel);
                    h.obj.(h.name).Position(2) = h.height - distfromprev - h.obj.(h.name).Position(4);
                    h.lastbottompos = h.height;
                end
                
                h.(sort) = fplot(h.obj.(h.name),-M(x),[0,l],'color',colorM,'Linewidth',2);   
                hold on
                if h.S.N.end == 0 || type == 2
                	text(h.obj.(h.name), -1/20*l, 0, {strcat('$M_{pl',num2str(i),'}$')},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                else
                    text(h.obj.(h.name), -1/20*l, 0, {strcat('$M_{0,pl',num2str(i),'}$')},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                end
                if double(M(0)) ~= 0, plot(h.obj.(h.name), [0,0],[0,-M(0)],'color',colorM,'Linewidth',2); end
                if double(M(l)) ~= 0, plot(h.obj.(h.name), [l,l],[-M(l),0],'color',colorM,'Linewidth',2); end
                fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish
                
                if h.S.spl(i) ~= 0  && h.S.spl(i) ~= l && double(M(h.S.spl(i))) ~= 0
                        if double(M(h.S.spl(i))) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                        text(h.obj.(h.name), h.S.spl(i), -M(h.S.spl(i)), {' ',['$' checkfrac(M(h.S.spl(i))) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
                        Mmax =  double(M(h.S.spl(i)));
                end
                
                if double(M(0)) ~= 0
                    if double(M(0)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), 0, -M(0), {' ',['$' checkfrac(M(0)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);        
                    if isempty(Mmax), Mmax = double(M(0)); end
                end
                
                if double(M(l)) ~= 0
                    if double(M(l)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), l, -M(l), {' ',['$' checkfrac(M(l)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);        
                    if isempty(Mmax), Mmax =  double(M(l)); end                
                end
                    
                set(h.obj.(h.name),'xtick',[]);
                set(h.obj.(h.name),'ytick',[]);
                set(h.obj.(h.name),'xcolor',[1 1 1]);
                set(h.obj.(h.name),'ycolor',[1 1 1]);
                
                if rem(i,2) == 1
                    ylim1 = ylim;
                else
                    ylim2 = ylim;
                end
                
                if rem(i,2) == 0
                    norm2 = h.obj.(h.name).Position(4) / (ylim2(2) - ylim2(1));
                else
                     norm1 = h.obj.(h.name).Position(4) / (ylim1(2) - ylim1(1));
                end
                
                if rem(i,2) == 0
                    if norm1*ylim1(2) > norm2*ylim2(2)
                        h.obj.(h.name).Position(2) = h.obj.(strcat(sort,num2str(h.number-1))).Position(2) + (norm2*ylim2(1) - norm1*ylim1(1));
                        h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
                    elseif norm1*ylim1(2) < norm2*ylim2(2)
                        h.obj.(strcat(sort,num2str(h.number-1))).Position(2) = h.obj.(h.name).Position(2) - (norm2*ylim2(1) - norm1*ylim1(1));
                        h.lastbottompos = h.obj.(strcat(sort,num2str(h.number-1))).Position(2) - distfromnext;
                    else
                        h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
                    end    
                elseif i==numel(m)
                    h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
                end
                if h.interactive
                    set(h.obj.(h.name), 'Visible','Off');
                    set(h.obj.(h.name).Children, 'Visible','Off');
                end
                
                if h.Mloadequal0(i) == 1 || h.S.N.end == 0
                    h.intconst1pl_l = [h.intconst1pl_l; 0];
                    h.intconst1pl_r = [h.intconst1pl_r; 0];
                    h.intconst1pl = [h.intconst1pl; 0];
                    h.intconst2pl = [h.intconst2pl; 0];
                    h.intsol1pl = [h.intsol1pl; 0];
                    h.intsol2pl = [h.intsol2pl; 0];
                    h.Mmaxpl = [h.Mmaxpl; 0];
                    h.Mmaxpl_l = [h.Mmaxpl_l; 0];
                    h.Mmaxpl_r = [h.Mmaxpl_r; 0];
                    if i == 1, h.nrpointload = h.number; end
                elseif (sh > 0 && ((sh < h.S.spl(i) && h.S.rotationpoint(1) == 2 && h.S.N.end == 1) || (sh > h.S.spl(i) && h.S.rotationpoint(1) == 2 && h.S.N.end == 1)))
                    if sh < h.S.spl(i) && h.S.rotationpoint(1) == 2 && h.S.N.end == 1
                        h.Mmaxpl_l = [h.Mmaxpl_l; M(0)];
                        h.Mmaxpl_r = [h.Mmaxpl_r; Mmax];
                    else
                        h.Mmaxpl_l = [h.Mmaxpl_l; Mmax];
                        h.Mmaxpl_r = [h.Mmaxpl_r; M(l)];
                    end
                    h.intconst1pl_l = [h.intconst1pl_l; abs(int(M(x)/h.Mmaxpl_l(i)*h.S.M1(x)/h.S.M1max_l/h.S.spl(i),0,h.S.spl(i)))];
                    h.intconst1pl_r = [h.intconst1pl_r; abs(int(M(x)/h.Mmaxpl_r(i)*h.S.M1(x)/h.S.M1max_r/(l-h.S.spl(i)),h.S.spl(i),l))];
                    h.intconst1pl = [h.intconst1pl; 0];
                    if isnan(h.intconst1pl_r(i)), h.intconst1pl_r(i) = 0; end
                    h.intsol1pl = [h.intsol1pl; int(M(x)*h.S.M1(x),0,l) + h.S.cfsol10(i+2)];
                    h.intsol2pl = [h.intsol2pl; 0];
                    h.Mmaxpl = [h.Mmaxpl; 0];
                    if i == 1, h.nrpointload = h.number; end
                else
                    h.intconst1pl = [h.intconst1pl; abs(int(M(x)/h.S.M1max(1)/Mmax*h.S.M1(x)/l,0,l))];
                    if h.S.N.end == 2, h.intconst2pl = [h.intconst2pl; abs(int(M(x)/h.S.M1max(2)/Mmax*h.S.M2(x)/l,0,l))]; else, h.intconst2pl = [h.intconst2pl; 0]; end
                    h.intsol1pl = [h.intsol1pl; int(M(x)*h.S.M1(x),0,l) + h.S.cfsol10(i+2)];
                    if h.S.N.end == 2, h.intsol2pl = [h.intsol2pl; int(M(x)*h.S.M2(x),0,l)]; else h.intsol2pl = [h.intsol2pl; 0]; end
                    h.Mmaxpl = [h.Mmaxpl; Mmax];
                    if i == 1, h.nrpointload = h.number; end
                    h.intconst1pl_l = [h.intconst1pl_l; 0];
                    h.intconst1pl_r = [h.intconst1pl_r; 0];
                    h.Mmaxpl_l = [h.Mmaxpl_l; 0];
                    h.Mmaxpl_r = [h.Mmaxpl_r; 0];
                end
            end
else
        if type == 1    
            % plot unit force
            h.unitforcenr = 1 + h.unitforcenr;
            h.number = h.number + 1;
            h.name = strcat(sort,num2str(h.unitforcenr));
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','position', [h.width/2-width/2, h.lastbottompos - distfromprev - height1, width1, height1]);
            if h.obj.(h.name).Position(2) < 10
                set(h.(h.panel).Children,'Visible','Off');
                set(h.(h.panel),'Visible','Off');
                h.panelnr = h.panelnr + 1;
                h.newpanelnr = h.panelnr;
                h.panel = strcat('panel',num2str(h.panelnr));
                h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
                h.lastnronpage = h.number - 1;
                h.obj.(h.name).Parent = h.(h.panel);
                h.obj.(h.name).Position(2) = h.height - distfromprev - h.obj.(h.name).Position(4);
            end
            h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;

            h.(sort) = fplot(h.obj.(h.name),-M(x),[0,l],'color',colorM,'Linewidth',2);   
            hold on
            if h.unitforcenr == 1
                text(h.obj.(h.name), -1/20*l, 0, {strcat('$',h.S.M_1,'$')},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
            else
                text(h.obj.(h.name), -1/20*l, 0, {'$M_{2}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
            end
            if double(M(0)) ~= 0, plot(h.obj.(h.name), [0,0],[0,-M(0)],'color',colorM,'Linewidth',2); end
            if double(M(l)) ~= 0, plot(h.obj.(h.name), [l,l],[-M(l),0],'color',colorM,'Linewidth',2); end
            fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish

            if double(M(0)) ~= 0
                if double(M(0)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                     text(h.obj.(h.name), 0, -M(0), {strcat('$',checkfrac(M(0)),'$')},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment',alignment,'Color','blue');
            end
            if double(M(l)) ~= 0
                if double(M(l)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                     text(h.obj.(h.name), l, -M(l), {strcat('$',checkfrac(M(l)),'$')},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment,'Color','blue');
            end

            set(h.obj.(h.name),'xtick',[]);
            set(h.obj.(h.name),'ytick',[]);
            set(h.obj.(h.name),'xcolor',[1 1 1]);
            set(h.obj.(h.name),'ycolor',[1 1 1]);
            
            h.intconst11 = abs(int(M(x)/h.S.M1max(1)^2*h.S.M1(x)/l,0,l));
            h.intconst11_l = abs(int(M(x)/M(0)^2*h.S.M1(x)/sh,0,sh));
            h.M11max_l = M(0);
            h.intconst11_r = abs(int(M(x)/M(l)^2*h.S.M1(x)/(l-sh),sh,l));
            h.M11max_r = M(l);
            if h.S.N.end == 2, h.intconst22 = abs(int(M(x)/h.S.M1max(2)^2*h.S.M2(x)/l,0,l)); else, h.intconst22 = 0; end
            
        else
            if isequal(M(x), M000(x)) && h.load == 1
                h.allfuncequal0 = true;
            elseif isequal(M(x), M000(x))
                h.onefuncequal0 = true;
            else
                h.allfuncequal0 = false;
                h.onefuncequal0 = false;
            end
                
            h.number = 1 + h.number;
            h.name = strcat(sort,num2str(h.number));
            if V, positionM = [h.width/4-width/2+10, h.lastbottompos - distfromprev - height, width, height]; else, positionM = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height]; end
            if positionM(2) < 10
                set(h.(h.panel).Children,'Visible','Off');
                set(h.(h.panel),'Visible','Off');
                h.panelnr = h.panelnr + 1;
                h.newpanelnr = h.panelnr;
                h.panel = strcat('panel',num2str(h.panelnr));
                h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
                h.lastnronpage = h.number - 1;
                h.obj.(h.name).Parent = h.(h.panel);
                positionM(2) = h.height - distfromprev - positionM(4);
            end
            h.lastbottompos = positionM(2) - distfromnext;

                h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','position', positionM);
                h.(sort) = fplot(h.obj.(h.name),-M,[0,l],'color',colorM,'Linewidth',2);   
                hold on
                if double(M(0)) ~= 0, plot(h.obj.(h.name), [0,0],[0,-M(0)],'color',colorM,'Linewidth',2); end
                if double(M(l)) ~= 0, plot(h.obj.(h.name), [l,l],[-M(l),0],'color',colorM,'Linewidth',2); end
                fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish
                if h.S.N.end == 0 || type == 2
                    if strcmp(sort, 'pointload') && h.load > 1
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{pl}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    elseif strcmp(sort, 'distributedload') && h.load > 1
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{dl}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    elseif h.load > 1
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{m}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    else
                        text(h.obj.(h.name), -1/20*l, 0, {'$M$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    end
                else
                    if strcmp(sort, 'pointload') && h.load > 1
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{0,pl}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    elseif strcmp(sort, 'distributedload') && h.load > 1
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{0,dl}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    elseif h.load > 1
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{0,m}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    else
                        text(h.obj.(h.name), -1/20*l, 0, {'$M_{0}$'},'Interpreter','latex','HorizontalAlignment','right','FontSize',12, 'FontName','Calibri');
                    end
                end
                
                if ~strcmp(sort, 'pointload') && ~strcmp(sort,'moment')
                %Value at a
                if h.S.a ~= 0 && double(M(h.S.a)) ~= 0
                    if double(M(h.S.a)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), h.S.a, -M(h.S.a), {['$' checkfrac(M(h.S.a)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment',alignment);
                end

                %Value at b
                if h.S.b ~= 0 && h.S.b ~= l && double(M(h.S.b)) ~= 0
                    if double(M(h.S.b)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), h.S.b, -M(h.S.b), {['$' checkfrac(M(h.S.b)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
                end
                
                %values at continuous maxima
                xmaxmin = double(solve(diff(M(x)) == 0, x));
                for i = 1 : numel(xmaxmin)
                    dd(x) = diff(diff(M));
                    sign = dd(xmaxmin(i));
                    if xmaxmin(i) ~= 0   && xmaxmin(i) ~= l && double(M(xmaxmin(i))) ~= 0
                        if sign >= 0, alignment = 'bottom'; else, alignment = 'top'; end
                                text(h.obj.(h.name), xmaxmin(i), -M(xmaxmin(i)), {' ',['$' checkfrac(M(xmaxmin(i))) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
                    end
                end
                
                if ~isempty(xmaxmin)
                    if numel(xmaxmin) > 1
                        maxmoment = [abs(M(h.S.a)), abs(M(h.S.b)), abs(M(xmaxmin(1))), abs(M(xmaxmin(2)))];
                    else
                        maxmoment = [abs(M(h.S.a)), abs(M(h.S.b)), abs(M(xmaxmin(1)))];
                    end
                    maxM = max(maxmoment);
                    whichM = find(maxmoment == maxM,1,'last');

                    if whichM == 1
                        Mmax =  M(h.S.a);
                    elseif whichM == 2
                        Mmax =   M(h.S.b);
                    else
                        if numel(xmaxmin) > 1
                            Mmax = max(M(xmaxmin(1)), M(xmaxmin(2)));
                        else
                            Mmax =   M(xmaxmin(1));
                        end
                    end
                end
                
                end
                
                if ~strcmp(sort, 'distributedload') && ~strcmp(sort,'pointload')
                %Value at sm
                        if double(M(h.S.sm)) < 0 && h.S.sm ~= 0 && h.S.sm ~= l
                            if double(-M(h.S.sm) - h.S.Mvalue) ~= 0
                                text(h.obj.(h.name), h.S.sm, -M(h.S.sm) - h.S.Mvalue, {' ',['$' checkfrac(M(h.S.sm) + h.S.Mvalue) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top');
                            elseif double(-M(h.S.sm)) ~= 0
                                text(h.obj.(h.name), h.S.sm, -M(h.S.sm), {' ',['$' checkfrac(M(h.S.sm)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom');                
                            end
                        elseif double(M(h.S.sm)) > 0 && h.S.sm ~= 0 && h.S.sm ~= l
                            if double(-M(h.S.sm)) ~= 0
                                text(h.obj.(h.name), h.S.sm, -M(h.S.sm), {' ',['$' checkfrac(M(h.S.sm)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top');                
                            elseif double(-M(h.S.sm) - h.S.Mvalue) ~= 0
                                text(h.obj.(h.name), h.S.sm, -M(h.S.sm) - h.S.Mvalue, {' ',['$' checkfrac(M(h.S.sm) + h.S.Mvalue) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom');                
                            end
                        end
                        h.Mmaxm_lj = M(h.S.sm) + h.S.Mvalue;
                        h.Mmaxm_rj = M(h.S.sm);
                        Mmax =  max(abs(M(h.S.sm) + h.S.Mvalue), abs(M(h.S.sm)));
                        if double(Mmax) == double(abs(M(h.S.sm) + h.S.Mvalue))
                            Mmax = M(h.S.sm) + h.S.Mvalue;
                        else
                            Mmax = M(h.S.sm);
                        end
                end
                
                if ~strcmp(sort, 'distributedload') && ~strcmp(sort,'moment')
                %Value at pointloads
                    for i = 1 : numel(h.S.spl)
                        if h.S.spl(i) ~= 0  && h.S.spl(i) ~= l && double(M(h.S.spl(i))) ~= 0
                            if double(M(h.S.spl(i))) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                                text(h.obj.(h.name), h.S.spl(i), -double(M(h.S.spl(i))), {' ',['$' checkfrac(M(h.S.spl(i))) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
                                Mmax =  double(M(h.S.spl(i)));          
                        end
                    end
                end

                if double(M(0)) ~= 0
                    if double(M(0)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                        text(h.obj.(h.name), 0, -double(M(0)), {['$' checkfrac(M(0)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
                        h.Mmaxm_square = double(M(0));
                        if isempty(Mmax), Mmax = double(M(0)); end                
                end
                if double(M(l)) ~= 0
                    if double(M(l)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                        text(h.obj.(h.name), l, -double(M(l)), {['$' checkfrac(M(l)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
                        if isempty(Mmax), Mmax = double(M(l)); end                
                end

            set(h.obj.(h.name),'xtick',[]);
            set(h.obj.(h.name),'ytick',[]);
            set(h.obj.(h.name),'xcolor',[1 1 1]);
            set(h.obj.(h.name),'ycolor',[1 1 1]);
            ylimM = ylim;
            
            set(h.obj.(h.name), 'Visible','Off');
            set(h.obj.(h.name).Children, 'Visible','Off');
            
            M000(x) = piecewise(x>=0 & x<=l, 0);

            if type ~= 2 && ~h.superposition
                if strcmp(sort,'distributedload') 
                    if isequal(M(x), M000(x))
                        h.Mmaxdl = 0;
                        h.Mmaxdlx = 0;
                    elseif sh > 0 && ((sh <= h.S.a && sh <= h.S.b && h.S.rotationpoint(1) == 2 && h.S.N.end == 1) || (sh >= h.S.a && sh >= h.S.b && h.S.rotationpoint(1) == 1 && h.S.N.end == 1)) && ((h.beam.q1(1)>=0 && h.beam.q2(1)>=0) || (h.beam.q1(1)<=0 && h.beam.q2(1)<=0))
                        if sh < h.S.a && sh < h.S.b && h.S.rotationpoint(1) == 2 && h.S.N.end == 1
                            h.Mmaxdl_l = M(0);
                            h.Mmaxdl_r = M(xmaxmin);
                            Mx = [0, xmaxmin];
                        elseif sh < h.S.a && sh < h.S.b && h.S.rotationpoint(1) == 1 && h.S.N.end == 1
                            h.Mmaxdl_l = M(xmaxmin);
                            h.Mmaxdl_r = M(l);
                            Mx = [xmaxmin, l];
                        else
                            h.Mmaxdl_l = M(0);
                            h.Mmaxdl_r = M(xmaxmin);
                            Mx = [0, xmaxmin];
                        end
                        Mext = [double(h.Mmaxdl_l), double(h.Mmaxdl_r)];
                        M_max = double(Mext(Mext == max(Mext)));
                        M_min = double(Mext(Mext == min(Mext)));
                        if abs(M_max) > abs(M_min), Mmax = M_max; else, Mmax = M_min; end
                        if numel(Mmax) > 1, Mmax = 0; end
                        h.Mmaxdl = Mmax;   
                        h.Mmaxdlx = Mx(double(Mext) == double(Mmax));
                        h.intconst1dl_l = abs(int(M(x)/h.Mmaxdl_l*h.S.M1(x)/h.S.M1(0)/sh,0,sh));
                        h.intconst1dl_r = abs(int(M(x)/h.Mmaxdl_r*h.S.M1(x)/h.S.M1(l)/(l-sh),sh,l));
                    elseif sh > 0 && (sh >= h.S.a && sh >= h.S.b && h.S.rotationpoint(1) == 2 && h.S.N.end == 1) && ((h.beam.q1(1)>=0 && h.beam.q2(1)>=0) || (h.beam.q1(1)<=0 && h.beam.q2(1)<=0))
                        h.S.M1max(1) = h.S.M1(0);
                        h.Mmaxdl = M(0);
                        h.Mmaxdlx = 0;
                        h.intconst1dl = abs(int(M(x)/h.Mmaxdl*h.S.M1(x)/h.S.M1max(1)/l,0,l));
                    elseif numel(xmaxmin) > 1
                        Mext = [double(M(xmaxmin(1))), double(M(xmaxmin(2)))];
                        Mx = [xmaxmin(1), xmaxmin(2)];
                        M_max = double(Mext(Mext == max(Mext)));
                        M_min = double(Mext(Mext == min(Mext)));
                        if abs(M_max) == abs(M_min), Mmax = M_max; elseif abs(M_max) > abs(M_min), Mmax = M_max; else, Mmax = M_min; end
                        if numel(Mmax) > 1, Mmax = 0; end
                        h.Mmaxdl = Mmax;  
                        h.Mmaxdlx = Mx(double(Mext) == double(Mmax));
                        h.intconst1dl = double(abs(int(M(x)/h.S.M1max(1)/Mmax*h.S.M1(x)/l,0,l)));
                        if h.S.N.end == 2, h.intconst2dl = double(abs(int(M(x)/h.S.M1max(2)/Mmax*h.S.M2(x)/l,0,l))); else, h.intconst2dl = 0; end                        
                    else
                        Mext = [M(0), M(l), M(xmaxmin)];
                        Mx = [0, l, xmaxmin];
                        M_max = double(Mext(Mext == max(Mext)));
                        M_min = double(Mext(Mext == min(Mext)));
                        if abs(M_max) > abs(M_min), Mmax = M_max; else, Mmax = M_min; end
                        if numel(Mmax) > 1, Mmax = 0; end
                        h.Mmaxdl = Mmax;
                        h.Mmaxdlx = Mx(double(Mext) == double(Mmax));
                        if all(Mmax) == 0
                            h.intconst1dl = 0;
                            h.intconst2dl = 0;
                        else
                            h.intconst1dl = abs(int(M(x)/h.S.M1max(1)/Mmax*h.S.M1(x)/l,0,l));
                            if h.S.N.end == 2, h.intconst2dl = abs(int(M(x)/h.S.M1max(2)/Mmax*h.S.M2(x)/l,0,l)); else, h.intconst2dl = 0; end
                        end
                    end
                    h.intsol1dl = int(M(x)*h.S.M1(x),0,l) + h.S.cfsol10(1);
                    if h.S.N.end == 2, h.intsol2dl = int(M(x)*h.S.M2(x),0,l); else, h.intsol2dl = 0; end
                    if numel(Mmax) > 1, Mmax = 0; end
                    if isempty(h.Mmaxdl), h.Mmaxdl = Mmax; h.Mmaxdlx = 0; end
                elseif strcmp(sort,'moment') && h.S.N.end > 0
                        if h.S.sm == 0 && h.S.N.end > 0
                            if abs(h.S.M1(0)) > abs(h.S.M1(l)), h.S.M1max_l = h.S.M1(0); else, h.S.M1max_lr = h.S.M1(l); end
                            if abs(h.S.M2(0)) > abs(h.S.M2(l)), h.S.M2max_l = h.S.M2(0); else, h.S.M2max_l = h.S.M2(l); end
                            h.Mmaxm = M(0);
                            h.intconst1m = abs(int(M(x)/h.Mmaxm*h.S.M1(x)/h.S.M1max_l/l,0,l));
                            h.intconst2m = abs(int(M(x)/h.Mmaxm*h.S.M2(x)/h.S.M2max_l/l,0,l));
                        elseif h.S.sm == l && h.S.N.end > 0
                            if abs(h.S.M1(0)) > abs(h.S.M1(l)), h.S.M1max_r = h.S.M1(0); else, h.S.M1max_r = h.S.M1(l); end
                            if abs(h.S.M2(0)) > abs(h.S.M2(l)), h.S.M2max_r = h.S.M2(0); else, h.S.M2max_r = h.S.M2(l); end
                            h.S.M2max_r = max(h.S.M2(0), h.S.M2(l));
                            h.Mmaxm = M(l);
                            h.intconst1m = abs(int(M(x)/h.Mmaxm*h.S.M1(x)/h.S.M1max_r/l,0,l));
                            h.intconst2m = abs(int(M(x)/h.Mmaxm*h.S.M2(x)/h.S.M2max_r/l,0,l));
                        elseif h.S.rotationpoint(1) == 2 && sh == -1
                            h.intconst1m_lj = abs(int(M(x)/h.Mmaxm_lj*h.S.M1(x)/h.S.M1max_sm/sm,0,sm));
                            h.intconst1m_rj = abs(int(M(x)/h.Mmaxm_rj*h.S.M1(x)/h.S.M1max(1)/(l-sm),sm,l));
                            if h.S.N.end == 2, h.intconst2m_lj = abs(int(M(x)/h.Mmaxm_lj*h.S.M1(x)/h.S.M2max_sm/sm,0,sm)); else, h.intconst2m_lj = 0; end
                            if h.S.N.end == 2, h.intconst2m_rj = abs(int(M(x)/h.S.M1max(2)/h.Mmaxm_rj*h.S.M2(x)/(l-sm),sm,l)); else, h.intconst2m_rj = 0; end
                        elseif sh == -1
                            h.intconst1m_lj = abs(int(M(x)/h.Mmaxm_lj*h.S.M1(x)/h.S.M1max_l/(sm),0,sm));
                            h.intconst1m_rj = abs(int(M(x)/h.Mmaxm_rj*h.S.M1(x)/h.S.M1max_sm/(l-sm),sm,l));
                            h.intconst2m_lj = abs(int(M(x)/h.Mmaxm_lj*h.S.M2(x)/h.S.M2max_sm/(sm),0,sm));
                            h.intconst2m_rj = abs(int(M(x)/h.Mmaxm_rj*h.S.M2(x)/h.S.M2max_r/(l-sm),sm,l));
                        elseif sm > sh
                            h.Mmaxm_l = M(0);
                            h.intconst1m_l = abs(int(M(x)/h.Mmaxm_l*h.S.M1(x)/h.S.M1max_l/sh,0,sh));
                            h.intconst1m_lj = abs(int(M(x)/h.Mmaxm_lj*h.S.M1(x)/h.S.M1max_sm/(sm-sh),sh,sm));
                            h.intconst1m_rj = abs(int(M(x)/h.Mmaxm_rj*h.S.M1(x)/h.S.M1max_r/(l-sm),sm,l));
                        else
                            h.intconst1m_square = abs(int(M(x)/h.Mmaxm_square*h.S.M1(x)/h.S.M1max_l/sm,0,sm));
                        end
                    h.intsol1m = int(M(x)*h.S.M1(x),0,l) + h.S.cfsol10(2);
                    if h.S.N.end == 2, h.intsol2m = int(M(x)*h.S.M2(x),0,l); else, h.intsol2m = 0; end
                end
            end

                if V
                    h.number = 1 + h.number;
                    h.name = strcat(sort,num2str(h.number));

                    positionV = positionM;
                    positionV(1) = 3/4*h.width-positionM(3)/2-10;
                    h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','position', positionV);
                    V = vpa(diff(M));
                    h.(sort) = fplot(h.obj.(h.name),-V(x),[0,l],'color',colorV,'Linewidth',2);
                    hold on
                    plot([0,0],[0,-Vatot],'color',colorV,'Linewidth',2);
                    plot([l,l],[Vbtot,0],'color',colorV,'Linewidth',2);            
                    fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish

                    if Vatot ~= 0
                        if Vatot < 0, alignment = 'bottom'; else, alignment = 'top'; end
                            text(h.obj.(h.name), 0, -Vatot, {['$' checkfrac(Vatot) '\ ' h.unitV '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
                    end
                    if Vbtot ~= 0
                        if Vbtot > 0, alignment = 'bottom'; else, alignment = 'top'; end
                            text(h.obj.(h.name), l, Vbtot, {['$' checkfrac(-Vbtot) '\ ' h.unitV '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
                    end

                    text(h.obj.(h.name), -1/20*l, 0, {'V'},'Interpreter','latex','HorizontalAlignment','right','FontSize',14);

                ylimV = ylim;

                set(h.obj.(h.name),'xtick',[]);
                set(h.obj.(h.name),'ytick',[]);
                set(h.obj.(h.name),'xcolor',[1 1 1]);
                set(h.obj.(h.name),'ycolor',[1 1 1]);
                
                if h.interactive
                set(h.obj.(h.name), 'Visible','Off');
                set(h.obj.(h.name).Children, 'Visible','Off');
                end

                normV = h.obj.(h.name).Position(4) / (ylimV(2) - ylimV(1));
                normM = h.obj.(strcat(sort,num2str(h.number-1))).Position(4) / (ylimM(2) - ylimM(1));
                
                if normM*ylimM(2) > normV*ylimV(2)
                    h.obj.(h.name).Position(2) = h.obj.(strcat(sort,num2str(h.number-1))).Position(2) + (normV*ylimV(1) - normM*ylimM(1));
                    h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
                else
                    h.obj.(strcat(sort,num2str(h.number-1))).Position(2) = h.obj.(strcat(sort,num2str(h.number-1))).Position(2) - (normV*ylimV(1) - normM*ylimM(1));
                    h.lastbottompos = h.obj.(strcat(sort,num2str(h.number-1))).Position(2) - distfromnext;
                end    
                
                if (h.lastbottompos + 100 - h.obj.(h.name).Position(4) < 10) && ~h.panelofsuperposition
                    set(h.(h.panel).Children,'Visible','Off');
                    set(h.(h.panel),'Visible','Off');
                    h.panelnr = h.panelnr + 1;
                    h.newpanelnr = h.panelnr;
                    h.panel = strcat('panel',num2str(h.panelnr));
                    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
                    h.lastnronpage = h.number - 1;
                    h.obj.(strcat(sort,num2str(h.number-1))).Parent = h.(h.panel);
                    h.obj.(h.name).Parent = h.(h.panel);
                    fields = fieldnames(h.obj);
                    if contains(char(fields(h.number-2)),'text')
                        if strcmp(h.obj.(strcat('text',num2str(h.number-2))).Tag,'yes')
                            h.obj.(strcat('text',num2str(h.number-2))).Parent = h.(h.panel);
                            h.obj.(strcat('text',num2str(h.number-2))).Position(2) = h.height - distfromprev - h.obj.(strcat('text',num2str(h.number-2))).Position(4);
                            h.obj.(strcat(sort,num2str(h.number-1))).Position(2) = h.obj.(strcat('text',num2str(h.number-2))).Position(2) - distfromprev - positionM(4); 
                            h.obj.(h.name).Position(2) = h.obj.(strcat('text',num2str(h.number-2))).Position(2) - distfromprev - positionV(4); 
                            h.lastbottompos = h.obj.(strcat(sort,num2str(h.number-1))).Position(2) - distfromnext;
                        end
                    end
                end
                
                end
        end
        
        h.superposition = false;
        
if h.interactive
    pause(h.pause);
else
    pause(0.2);
end
end
end