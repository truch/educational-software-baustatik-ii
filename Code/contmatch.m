function result = contmatch(pnr,element,handles)
previous = handles.paths.(pathname(pnr))(element-1);
current = handles.paths.(pathname(pnr))(element);
currentbeamname = beamname(previous,current);
if handles.beams.(currentbeamname).haspsi
    syms (psiname(handles.beams.(currentbeamname)))
    handles.beams.(currentbeamname).psisym = eval(psiname(handles.beams.(currentbeamname)));
    if isfield(handles.points.(pointname(current)),'v')
        handles.points.(pointname(current)).v_former = handles.points.(pointname(current)).v;
    end
    if current > previous
        factor = 1;
    else
        factor =-1;
    end
    handles.points.(pointname(current)).v = factor*handles.beams.(currentbeamname).n*...
                                            handles.beams.(currentbeamname).l*...
                                            handles.beams.(currentbeamname).psisym + ...
                                            handles.points.(pointname(previous)).v;
else
    handles.points.(pointname(current)).v = handles.points.(pointname(previous)).v;
end
if element == numel(handles.paths.(pathname(pnr)))
    handles = kinematic_equilibrium(pnr,handles);
    if not(pnr == numel(fieldnames(handles.paths)))
        pnr = pnr + 1;
        handles = initmatch(pnr, handles);
    end
else
    element = element + 1;
    handles = contmatch(pnr,element,handles);
end

result = handles;
end