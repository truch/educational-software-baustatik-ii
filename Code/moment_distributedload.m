function [M0, Vatot, Vbtot, diagrams] = moment_distributedload(l,a,b,qa,qb,knottype_a,knottype_b,M0, diagrams)

syms x qx(x) R0(x) R1(x) R2(x) xs0(x) xs1(x) xs2(x) 

     qx(x) = (qb-qa)/(b-a)*(x-a)+qa; %load at position x
     
     if not((sign(qa)>=0 && sign(qb)>=0) || (sign(qa)<=0 && sign(qb)<=0))
              c = solve( qx(x) == 0 ,x); %point on beam where the load turns 0 (changes sign)
              
              R0(x) = 0;
              R1(x) = ((-qa/(c-a)*(x-a)+qa)+qa)/2*(x-a); %resultant of load at position x from a to c
              R2(x) = ((qb/(b-c)*(x-c)))/2*(x-c); %resultant of load at position x from c to b
              
              xs0(x) = 0;
              xs1(x) = (x-a)/3 * (qa+2*qx(x))/(qa+qx(x)) + a; %center of mass of load at position x from a to c
              xs2(x) = 2/3*(x-c) + c; %center of mass of load at position x from c to b
     else          
             c = 0;
             R0(x) = (((qb-qa)/(b-a)*(x-a)+qa)+qa)/2*(x-a); %resultant of load
             xs0(x) = (x-a)/3 * (qa+2*qx(x))/(qa+qx(x)) + a;  %center of mass of load
             R1(x) = 0;
             xs1(x) = 0;
             R2(x) = 0;
             xs2(x) = 0;
     end

     if strcmp(knottype_b,'free')
         Va = R0(b) + R1(c) + R2(b);
         Mfixed = -R0(b)*xs0(b) - R1(c)*xs1(c) - R2(b)*xs2(b);
     elseif strcmp(knottype_a,'free')
         Va = 0;
         Mfixed = 0;
     else
         Va = R0(b)*(l-xs0(b))/l + (R1(c)* (l-xs1(c)) + R2(b)*(l-xs2(b)))/l;
         Mfixed = 0;
     end
     
     Vatot = Va;
     Vbtot = R0(b) + R1(c) + R2(b) - Va;
     
     M01(x) = piecewise(x>=0 & x<a, Va*x, x>=a & x<=l, 0);
     if not((sign(qa)>=0 && sign(qb)>=0) || (sign(qa)<=0 && sign(qb)<=0))
        M02(x) = piecewise(x>=0 & x<a, 0, x>=a & x<c, Va*x - R0(x)*(x-xs0(x)) - R1(x)*(x-xs1(x)), x>=c & x<=l, 0);
        M03(x) = piecewise(x>=0 & x<c, 0, x>=c & x<b, Va*x - R0(x)*(x-xs0(x)) - R1(c)*(x-xs1(c)) - R2(x)*(x-xs2(x)), x>=b & x<=l, 0);
     else
         M02(x) = piecewise(x>=0 & x<a, 0, x>=a & x<b, Va*x - R0(x)*(x-xs0(x)), x>=b & x<=l, 0);
         M03(x) = piecewise(x>=0 & x<=l, 0);
     end
     M04(x) = piecewise(x>=0 & x<b, 0, x>=b & x<=l, Va*x - R0(b)*(x-xs0(b)) - R1(c)*(x-xs1(c)) - R2(b)*(x-xs2(b)));

     M00(x) = piecewise(x>=0 & x<=l, Mfixed) + M01(x) + M02(x) + M03(x) + M04(x);
     diagrams.dl = diagrams.dl + M00(x);

     M0(x) = M00(x) + M0(x);
     
end