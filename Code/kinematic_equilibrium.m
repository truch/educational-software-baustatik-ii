function result = kinematic_equilibrium(pnr,handles)
element = numel(handles.paths.(pathname(pnr))); 
current = handles.paths.(pathname(pnr))(element);
previous = handles.paths.(pathname(pnr))(element-1);
currentbeamname = beamname(previous,current);

if handles.points.(pointname(current)).fix
    handles.psieqn.(eqname(pnr)) = (handles.points.(pointname(current)).v == [0;0]);
elseif strcmp(handles.points.(pointname(current)).knottype,'free')
    %do nothing, no kinematic boundary condition at a free end...
elseif isfield(handles.points.(pointname(current)),'tvec') && numel(handles.points.(pointname(current)).tvec(1,:)) == 1 && numel(handles.points.(pointname(current)).neighbours) == 1
    handles.psieqn.(eqname(pnr)) = (dot(handles.points.(pointname(current)).v,handles.points.(pointname(current)).tvec(:,1)) == 0);
    
% elseif not(isempty(handles.points.(pointname(current)).tvec)) && numel(handles.points.(pointname(current)).tvec(1,:)) == 1
%     if handles.beams.(currentbeamname).haspsi
%         joint = joinlines(handles.points.(pointname(current)).pvec,...
%                       handles.points.(pointname(current)).tvec,...
%                       handles.points.(pointname(previous)).r,...
%                       rot90vec(handles.points.(pointname(previous)).v));
%         l_mom = norm(joint-handles.points.(pointname(current)).r);
%         if numel(handles.paths.(pathname(pnr))) > 2
%             factor = factor2plus(pnr,handles);
%             v_bc = factor*l_mom*rot90vec(handles.points.(pointname(current)).tvec)*handles.beams.(currentbeamname).psisym;
%         else
%             v_bc = factorsingle(pnr,handles)*l_mom*rot90vec(handles.points.(pointname(current)).tvec)*handles.beams.(currentbeamname).psisym;
%         end
%         handles.psieqn.(eqname(pnr)) = (handles.points.(pointname(current)).v == v_bc);
%     else
%         v_bc = handles.points.(pointname(1)).v;
%         handles.psieqn.(eqname(pnr)) = (handles.points.(pointname(current)).v == v_bc);
%     end

else %no external kinematic boundary condition
    if numel(handles.points.(pointname(current)).neighbours) > 1 %moveable connection
        handles.psieqn.(eqname(pnr)) = (handles.points.(pointname(current)).v == handles.points.(pointname(current)).v_former);
    else
        handles.psieqn.(eqname(pnr)) = [];
    end
end
result = handles;
end