function h = equation(Fig, h, str)

set(0, 'currentfigure', Fig);

    h.posforequ = h.lastbottompos;
    h.number = 1 + h.number;
    h.name = strcat('equation',num2str(h.number));    
    h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels');
    hold on
    if contains(str, 'underbrace')
        maxlim = 200;
    else
        maxlim = 100;
    end    
    width = h.width - 40;
    numberofchar = numel(str);
%     numberofcharnew = numberofchar;
%     if count(str, '_{'), numberofcharnew = numberofcharnew - 4*count(str, '_{'); end
%     if count(str, 'underbrace'), numberofcharnew = numberofcharnew - 10*count(str, 'underbrace'); end
%     if count(str, 'frac'), numberofcharnew = numberofcharnew - 2*count(str, 'frac'); end
%     if count(str, 'mathrm'), numberofcharnew = numberofcharnew - 5*count(str, 'mathrm'); end
%     if count(str, '\left'), numberofcharnew = numberofcharnew - 2*count(str, '\left'); end
%     if count(str, '\right'), numberofcharnew = numberofcharnew - 3*count(str, '\right'); end
    numberoftexts = ceil(numberofchar/maxlim);
    numberoftexts_def = 1;
    startpos = 1;
    partpos = 0;
    rounded = 0;
    curly = 0;
    for i = 1 : numberoftexts
        if numel(str) > startpos + maxlim-1, stop = startpos + maxlim-1;
        else stop = numel(str);
        end
        foundnew = false;
        for j = startpos : stop
            if strcmp(str(j),'{'),curly = curly + 1;
            elseif strcmp(str(j),'('),rounded = rounded + 1;
            elseif strcmp(str(j),'}'),curly = curly - 1;
            elseif strcmp(str(j),')'),rounded = rounded - 1;
            end
        end
        for j = startpos + maxlim : numel(str)
            foundnew = false;
            if strcmp(str(j),'{'),curly = curly + 1;
            elseif strcmp(str(j),'('),rounded = rounded + 1;
            elseif strcmp(str(j),'}'),curly = curly - 1;
            elseif strcmp(str(j),')'),rounded = rounded - 1;
            end
        if (strcmp(str(j),'+') || strcmp(str(j),'-') || j == numberofchar || strcmp(str(j),'='))  && rounded == 0 && curly == 0
            partpos = j;
            foundnew = true;
            break;
        end
        end
        if not(isempty(str(startpos : partpos))) && foundnew
            if partpos == numberofchar, break; end
            numberoftexts_def = numberoftexts_def + 1;
            startpos = partpos + 1;
        else
            if numberoftexts_def == 0
                numberoftexts_def = 1; 
            end
        end
    end
    
    if contains(str, 'underbrace')
        height = 60*(numberoftexts_def);
    else
        height = 45*(numberoftexts_def);
    end
    h.heightequ = height;
    distfromprev = 10;
    distfromnext = 10;
    h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
    h.obj.(h.name).XLim = [0 1];
    h.obj.(h.name).YLim = [0 numberoftexts_def];
    if numberoftexts_def > 1
    startpos = 1;
    curly = 0;
    rounded = 0;
    for i = 1 : numberoftexts
        if numel(str) > startpos + maxlim-1, stop = startpos + maxlim-1;
        else stop = numel(str); partpos = stop;
        end
        for j = startpos : stop
            if strcmp(str(j),'{'),curly = curly + 1;
            elseif strcmp(str(j),'('),rounded = rounded + 1;
            elseif strcmp(str(j),'}'),curly = curly - 1;
            elseif strcmp(str(j),')'),rounded = rounded - 1;
            end
        end
        for j = startpos + maxlim : numel(str)
        if strcmp(str(j),'{'),curly = curly + 1;
        elseif strcmp(str(j),'('),rounded = rounded + 1;
        elseif strcmp(str(j),'}'),curly = curly - 1;
        elseif strcmp(str(j),')'),rounded = rounded - 1;
        end
        if (strcmp(str(j),'+') || strcmp(str(j),'-') || j == numberofchar || strcmp(str(j),'=')) && rounded == 0 && curly == 0
            partpos = j;
            break;
        end
        end
        if not(isempty(str(startpos : partpos)))
            text(0.5, numberoftexts_def + .5-i, {['$' str(startpos : partpos) '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment','middle','FontSize',12);
            startpos = partpos + 1;
        end
    end
    else
            text(0.5, 0.5, {['$' str '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment','middle','FontSize',12);
    end
    
    
    set(h.obj.(h.name),'xtick',[]);
    set(h.obj.(h.name),'ytick',[]);
    set(h.obj.(h.name),'xcolor',[1 1 1]);
    set(h.obj.(h.name),'ycolor',[1 1 1]);
    
if h.obj.(h.name).Position(2) < 30
    set(h.(h.panel).Children,'Visible','Off');
    set(h.(h.panel),'Visible','Off');
    h.panelnr = h.panelnr + 1;
    h.panel = strcat('panel',num2str(h.panelnr));
    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);%
    h.lastnronpage = h.number - 1;
    h.obj.(h.name).Parent = h.(h.panel);
    
    h.obj.(h.name).Position(2) = h.height - height - 20;
else
    h.obj.(h.name).Position(2) = h.lastbottompos - distfromprev - height;
end

    h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
    
    if h.posforequ < 30
        h.posforequ = h.height - distfromprev;
    end
    
if h.interactive
    pause(0.2);
else
    pause(0.2);
end
end
    