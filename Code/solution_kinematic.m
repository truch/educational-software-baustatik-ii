function result = solution_kinematic(getpoints,handles)
format short
f2h = findall(0, 'name', 'displacement method');
    
% PC's active screen size
screen_size = get(0,'ScreenSize');
pc_width  = screen_size(3);
pc_height = screen_size(4);
%So far fixed maximal screen size (not to cause a problem with too much
%space. So far just optimized for small laptop screens)

if pc_width > 1600 || pc_height > 900
    pc_height = 870;
    pc_width = 1550;
end
%Matlab also does not consider the width of the border:
window_border  = 5;
%new position:
height = pc_height - 100;
width = height/sqrt(2) + 60;
widthqst = 300;
bigwidth = width + widthqst;
bottom = pc_height/2 - height/2 + 10;
left   = pc_width/2 - width/2  - window_border/2;

Fig = figure('Tag', 'displacement', ...
      			'Units', 'pixels', ...
      			'Position', [left bottom bigwidth height], ...
      			'Name', 'displacement method', ...
      			'MenuBar', 'none', ...
      			'NumberTitle', 'off', ...
      			'Color', 'white', ...
                'Resize','off');
            
h = guihandles(Fig);

h.height = height;
h.width = width;
h.bottom = bottom;
h.left = left;
h.screendim = [0, 0, pc_width, pc_height];

h.interactive = handles.interactive;


%start INITIALIZATION:
h.questionpanel = uipanel('Parent',Fig,'units','pixels','Position',[h.width, 0, widthqst+2, h.height+2],'BackgroundColor', [ 0.9216    0.9686    1.0000]); %[0.9, 0.9, 0.9]); %
h. graficpanel = uipanel('Parent',h.questionpanel,'Visible','Off','units','pixels','Position',[0, 0, widthqst+2, h.height-220],'BackgroundColor', [0.9882    0.9922    1.0000]); %[ 0.9216    0.9686    1.0000]); %
h.number = 1;
h.panelnr = 1;
h.panel = strcat('panel',num2str(h.panelnr));
h.(h.panel) = uipanel('Parent',Fig,'units','pixels','Position',[0 0 h.width h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]); %
h.name = strcat('text',num2str(h.number));
h.obj.(h.name) = uicontrol(h.(h.panel),'Style','Text','HorizontalAlignment','left','Position',[20, h.height-10-30, 600, 30],'BackgroundColor','white','FontSize',16, 'FontName','Calibri','FontWeight','bold',...
   'String',strcat('Displacement Method'));
h.lastbottompos = h.obj.(h.name).Position(2);
h.pause = 0.5;
h.pausequestion = 0.5;
h.theory = handles.theory;
% end INITIALIZATION: 

%PROGRAM:

%Kinematic analysis
handles = locklines(handles);
handles = findpsi(handles);
for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if handles.beams.(beam).S.s12 == 0 && handles.beams.(beam).S.s21 == 0 && handles.beams.(beam).S.t12 == 0
        handles.beams.(beam).haspsi = false;
    end
end
foundpsi = testpsi(handles); %In Testpsi sind die Disp-Funktionen auszukommentieren.
if foundpsi %system is displaceable
handles.beamdone = [{}];
% Find a fixpoint to start a path from
for j = 1 : numel(handles.pointnames)
    if handles.points.(char(handles.pointnames(j))).fix
        startpoint = handles.points.(char(handles.pointnames(j))).pointnr;
        break;
    end
end

handles = initpath(startpoint,1,handles);
handles = initmatch(1,handles);

nvar = countvar(handles.psieqn);
if not(nvar == 0)
handles.validpsieqn = valideqn(handles.psieqn);
neq = numel(handles.validpsieqn);
dgr = nvar-neq;
varstosolve = symvar(handles.validpsieqn);
handles.dgr = dgr;
h = theory(Fig, h,[{'Identify the degree of displaceability'},{'title'},{1};{'By introducing a hinge at each node, the system possibly becomes a mechanism. The number of holding forces necessary to stabilize the system is equal to its degree of displaceability.'},{'text'},{1};{'theory_holdingforce.PNG'},{'image'},{[8]};{'The system above has two kinematic degrees of freedom. Two holding forces need to be introduced.'},{'text'},{1}]);
h = write(Fig, h, 'text', 'Degree of displaceability: ...', true, false);
h = question(Fig,h,' ',checkfracstring(handles.dgr),'At each node, introduce a hinge instead of a fixed connection. Then, count the number of external holding forces that have to be added, so that the system cannot be displaced. To see the pole-plans, advance to the section "Dislpacement Equilibrium".',false,' ');

if handles.dgr > 1
h = theory(Fig, h,[{'Degree of displaceability n > 1'},{'title'},{1};...
    {'In this system, the degree of displaceability n is greater than 1. Now, n indepentent angles of rotation \psi_{i,j} must be identified and chosen. Two independent angles of rotation do not influence eachother. This means, the rotation of one independent \psi_{i,j} never leads to a rotation of another independent \psi_{k,l}. The system below has a degree of displaceability n = 2.'},{'text'},{1};...
    {'theory_twodegrees.PNG'},{'image'},{[7]};...
    {'The two angles of rotation \psi_{2,3} and \psi_{1,4} are not independent. However, e.g. \psi_{1,4} and \psi_{4,5} are independent. The other independent angles of rotation \psi_{2,3} and \psi_{3,6} must now be expressed in terms of the independent ones.'},{'text'},{1}]);
else
h = theory(Fig, h,[{'Degree of displaceability n = 1'},{'title'},{1};...
    {'In this system, the degree of displaceability n is equal to 1. Choose one angle of rotation that the others will be expressed in terms of.'},{'text'},{1}]);
end

[sol,varstosolve] = psidialog(dgr,varstosolve,handles);
if not(isstruct(sol))
    sol = struct(char(string(varstosolve)),sol);
end
handles.varstosolve = varstosolve;
handles.varindep = symvar(handles.validpsieqn);
for i = 1 : numel(varstosolve)
handles.varindep(handles.varindep == varstosolve(i)) = [];
end

handles = renamepsi(sol,varstosolve,handles);

else
    handles.dgr = 1;
    handles.varstosolve = [];
    handles.varindep = [];
    for p = 1 : numel(handles.beamnames)
        if handles.beams.(char(handles.beamnames(p))).haspsi
            handles.varindep = [handles.varindep handles.beams.(char(handles.beamnames(p))).psisym];
        end
    end
end
h = write(Fig, h, 'text', 'Beam angles of rotation:',false,false);

% Save the equations for the beamangles of rotation in a cell array, ready
% for interpreter latex.
netpsieq = [];
for i = 1 : numel(handles.varstosolve)
    [p1, p2] = readpsivar(char(string(handles.varstosolve(i))));
    thisbeam = beamname(p1,p2);
    thisequation = handles.beams.(thisbeam).psisym;
    netpsieq = strcat('$\psi_{',num2str(p1),',',num2str(p2),'}=',latex_equationconverter(thisequation),'$');
    h = equation(Fig, h,netpsieq);
end

h = theory(Fig, h,[{'Instantaneous centers of rotation '},{'title'},{1};...
    {'Identify the instantaneous centers of rotation (ICR). Each constraint with all directions not displaceable is a possible ICR. Other ICR can be found by drawing straight "locking" lines along each beam initiating from an ICR. Any displacement parallel to such locking lines is not possible. Other locking lines can be derived from constraints such as springs and rollers. Intersections of locking lines are new ICR. In the figure below, all constraints, their associated direction of free displacement and locking lines are listed. '},{'text'},{1};...
    {'theory_constraints.PNG'},{'image'},{[7]}]);
h = theory(Fig, h,[{'Connect the angles of rotation \psi_{i,j}'},{'title'},{1};
    {'Associate each ICR with its beam. Formulate the virtual displacement at each node in terms of all angles of rotation \psi_{i,j}. Use compatibility conditions to connect the angles of rotation with eachother.'},{'text'},{1};...
    {'theory_icr.PNG'},{'image'},{[12]};...
    {'In the simple system above, the degree of displaceability is 1. Point 1 is the ICR for beam 1-2. From point 3, one locking line initiates due to the roller. At the intersection of the locking lines of point 1 and 3, the ICR of beam 2-3 is located. Note that a positive rotation around the ICR is defined in counter-clockwise direction. The virtual displacement at point 2 can be formulated as follows:'},{'text'},{1};...
    {'Virtual displacement at point 2, expressed in terms of \psi_{1,2}:'},{'text'},{1};...
    {'$v_2 = \psi_{1,2}\cdot\sqrt{2}\cdot l$'},{'equation'},{1};...    
    {'Virtual displacement at point 2, expressed in terms of \psi_{2,3}:'},{'text'},{1};...
    {'$v_2 = -\psi_{2,3}\cdot\sqrt{2}\cdot\frac{l}{2}.$'},{'equation'},{1};...
    {'Compatibility condition of the two virtual displacements:'},{'text'},{1};...
    {'$\psi_{1,2}\cdot\sqrt{2}\cdot l = -\psi_{2,3}\cdot\sqrt{2}\cdot\frac{l}{2}\qquad \Longrightarrow\qquad \psi_{2,3} = -2\psi_{1,2}.$'},{'equation'},{1}]);

% Hier Ausgabe des Grads der Verschieblichkeit
% Hier Ausgabe der miteinander verkn�pften Stabdrehwinkel (varstosolve sind
% die abh�ngigen Stabdrehwinkel, mit fieldnames arbeiten.)
else
    handles.dgr = 0;
    h = write(Fig,h, 'text', 'Degree of displaceability: ...', true, false);
    h = question(Fig,h,' ',checkfracstring(handles.dgr),'At each node, introduce a hinge instead of a fixed connection. Then, count the number of external holding forces that have to be added, so that the system cannot be displaced. To see the pole-plans, advance to the section "Dislpacement Equilibrium".',false,' ');
    h = theory(Fig, h,[{'Identify the degree of displaceability'},{'title'},{1};{'By introducing a hinge at each node, the system possibly becomes a mechanism. The number of holding forces necessary to stabilize the system is equal to its degree of displaceability.'},{'text'},{1};{'theory_notdispl.PNG'},{'image'},{[10]};{'The system above has no kinematic degrees of freedom. The pin at point 3 locks the displacement parallel to the direction of beam 2-3. The fixed support at point 1 locks the displacement parallel to beam 1-2. Thus, no displacement is possible.'},{'text'},{1}]);
    h = write(Fig, h,'text','The system is not displaceable.',false,false);
end
% Find the knots having phis
handles = findphi(handles);

%Formulate all of the point equilibrium-equations
handles.pointeqn = [];
handles.pointeqnname = [];
handles = pointequilibrium(handles);

h = write(Fig, h,'title','Beam-End-Moments',false,false);
h = theory(Fig, h,[{'Definition of the beam-end moments'},{'title'},{1};...
    {'The beam end moments at beam i-j are defined as follows:'},{'text'},{1};...
    {'$ M_{i,j} = M_{i,j}^0 + s_{i,j}\varphi_i + t_{i,j}\varphi_j - (s_{i,j} + t_{i,j})\psi_{i,j}$'},{'equation'},{1};...
    {'where'},{'text'},{1};...
    {'M_{i,j}^0 is the fixed-end moment of the beam, caused by external load;'},{'text'},{1};...
    {'s_{i,j} is the stiffness defined as the moment acting in i, necessary for a unit rotation of point i'},{'text'},{1};...
    {'t_{i,j} is the stiffness defined as the moment acting in j, necessary for a unit rotation of point i'},{'text'},{1};...
    {'Sign convention: The beam end moments are defined to rotate counter-clockwise around the beam.'},{'text'},{1};...
    {'theory_sign2.PNG'},{'image'},{[9]}]);
for i = 1 : numel(handles.pointnames)
    current = char(handles.pointnames(i));
    currentnr = handles.points.(current).pointnr;
    for j = 1 : numel(handles.points.(current).neighbours)
        neighbour = pointname(handles.points.(current).neighbours(j));
        neighbournr = handles.points.(current).neighbours(j);
        if h.interactive
        if not(isempty(symvar(handles.points.(current).(neighbour).M)))
            %Here comes the function generating the multiplechoice panel
            %with wrong equations.
            h = beamendmoments_wrong(h,handles.points.(current).(neighbour).M,currentnr,neighbournr);
            h = question(Fig,h,'Please choose the correct equation for the current beam-end-moment.',' ',['The definition for beam end moments is:',10,' M_{i,j} = M_{i,j}^0 + s_{i,j}\phi_i + t_{i,j}\phi_j - (s_{i,j} + t_{i,j})\psi_{i,j}'],true,'beam-end-moment');
            set(0,'currentfigure',Fig);
        end
        end
        h = equation(Fig, h,strcat('M_{',num2str(currentnr),',',num2str(neighbournr),'} = ',...
            latex_equationconverter(handles.points.(current).(neighbour).M)));
    end
end
%Hier oben Ausgabe der Stabendmomente

handles.displeqn = [];
if foundpsi
%Formulate the displacement-equilibria:
h.pldone = false;
h.mdone = false;
h.dldone = false;
h.intdone = false;
h = write(Fig, h,'title','Displacement equilibrium',false,false);
if handles.dgr == 1
    thispsi = latexpsi(char(string(handles.varindep)));
    thispsi = thispsi(2:numel(thispsi)-1);
h = theory(Fig, h,[{'Displacement equilibrium initialisation'},{'title'},{1};...
    {strcat('The degree of displaceability is equal to 1. Set the independent angle of rotation ',thispsi,' to 1. (This is the angle all others are expressed in terms of.) Calculate the other angles of rotation.')},{'text'},{1}]);
else
    thispsi = latexpsi(char(string(handles.varindep(1))));
    thispsi = thispsi(2:numel(thispsi)-1);
h = theory(Fig, h,[{'Displacement equilibrium initialisation'},{'title'},{1};...
    {strcat('The degree of displaceability n =',num2str(handles.dgr),'is greater than 1. Hence, n =',num2str(handles.dgr),' displacement equilibria have to be formulated. Choose the first independent angle of rotation, here ', thispsi ,'. Set it to 1 and the other independent angles to 0. Using this, calculate the value of the dependent angles of rotation. Repeat this procedure for all of the independent angles.')},{'text'},{1}]);
end
for i = 1 : numel(handles.varindep)
    [handles,h,Fig] = displacementequilibrium(handles.varindep(i),handles,h,Fig);
end
% Hier Ausgabe der Verschiebegleichgewichte (MIT ZUGEHOERIGEN BILDERN)
end

if not(isempty(handles.pointeqn))
h = write(Fig, h,'title','Point equilibrium',false,false);
h = theory(Fig, h,[{'Point equilibrium'},{'title'},{1};...
    {'The point equilibrium is basically a moment equilibrium. It is formulated at every node that has an angle of rotation \phi_i. All beam-end moments at the current node are summed up and brought into equilibrium.'},{'text'},{1};...
    {'theory_sign.PNG'},{'image'},{[10]};...
    {'$M_i + \sum M_{i,k} = 0$'},{'equation'},{1};
    {'Sign convention: All moments rotating counter-clockwise around the point have a positive sign. Note: A beam-end moment that rotates counter-clockwise around its beam rotates clockwise around the point!'},{'text'},{1}]);
end
for i = 1 : numel(handles.pointeqn)
    h = equation(Fig, h, strcat(' $ Point $ ',num2str(handles.pointeqnname(i))));
    if h.interactive
       h = pointequilibrium_wrong(h,handles.pointeqn(i),handles.pointeqnname(i));
       h = question(Fig,h,'Please choose the correct equation for the point equilibrium.',' ','Add up all the beam-end moments at the current point. Add an external moment if there is one. Sign convention: Moments rotating counterclockwise around the end of a beam have a positive sign.',true,'beam-end-moment');
       set(0,'currentfigure',Fig);
    end
    h = equation(Fig, h, latex_equationconverter(handles.pointeqn(i)));
end
% Hier Ausgabe der Knotengleichgewichte (MIT ZUGEHOERIGEM KNOTEN) Ist
% sinnvoller hier, dann ist alles was zu Verschiebungen etc geh�rt
% beisammen
if not(isempty(handles.pointeqn))
    angles = findangles(handles.displeqn, handles.pointeqn);
    solangles = solve([handles.displeqn;handles.pointeqn],angles);
if not(isstruct(solangles))
    if not(isempty(handles.displeqn))
    solangles = struct(char(string(angles)),solangles);
    elseif not(isempty(handles.pointeqn))
    solangles = struct(char(string(angles)),solangles);
    end
end
names = fieldnames(solangles);
h = write(Fig, h,'title','Solutions for the angles:',false,false);


% Hier Ausgabe der definitiven Werte aller Winkel
% Substitute all the symbolic variables by their actual values
handles = subs_angles_def(solangles,names,handles);

for i = 1 : numel(names)
    currentname = char(names(i));
approxeqn = approximate_equation(solangles.(currentname));
exacteqn = latex_equationconverter(solangles.(currentname));

if strcmp(handles.unit,'kN'), unit = '\mathrm{rad}';
elseif strcmp(handles.unit,'q'), unit = '\mathrm{\frac{ql^3}{EI}}';
elseif strcmp(handles.unit,'Q'), unit = '\mathrm{\frac{Ql^2}{EI}}';
else unit = '\mathrm{\frac{Ml}{EI}}'; end


if strcmp(currentname(1:3),'psi')
    varname = latexpsi(currentname);
    varname = varname(2 : numel(varname)-1);
else
    varname = latexphi(currentname);
    varname = varname(2 : numel(varname)-1);
end

if numel(exacteqn) > 100
    h = equation(Fig, h,strcat(varname, '\approx', approx_angle(approxeqn,handles.unit)));
else
h = equation(Fig, h,strcat(varname, ' = ', exacteqn,'\approx', approx_angle(approxeqn,handles.unit)));
end
end
end
h = write(Fig, h,'title','Beam-End-Moments (numerical)',false,false);

if strcmp(handles.unit,'kN'), unit = '\mathrm{kNm}';
elseif strcmp(handles.unit,'q'), unit = '\mathrm{ql^2}';
elseif strcmp(handles.unit,'Q'), unit = '\mathrm{Ql}';
else unit = '\mathrm{M}'; end

for i = 1 : numel(handles.pointnames)
    current = char(handles.pointnames(i));
    currentnr = handles.points.(current).pointnr;
    for j = 1 : numel(handles.points.(current).neighbours)
        neighbour = pointname(handles.points.(current).neighbours(j));
        neighbournr = handles.points.(current).neighbours(j);
        if numel(latex_equationconverter(handles.points.(current).(neighbour).M)) > 100
            h = equation(Fig, h,strcat('M_{',num2str(currentnr),',',num2str(neighbournr),'} \approx', num2str(round(double(approximate_equation(handles.points.(current).(neighbour).M)),4)),unit));
        else
            if strcmp(latex_equationconverter(handles.points.(current).(neighbour).M),'0')
                h = equation(Fig, h,strcat('M_{',num2str(currentnr),',',num2str(neighbournr),'} = ',...
                latex_equationconverter(handles.points.(current).(neighbour).M)));
            else
                h = equation(Fig, h,strcat('M_{',num2str(currentnr),',',num2str(neighbournr),'} = ',...
                latex_equationconverter(handles.points.(current).(neighbour).M), '\approx', num2str(round(double(approximate_equation(handles.points.(current).(neighbour).M)),4)),unit));
            end
        end
    end
end

%Hier Ausgabe aller definitiven Stabendmomente.
%Superpone the Moment lines M0 and the beam-end-moments calculated by the
%displacement method.
for i = 1 : numel(handles.pointnames)
    current = char(handles.pointnames(i));
    neighbours = handles.points.(current).neighbours;
    for j = 1 : numel(neighbours)
        neighbour = pointname(neighbours(j));
        handles.points.(current).(neighbour).M = approximate_equation(handles.points.(current).(neighbour).M);
    end
end
handles = MomentSup(handles);

%Plot the final moment lines:
set(0,'currentfigure',getpoints)
set(getpoints, 'currentaxes', handles.axes1)
handles = finalmoments(handles);

%Copy the image to the sample solution
    h.number = 1 + h.number;
    h.name = strcat('image',num2str(h.number)); 
    h.obj.(h.name) = copyobj(handles.axes1,h.(h.panel));
    hold on

    h.obj.(h.name).Units = 'Pixels';
    width = h.width - 40;
    height = 500;
    distfromprev = 10;
    distfromnext = 10;
    h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
    h.obj.(h.name).XTick = [];
    h.obj.(h.name).YTick = [];
    h.obj.(h.name).XColor = [1,1,1];
    h.obj.(h.name).YColor = [1,1,1];
    bottom = h.obj.(h.name).Position(2);

if bottom < 5
    set(h.(h.panel).Children,'Visible','Off');
    set(h.(h.panel),'Visible','Off');
    h.panelnr = h.panelnr + 1;
    h.panel = strcat('panel',num2str(h.panelnr));
    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white','Parent',Fig); %[0.9, 0.9, 0.9]);
    h.lastnronpage = h.number - 1;
    h.obj.(h.name).Parent = h.(h.panel);
    
    h.obj.(h.name).Position(2) = h.height - height - 20;
else
    h.obj.(h.name).Position(2) = h.lastbottompos - distfromprev - height;
end
set(0,'currentfigure',getpoints)
set(getpoints,'currentaxes',handles.axes1)
for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'Mplot')
        set(handles.beams.(beam).Mplot,'Visible','Off');
    end
    if isfield(handles.beams.(beam),'labels')
        labelnames = fieldnames(handles.beams.(beam).labels);
        for j = 1 : numel(labelnames)
            currentlabel = char(labelnames(j));
            set(handles.beams.(beam).labels.(currentlabel),'Visible','Off')
        end
    end 
end
set(0,'currentfigure',Fig);
dopdf = uicontrol('Parent',h.questionpanel,'Style','push','String','Print','Visible', 'Off','HorizontalAlignment','center','BackgroundColor',[0.38, 0.48, 0.55],'FontSize',12,'ForegroundColor','white', 'FontName','Calibri','Position',[widthqst-60, h.height-30, 50, 20],'Callback',@printpage);
h = question(Fig,h,'ische','ische','ische',false,'empty');
%Here comes the shear-force-line.



%previouspage: 
% newpanelnr = h.panelnr;
% previouspage = uicontrol(Fig,'Style','Pushbutton','Position',[20, 20, 100, 20], 'String','previous page','FontSize',11,'FontName','Calibri','Backgroundcolor',[0.7 0.9 0.9],'ButtonDownFcn',@setpreviouspage);
% previouspage.Enable = 'Inactive';
    
% function setpreviouspage(h, event)
%         h = guidata(gcbo);
%         guidata(gcbo,h);
%         if newpanelnr > 1
%             set(h.(strcat('panel',num2str(newpanelnr))).Children,'Visible','Off');
%             set(h.(strcat('panel',num2str(newpanelnr))),'Visible','Off');
%             set(h.(strcat('panel',num2str(newpanelnr-1))).Children,'Visible','On');
%             set(h.(strcat('panel',num2str(newpanelnr-1))),'Visible','On');
%             newpanelnr = newpanelnr - 1;
%         end
%     end

%nextpage:
% nextpage = uicontrol(Fig,'Style','Pushbutton','Position',[h.width-20-70, 20, 70, 20], 'String','next page','FontSize',11,'FontName','Calibri','Backgroundcolor',[0.7 0.9 0.9],'ButtonDownFcn',@setnextpage);
% nextpage.Enable = 'Inactive';
    

% function setnextpage(h, event)
%         h = guidata(gcbo);
%         guidata(gcbo,h);
%         if newpanelnr < h.panelnr
%             set(h.(strcat('panel',num2str(newpanelnr))),'Visible','Off');
%             set(h.(strcat('panel',num2str(newpanelnr))).Children,'Visible','Off');
%             set(h.(strcat('panel',num2str(newpanelnr+1))),'Visible','On');
%             set(h.(strcat('panel',num2str(newpanelnr+1))).Children,'Visible','On');
%             newpanelnr = newpanelnr + 1;
%         end
%     end

%print


function printpage(hObject,eventdata,figure)
    % get the proper format
    set(Fig, 'PaperUnits', 'points');
    set(Fig, 'PaperSize', [h.width-widthqst, height]); 
    set(Fig, 'PaperPosition', [0, 0, h.width-widthqst, h.height]); %[ left, bottom, width, height]
    set(Fig, 'PaperPositionMode', 'auto');
%     
    document = 'force method.fig';
    copyobj(h.panel1,gcf); 
%   print('doc1','-dpng','-r0')
    print('ische','-dpsc2','-r0');
%   print(gcf,'-dpdf',document);
    copyobj(h.panel2,gcf); 
    print('ische','-dpsc2','-append','-r0');
    ps2pdf(load('ische.ps'))
%   print('doc2','-dpng','-r0');

%     cmdfile = ['force method' '.txt'];
% % fh = fopen(cmdfile, 'w');
% % print(fh, '-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile="%s" -f', 'page0.pdf');
% % print(fh, ' "%s"', 'page1.pdf');    
% %     print(fig1, '-dpdf', 'Filename.pdf')
% fprintf(fig0, '-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile="%s" -f', 'forcemethod.pdf');%     print(gcf, '-dpdf', ['force method','.pdf']);
% fprintf(fig_handle, '-dpsc', '-append', 'D:\output_dir\output_file.ps')
end


% Use sprintf to create text with a new line character, such as sprintf('first line \n second line'). This property converts text with new line characters to cell arrays.
result = handles;

guidata(Fig,h);
uiwait(Fig);
end