function vres = callv(v)
var = symvar(v);
vsub = v;
for i = 1 : numel(var)
    vsub = subs(vsub,var(i),1);
end
vres = vsub;
end