function result = cdial_set(constrainttype,knottype,neighbours,handles)
% Function only considers allowed combinations. Neighbours must be a vector
% containing the point numbers of the neighbours to the current point.
%Resetting the interface to original state:
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');
    set(handles.constraintimages.fixed.patch,'Visible','off');
    set(handles.constraintimages.pin.patch,'Visible','off');
    set(handles.constraintimages.roller.patch,'Visible','off');
    set(handles.constraintimages.spring.patch,'Visible','off');
    set(handles.constraintimages.torsional.patch,'Visible','off');
    set(handles.constraintimages.none.patch,'Visible','off');
    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    set(handles.connectionimages.free_im,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');    
if not(isempty(constrainttype))
%Evualuating the input args
if strcmp(constrainttype,'fixed')
    set(handles.constraintimages.fixed.patch,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.rigid.patch,'Visible','On');
    handles.temp.constrainttype = 'fixed';
    handles.temp.knottype = 'connection';
elseif strcmp(constrainttype,'pin')
    set(handles.constraintimages.pin.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'pin';
elseif strcmp(constrainttype,'roller')
    set(handles.constraintimages.roller.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'roller';
elseif strcmp(constrainttype,'spring')
    set(handles.constraintimages.spring.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.constrainttype = 'spring';
    handles.temp.knottype = 'hinge';
elseif strcmp(constrainttype,'torsional spring')
    set(handles.constraintimages.torsional.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
        set(handles.connectionimages.hinge_im,'Visible','on');
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'torsional spring';
elseif strcmp(constrainttype,'none')
    set(handles.constraintimages.none.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
    set(handles.connectionimages.free_im,'Visible','on');
    set(handles.connectionimages.free.patch,'Visible','on');
    handles.temp.knottype = 'free';
    end
    handles.temp.constrainttype = 'none';
end
end
result = handles;
end