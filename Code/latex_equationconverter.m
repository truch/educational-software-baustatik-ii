function latexstr = latex_equationconverter(equation)
%input arg equation must be a sym
equationstr = char(string(latex(equation)));
%Get the positions of the word 'psi' in the equation
psipos = findstr('\mathrm{psi',equationstr);
%Replace those psis with their latex-equivalent
psitoreplace = [];
psireplaced = [];
for i = 1 : numel(psipos)
    %find the end of the variablename psiixy
    bracket = 1;
    for j = psipos(i) + 12 : numel(equationstr)
        if strcmp(equationstr(j),'}') && bracket == 2            
            psiendpos = j;
            break;
        elseif strcmp(equationstr(j),'}') && bracket == 1
            bracket = 2;
        end
    end
    thispsi = equationstr(psipos(i) : psiendpos);
    [p1, p2] = readpsi(thispsi);
    oldpsi = thispsi;
    thispsi = strcat('\psi_{',num2str(p1),',',num2str(p2),'}');
    if not(ismember(thispsi,psitoreplace))
    psireplaced = [psireplaced, {oldpsi}];
    psitoreplace = [psitoreplace, {thispsi}];
    end
end
for i = 1 : numel(psitoreplace)
    equationstr = strrep(equationstr,char(psireplaced(i)),char(psitoreplace(i)));
end
equationstr = strrep(equationstr,'ql','ql^2');
equationstr = strrep(equationstr,'EI','\frac{EI}{l}');
latexstr = equationstr;
end