function h = write(Fig, h, type, str, question, staytogether)
%staytogether = true: line and graph belong together and should not be
%separated by a new page
%type = 'text'/ 'title' 
%question = true/false

set(0, 'currentfigure', Fig);

distfromprev = 10;
distfromnext = 10;
fields = fieldnames(h.obj);
    
h.number = 1 + h.number;
h.name = strcat('text',num2str(h.number));

% define HEIGHT of box:
if strcmp(type,'text')
    % a text line can have 87 characteres
    height = ceil(strlength(str)/(57/536*h.screendim(3) - 5584/67))*20;        
else
    % title line can have 75 characteres
    height = ceil(strlength(str)/(0.018656716*h.screendim(3)+41.3428))*20;        
end    

% start POSITION:
    left = 20;
    bottom = h.lastbottompos - distfromprev - height;
    width = h.width - 30;
    
if staytogether
    bottom = h.lastbottompos - distfromprev - height - 20 - 100; % height of the graph = 100
end
    
if bottom < 50
    set(h.(h.panel).Children,'Visible','Off');
    set(h.(h.panel),'Visible','Off');
    h.panelnr = h.panelnr + 1;
    h.newpanelnr = h.panelnr;
    h.panel = strcat('panel',num2str(h.panelnr));
    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
    h.lastnronpage = h.number - 1;
    h.obj.(h.name).Parent = h.(h.panel);

    bottom = h.height - distfromprev - height - 5;
else
    bottom = h.lastbottompos - distfromprev - height;
end

if strcmp(fields(h.number-1), strcat('text',num2str(h.number-1))) && h.obj.(char(fields(h.number-1))).Parent == h.(strcat('panel',num2str(h.panelnr)))
    bottom = bottom+10;
end

h.lastbottompos = bottom - distfromnext;
% end POSITION

if question && ~h.interactive
    h.obj.(h.name).Visible = 'Off';
end

% STATIC TEXT:
if strcmp(type, 'text')
    h.obj.(h.name) = uicontrol(h.(h.panel),'Style','Text',...
                                'String',str,...
                                'Position',[left, bottom, width, height],...
                                'HorizontalAlignment','left',...
                                'BackgroundColor','white',...
                                'FontSize',11,...
                                'FontName','Calibri');
else
    h.obj.(h.name) = uicontrol(h.(h.panel),'Style','Text',...
                                'String',str,...
                                'Position',[left, bottom, width, height],...
                                'HorizontalAlignment','left',...
                                'BackgroundColor','white',...
                                'FontSize',12,...
                                'FontName','Calibri',...
                                'FontWeight','bold');    
end        

if staytogether
    h.obj.(h.name).Tag = 'yes';
else
    h.obj.(h.name).Tag = 'no';
end

if h.interactive && ~question
    pause(h.pause);
elseif ~h.interactive && ~question
    pause(0.2);
end

end