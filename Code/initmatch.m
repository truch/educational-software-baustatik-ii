function result = initmatch(pnr,handles)

%This function needs to be initialised the first time with a fixpoint
%(path(pnr==1)(1) must be fix.

element = 1;
current = handles.paths.(pathname(pnr))(element);
if handles.points.(pointname(current)).fix
    handles.points.(pointname(current)).v = [0;0];
else %This point has already been looked at
    handles.points.(pointname(current)).v = handles.points.(pointname(current)).v;
end
element = element + 1;
handles = contmatch(pnr,element,handles);
result = handles;
end







