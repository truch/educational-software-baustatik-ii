function result = pointequilibrium(handles)
%This function formulates the node-equilibrium for all the points that are
%defined in handles. 'hasphi' must be a fieldname of each point.
%Furthermore, the beamendmoments, as a function of node and beam angles are
%formulated for each beam.
pointnames = handles.pointnames;

if strcmp(handles.unit,'kN'), syms kNm; unit = kNm;
elseif strcmp(handles.unit,'q'), syms ql; unit = ql;
elseif strcmp(handles.unit,'Q'), syms Ql; unit = Ql;
else syms M; unit = M; end

if strcmp(handles.unit,'kN'), syms kNm; unit2 = kNm;
else syms EI; unit2 = EI; end

for i = 1 : numel(pointnames)
    neighbours = handles.points.(char(pointnames(i))).neighbours;
    currentpoint = handles.points.(char(pointnames(i))).pointnr;
    handles.points.(pointname(currentpoint)).equation = handles.points.(pointname(currentpoint)).M;
    for n = 1 : numel(neighbours)
        handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = handles.points.(pointname(currentpoint)).M;
        thisbeam = beamname(currentpoint,neighbours(n));
        if currentpoint < neighbours(n)
            s = handles.beams.(thisbeam).S.s12*unit2;
            t = handles.beams.(thisbeam).S.t12*unit2;
            M0 = - handles.beams.(thisbeam).S.Mtot(0)*unit;
        else
            s = handles.beams.(thisbeam).S.s21*unit2;
            t = handles.beams.(thisbeam).S.t12*unit2;
            M0 = handles.beams.(thisbeam).S.Mtot(handles.beams.(thisbeam).l)*unit;
        end
    if handles.points.(char(pointnames(i))).hasphi
            %Beamendmoment at currentpoint, of beam
            %currentpoint-neighbours(n)
            handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = ...
                M0 + s*(handles.points.(pointname(currentpoint)).phisym + ...
                handles.points.(pointname(currentpoint)).phi*unit/unit2) + ...
                t*handles.points.(pointname(neighbours(n))).phi*unit/unit2 -...
                (s + t)*handles.beams.(thisbeam).psi*unit/unit2;
            %same adds to the pointequilibrium equation
            if handles.points.(pointname(neighbours(n))).hasphi
            handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = ...
                handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M + ...
                t*handles.points.(pointname(neighbours(n))).phisym;
            end
            if handles.beams.(thisbeam).haspsi
            handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = ...
                handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M + ...
                - (s + t)*handles.beams.(thisbeam).psisym;
            end
            %Update the pointequilibrium-equation at the currentpoint
            handles.points.(pointname(currentpoint)).equation =... 
                handles.points.(pointname(currentpoint)).equation +...
                handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M + ...
                handles.points.(pointname(currentpoint)).stiffness*handles.points.(pointname(currentpoint)).phisym;
    else
        handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = ...
        handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M + M0 ...
            +s*handles.points.(pointname(currentpoint)).phi*unit/unit2...
            +t*handles.points.(pointname(neighbours(n))).phi*unit/unit2...
            -(s + t)*handles.beams.(thisbeam).psi*unit/unit2;
        if handles.points.(pointname(neighbours(n))).hasphi
            handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = ...
                handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M + ...
                t*handles.points.(pointname(neighbours(n))).phisym;
        end
        if handles.beams.(thisbeam).haspsi
            handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M = ...
                handles.points.(pointname(currentpoint)).(pointname(neighbours(n))).M + ...
                - (s + t)*handles.beams.(thisbeam).psisym;
        end
    end
    end
end

for i = 1 : numel(pointnames)
    currentpoint = char(pointnames(i));
    if handles.points.(currentpoint).hasphi
       handles.points.(currentpoint).equation = [handles.points.(currentpoint).equation == 0];
       handles.pointeqn = [handles.pointeqn; handles.points.(currentpoint).equation];
       handles.pointeqnname = [handles.pointeqnname handles.points.(currentpoint).pointnr];
    end
end
result = handles;
end