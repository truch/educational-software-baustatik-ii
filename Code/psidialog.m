function [sol,varstosolve] = psidialog(dgr,varstosolve,handles)
 d = figure('Position',[300 300 250 300],'Name','Choose indepentent angles of rotation','MenuBar','none','Color',[.98,.99,1]);
oldvarstosolve = varstosolve;
    txt = uicontrol('Parent',d,...
           'Style','text',...
           'Position',[20 210 210 50],...
           'FontName','Bahnschrift',...
           'BackgroundColor',[.98,.99,1],...
           'String',(strcat('Please type the beamnumbers of',{' '},num2str(dgr),...
           {' '},'independent beam angles of rotation. The following angles of rotation are nonzero:')));
    
       % Generate the latex string for varstosolve
        for i = 1 : numel(varstosolve)
            if i == 1
                varstosolvestr = latexpsi(char(string(varstosolve(i))));
            else
            varstosolvestr = strcat(varstosolvestr,',', {' '}, latexpsi(char(string(varstosolve(i)))));
            end
        end
        pos = get(d,'position');
        dx = pos(3);
        dy = pos(4);
       txt2 = annotation('textbox',[20/dx 150/dy 210/dx 55/dy],'string',varstosolvestr,'interpreter','latex','HorizontalAlignment','center');
    
    str1 = uicontrol('Parent',d,...
           'Style','edit',...
           'Position',[20 100 210 20],...
           'FontName','Bahnschrift',...
           'String','Format: 1x2,2x3 etc.');
       
    btn1 = uicontrol('Parent',d,...
           'Position',[89 60 70 25],...
           'FontName','Bahnschrift',...
           'String','OK',...
           'BackgroundColor',[.73,.93,1],...
           'Callback',@custompsis);

    btn2 = uicontrol('Parent',d,...
           'Position',[89 20 70 25],...
           'FontName','Bahnschrift',...
           'BackgroundColor',[.73,.93,1],...
           'String','Default',...
           'Callback',@defaultpsis);

   uiwait(d);
   
        function custompsis(event,btn1)
            varstosolve = oldvarstosolve;
        str = get(str1,'string');
        xpos = findstr('x',str);
        commapos = findstr(',',str);
        if numel(xpos) ~= dgr
            msgbox(strcat('Please choose',{' '},num2str(dgr),{' '},'independent angles'),'Error!','error');
        else
        if not(isempty(findstr(' ',str)))
            msgbox('Oops, something with the format went wrong. Make sure you do not use spaces.','Error!','error');
        else
        for i = 1 : numel(xpos)
            if i == 1
                pt1 = str2double(str(1 : xpos(i)-1));
            else
                pt1 = str2double(str(commapos(i-1) + 1 : xpos(i)-1));
            end
            if i ~= numel(xpos)
                pt2 = str2double(str(xpos(i)+1 : commapos(i)-1));
            else
                pt2 = str2double(str(xpos(i)+1 : numel(str)));
            end
            varstosolve(varstosolve== handles.beams.(beamname(pt1,pt2)).psisym) = [];
        end
            sol = solve(handles.validpsieqn,varstosolve);
            if not(isstruct(sol))
            sol = struct(char(string(varstosolve)),sol);
            end
            solnames = fieldnames(sol);
            for n = 1 : numel(solnames)
                stop2 = false;
                if isempty(sol.(char(solnames(n))))
                    stop2 = true;
                    break;
                end
            end
            if stop2
                msgbox('The angles you chose are not indepentent.','Error!','error');
            else
                delete(gcf);
            end
        end
        end
       end
   
    function defaultpsis(event,btn2)
        [sol,varstosolve] = validsol(handles.validpsieqn,varstosolve,dgr);
        delete(gcf);
    end
end
          