function result = initpath(i,pnr,handles)
%Initialises a path through the System
%For the Very first time this function is called, i must be a fixpoint
%'beamdone' must be initialised as a field of handles
neighbours = handles.points.(pointname(i)).neighbours;
handles.points.(pointname(i)).notdone = [{}];
handles.paths.(pathname(pnr)) = [i];
if numel(neighbours) == 1
    k = neighbours(1);
    handles.beamdone = [handles.beamdone; {beamname(i,k)}];
else
    for j = 1 : numel(neighbours) %Find first neighbour whose beam(i x neighbour) is not done yet
        if not(isdone(beamname(i,neighbours(j)),handles))
            k = neighbours(j);
            break;
        end
    end
    handles.beamdone = [handles.beamdone; {beamname(i,k)}];
    for j = 1 : numel(neighbours) %Remember all beams which are not done yet
        if not(isdone(beamname(i,neighbours(j)),handles))
            handles.points.(pointname(i)).notdone = [handles.points.(pointname(i)).notdone, {beamname(i,neighbours(j))}];
        end
    end
end
    handles = contpath(k,pnr,handles); %Continue the search of the path with contpath function
    result = handles;
end