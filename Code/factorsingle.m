function factor = factorsingle(pnr,handles)
%last beam of this path must have psi
current = handles.paths.(pathname(pnr))(numel(handles.paths.(pathname(pnr))));
precurrent = handles.paths.(pathname(pnr))(numel(handles.paths.(pathname(pnr)))-1);
tvec = handles.points.(pointname(current)).tvec;
% find the first path which "precurrent" is element of
for i = 1 : numel(fieldnames(handles.paths))
    for j = 1 : numel(handles.paths.(pathname(i)))
        foundpath = false;
        if handles.paths.(pathname(i))(j) == precurrent
            pnrf = i;
            foundpath = true;
            break;
        end
    end
    if foundpath
        break;
    end
end

    
    psisol = solve(handles.psieqn.(eqname(pnrf)));
    if isstruct(psisol)
        psisol = struct2cell(psisol);
    else
        psisol = mat2cell(psisol,1)
    end
    i = 2;
    variables = symvar(psisol{1});
    while true
    if variables(i-1) ~= symvar(psisol{i})
    variables = [variables symvar(psisol{2})];
    i = i + 1;
    else
        break;
    end
    end

    if numel(psisol) == 1
        solvedvar = symvar(handles.psieqn.(eqname(pnrf)),1);
        v = handles.points.(pointname(precurrent)).v,psisol{1};
        v(variables) = subs(v,solvedvar,psisol)
    else
        solvedvar = symvar(handles.psieqn.(eqname(pnrf)),2);
        v = handles.points.(pointname(precurrent)).v;
        v(variables) = subs(subs(v,solvedvar(1),psisol{1}),solvedvar(2),psisol{2});
    end
    vproj = callv(v);
if dot(double(vproj),rot90vec(tvec)) >= 0
    factor = 1;
else
    factor = -1;
end       
end