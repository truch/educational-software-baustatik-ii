function eqnarray = wrong_equation_generator(equation)
%Equation must not be equal to 0 and must contain at least one symvar!
var = symvar(equation);
defvar = [];
syms EI kNm ql Ql M m l
%identify variables which are not of the kind psiaxb or phia
for i = 1 : numel(var)
    if var(i) ~= EI && var(i) ~= m && var(i) ~= l
        defvar = [defvar var(i)];
    end
end

%First wrong equation
if isempty(defvar)
    wrongeqn1 = 0;
    wrongeqn2 = 0;
else
pickindex = randi([1,numel(defvar)]);
pickvar = defvar(pickindex);
d_or_m = randi([1,2]);
while true
    n = randi([-6,6]);
    if n~=1
        if not(n == 0 && d_or_m == 1)
        break;
        end
    end
end
if d_or_m == 1
    wrongvar = pickvar/n;
else
    wrongvar = pickvar*n;
end
wrongeqn1 = subs(equation,pickvar,wrongvar);

%Second wrong equation
pickindex = randi([1,numel(defvar)]);
pickvar = defvar(pickindex);
old_d_or_m = d_or_m;
old_n = n;
while true
    d_or_m = randi([1,2]);
    while true
    n = randi([-6,6]);
    if n~=1
        break;
    end
    end
    if not(n == old_n && old_d_or_m == d_or_m)
        if not(n == 0 && d_or_m == 1)
        break;
        end
    end
end
if d_or_m == 1
    wrongvar = pickvar/n;
else
    wrongvar = pickvar*n;
end
wrongeqn2 = subs(equation,pickvar,wrongvar);
end

eqnarray = [wrongeqn1; wrongeqn2];
end