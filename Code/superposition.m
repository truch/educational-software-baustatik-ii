function h = superposition(Fig, h, type)

set(0, 'currentfigure', Fig);

l = h.S.l;

syms x Msup M

colorM = [0.2 0.5 1];
colorM0 = [0.5 0.8 1];
pl = 0; 
dl = 0;
m = 0;
X1 = 0;
X2 = 0;
Msup(x) = 0;
if type == 0, total = h.load;
else, total = h.load + h.S.N.end;
end
load = 0;
heighteq = 120;
smallwidth = 90;
smallheight = 70;

bigwidth = 0.037313433*h.screendim(3) + 92.686567;
bigheight = 35/107*h.screendim(4) - 8840/107;
nrofgraphs = round(0.00186567*h.screendim(3) + 1.134328358);

if h.load ~= 0 && ~h.allfuncequal0
    if type == 0
        strequ = '\mathrm{M_0 = ';
    else
        strequ = '\mathrm{M =';
    end
    fields = fieldnames(h.obj);

    if h.lastbottompos < 220 + 95*ceil(h.load/nrofgraphs) + 100 + 100*ceil(h.load/(nrofgraphs*2))
        set(h.(h.panel).Children,'Visible','Off');
        set(h.(h.panel),'Visible','Off');
        h.panelnr = h.panelnr + 1;
        h.newpanelnr = h.panelnr;
        h.panel = strcat('panel',num2str(h.panelnr));
        h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
        h.lastnronpage = h.number - 1;
        h.lastbottompos = h.height - 10;
    end

    h.panelofsuperposition = true;

    if type == 0
        h = write(Fig, h,'title', 'Moment line of the superimposed moment M of the primary structure:', false, true); 
    else
        h = write(Fig, h,'title', 'Moment line of the superimposed moment M of the indetermined structure:', false, true); 
    end

    lastbottompos = h.lastbottompos;

    for i = 1 : numel(fields)
        if contains(fields(i),'pointload')
            yes = 1;
            sort = 'pointload';
            pl = pl + 1;
            if pl == 1 && type == 0
                if type == 0
                    strequline = strcat(strequ, '\underline{M_{point load',num2str(pl),', 0}}');
                    strequ = strcat(strequ, 'M_{point load',num2str(pl),', 0}');
                else
                    strequline = strcat(strequ, '\underline{M_{point load',num2str(pl),'}}');
                    strequ = strcat(strequ, 'M_{point load',num2str(pl),'}');
                end
            else
                h.lastbottompos = lastbottompos;
                if type == 0
                    strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{point load', num2str(pl), ', 0}}');
                    strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{point load', num2str(pl), ', 0}');
                else
                    strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{point load', num2str(pl), '}}');
                    strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{point load', num2str(pl), '}');
                end
            end
        elseif contains(fields(i),'distributedload')
            yes = 1;
            sort = 'distributedload';
            dl = 1;
            if pl > 0 || X1 == 1
                if type == 0
                    strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{distr. load, 0}}');
                    strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{distr. load, 0}');
                else
                    strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{distr. load}}');
                    strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{distr. load}');
                end
            else
                if type == 0
                    strequline = strcat(strequ, '\underline{M_{distr. load, 0}}');
                    strequ = strcat(strequ, 'M_{distr. load,0}');
                else
                    strequline = strcat(strequ, '\underline{M_{distr. load}}');
                    strequ = strcat(strequ, 'M_{distr. load}');
                end
            end
        elseif contains(fields(i),'moment')
            yes = 1;
            sort = 'moment';
            m = 1;
            if pl > 0 || dl == 1 || X1 == 1
                if type == 0
                    strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{moment, 0}}');
                    strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{moment, 0}');
                else
                    strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{moment}}');
                    strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{moment}');
                end
            else
                if type == 0
                    strequline = strcat(strequ, '\underline{M_{moment,0}}');
                    strequ = strcat(strequ, 'M_{moment, 0}');
                else
                    strequline = strcat(strequ, '\underline{M_{moment}}');
                    strequ = strcat(strequ, 'M_{moment}');
                end
            end
        else
            if i == 1 && h.S.N.end > 0 && type ~= 0
                yes = 1; sort = 'X1'; 
                X1 = 1;
                strequline = strcat(strequ, '\underline{M_{',h.S.nrX1,'}}'); 
                strequ = strcat(strequ, 'M_{',h.S.nrX1,'}');
            elseif i == 2 && h.S.N.end == 2 && type ~= 0
                yes = 2; sort = 'X2';
                X2 = 1;
                strequline = strcat(strequ, {' '}, '+', {' '}, '\underline{M_{',h.S.nrX2,'}}'); 
                strequ = strcat(strequ, {' '}, '+', {' '}, 'M_{',h.S.nrX2,'}');
            else
                yes = 0;
            end
        end

        if yes
            load = load + 1;
            if pl > 1 || (pl > 0 && dl == 1) || (pl > 0 && m == 1) || (dl == 1 && m == 1) || (X1 == 1 && X2 == 1) || (X1 == 1 && pl > 0) || (X1 == 1 && dl == 1) || (X1 == 1 && m == 1) 
                set(h.obj.(strcat('equation',num2str(h.number-2))), 'Visible','Off');
                set(h.obj.(strcat('equation',num2str(h.number-2))).Children, 'Visible','Off');
                set(h.obj.(strcat(prevsort,num2str(h.number))), 'Visible','Off');
                set(h.obj.(strcat(prevsort,num2str(h.number))).Children, 'Visible','Off');
                h.lastbottompos = lastbottompos;
            end
            prevsort = sort;

            %EQUATION
            h = equation(Fig, h,char(strcat(strequline, '}')));

            %SMALL GRAPH WITH ONLY ONE PLOT AT TIME
            h.number = h.number + 1;
            if 1 + nrofgraphs*ceil(load/nrofgraphs) - nrofgraphs == load, smallnr = 0;
            elseif 2 + nrofgraphs*ceil(load/nrofgraphs) - nrofgraphs == load, smallnr = 1;
            elseif 3 + nrofgraphs*ceil(load/nrofgraphs) - nrofgraphs == load, smallnr = 2;
            elseif 4 + nrofgraphs*ceil(load/nrofgraphs) - nrofgraphs == load, smallnr = 3;
            end
            
            h.name = strcat('smallgraph',num2str(h.number));
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels');
            
            h.obj.(h.name).Position = [20 + (smallwidth+40)*smallnr, h.lastbottompos - smallheight + 5 - (smallheight + 20)*(ceil(load/nrofgraphs)-1) - 30*(ceil(load/(nrofgraphs*2))-1), smallwidth, smallheight];
            set(h.obj.(h.name),'xtick',[]);
            set(h.obj.(h.name),'ytick',[]);
            hold on
            
            if strcmp(sort,'pointload')
                M(x) = h.S.diagrams.pl(pl);
            elseif strcmp(sort,'distributedload')
                M(x) = h.S.diagrams.dl;
            elseif strcmp(sort,'moment')
                M(x) = h.S.diagrams.m;
            elseif strcmp(sort, 'X1')
                M(x) = h.S.M1*h.S.X1;
            else
                M(x) = h.S.M2*h.S.X2;
            end

            if type == 2 && (i == 1 || i == 2)
                h.smallgraph = fplot(h.obj.(h.name),-M(x),[0,l],'color',colorM,'Linewidth',2);
            else
                h.smallgraph = fplot(h.obj.(h.name),-M(x),[0,l],'color',colorM0,'Linewidth',2);
            end
            fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish
            if double(M(0)) ~= 0
                if type == 2 && (i == 1 || i == 2)
                    plot(h.obj.(h.name), [0,0],[0,-double(M(0))],'Color', colorM, 'Linewidth',2); 
                else
                    plot(h.obj.(h.name), [0,0],[0,-double(M(0))],'Color', colorM0, 'Linewidth',2); 
                end
            end
            if double(M(l)) ~= 0
                if type == 2 && (i == 1 || i == 2)
                    plot(h.obj.(h.name), [l,l],[0,-double(M(l))],'Color', colorM, 'Linewidth',2); 
                else
                    plot(h.obj.(h.name), [l,l],[0,-double(M(l))],'Color', colorM0, 'Linewidth',2); 
                end
            end

            h.obj.(h.name).YLim = h.obj.(h.name).YLim + [-1/10*l , 1/10*l];
            h.obj.(h.name).XLim = h.obj.(h.name).XLim + [-1/10*l , 1/10*l];
            set(h.obj.(h.name),'xcolor',[0.5 0.5 0.5]);
            set(h.obj.(h.name),'ycolor',[0.5 0.5 0.5]);
            box on;

            ylim = h.obj.(h.name).YLim;

            if ylim(2) > ylim(1)
                posplus = ylim(1)-(ylim(1)-ylim(2))/2;
            else
                posplus = ylim(2)+(ylim(1)-ylim(2))/2;
            end
            
%             w = 5/12;
%             if load ~= total
%                 text(h.obj.(h.name), l + 2.5/20 + w*l, posplus, {'+'},'Interpreter','latex','HorizontalAlignment','right', 'FontSize', 14, 'FontWeight', 'bold');
%             end
    if load < total 
            plusboxsize = (20 + (smallwidth+40)*(smallnr+1) - h.obj.(h.name).Position(1) - smallwidth);
            plusboxposition = h.obj.(h.name).Position(1) + smallwidth;
            plus = axes('Parent', h.(h.panel),'units','pixels');
            plus.Position = [plusboxposition, h.obj.(h.name).Position(2), plusboxsize, h.obj.(h.name).Position(4)];
            set(plus,'xtick',[]);
            set(plus,'ytick',[]);
            plus.YLim = [0, 1];
            plus.XLim = [0, 1];
            set(plus,'xcolor',[1 1 1]);
            set(plus,'ycolor',[1 1 1]);
            hold on
            text(plus, 1/2, 1/2, {'+'},'Interpreter','latex','HorizontalAlignment','center', 'FontSize', 14, 'FontWeight', 'bold');
    end
            h.lastbottompos = h.obj.(h.name).Position(2) - ceil(h.load/nrofgraphs-1)*(smallheight+20);
            lastbottomposbiggraph = h.obj.(h.name).Position(2);

            %BIG GRAPH WITH SUPERPOSITION
            Msup(x) = Msup + M;
            h.superposition = true;
            h = dograph(Fig, h,type, Msup(x), false, sort);

            set(h.obj.(strcat(sort,num2str(h.number))), 'Visible', 'On');
            set(h.obj.(strcat(sort,num2str(h.number))).Children, 'Visible', 'On');

            pause(h.pausesuperposition);
            if load == total
                set(h.obj.(strcat('equation',num2str(h.number-2))), 'Visible','Off');
                set(h.obj.(strcat('equation',num2str(h.number-2))).Children, 'Visible','Off');
                set(h.obj.(strcat(prevsort,num2str(h.number))), 'Visible','Off');
                set(h.obj.(strcat(prevsort,num2str(h.number))).Children, 'Visible','Off');
                h.lastbottompos = lastbottompos;
                h = equation(Fig, h,char(strcat(strequ, '}')));
                h.lastbottompos = lastbottomposbiggraph;
                if type == 0
                    h.superposition = true;
                    h = dograph(Fig, h,type, h.S.M0(x), true, sort);
                else
                    h.superposition = true;
                    h = dograph(Fig, h,type, h.S.Mtot(x), true, sort);
                end
                set(h.obj.(strcat(sort,num2str(h.number-1))), 'Visible', 'On');
                set(h.obj.(strcat(sort,num2str(h.number-1))).Children, 'Visible', 'On');
                set(h.obj.(strcat(sort,num2str(h.number))), 'Visible', 'On');
                set(h.obj.(strcat(sort,num2str(h.number))).Children, 'Visible', 'On');
                if h.interactive
                    pause(h.pause+2);
                else
                    pause(0.2);
                end
                break;
            end
        end

    end
end
end