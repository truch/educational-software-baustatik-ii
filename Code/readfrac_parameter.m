function frac = readfrac_parameter(str,l)
format rat
lpos = findstr('l',str);
if isempty(lpos), lpos = findstr('L',str); end
slashpos = findstr('/',str);
timespos = findstr('*',str);
%find numerator
if not(isempty(slashpos)) %string is a fraction
    if not(isempty(lpos)) % string is parametric
        numeratorstr = [];
        denominatorstr = [];
        for i = 1 : slashpos -1
            if not(strcmp(str(i),'l')) && not(strcmp(str(i),'L')) && not(strcmp(str(i),'*'))
                numeratorstr = strcat(numeratorstr,str(i));
            end
        end
        if isempty(numeratorstr)
            numeratorstr = '1';
        end
        for i = slashpos + 1 : numel(str)
            if not(strcmp(str(i),'l')) && not(strcmp(str(i),'L')) && not(strcmp(str(i),'*'))
                denominatorstr = strcat(denominatorstr,str(i));
            end
        end
        frac = str2double(numeratorstr)/str2double(denominatorstr)*l;
    else
        frac = readfrac(str);
    end
else
    if not(isempty(lpos))
        fracstr = [];
        for i = 1 : numel(str)
           if not(strcmp(str(i),'l')) && not(strcmp(str(i),'L')) && not(strcmp(str(i),'*'))
               fracstr = strcat(fracstr, str(i));
           end
        end
        if isempty(fracstr)
           fracstr = '1';
        end
        frac = str2double(fracstr)*l;
    else
        frac = str2double(str);
    end
end
end