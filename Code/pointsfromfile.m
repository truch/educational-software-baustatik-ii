function result = pointsfromfile(handles)
for i = 1 : numel(handles.pointnames)
    currentpoint = char(handles.pointnames(i));
%defining the Symbol of the point plotted
    if strcmp(handles.points.(currentpoint).knottype,'hinge')
    pointsymbol = 'o';
    else pointsymbol = 'x';
    end


    if handles.scalexupper < handles.points.(currentpoint).r(1) %x-axis must be refitted towards +infty
        handles.scalexupper=handles.points.(currentpoint).r(1);
        scalechange_xu = true;
    else scalechange_xu = false;
    end
    if handles.scaleyupper < handles.points.(currentpoint).r(2) %y-axis must be refitted towards + infty
        handles.scaleyupper=handles.points.(currentpoint).r(2);
        scalechange_xl = true;
    else scalechange_xl = false;
    end
    if handles.scalexlower > handles.points.(currentpoint).r(1) %x-axis must be refitted towards -infty
        handles.scalexlower=handles.points.(currentpoint).r(1);
        scalechange_yu = true;
    else scalechange_yu = false;
    end
    if handles.scaleylower > handles.points.(currentpoint).r(2) %y-axis must be refitted towards -infty
       handles.scaleylower=handles.points.(currentpoint).r(2);
        scalechange_yl = true;
    else scalechange_yl = false;
    end
    axes(handles.axes1);
    hold on
    axis([handles.scalexlower-0.125*handles.imscale,handles.scalexupper+0.125*handles.imscale,handles.scaleylower-0.125*handles.imscale,handles.scaleyupper+0.125*handles.imscale]);
    axis equal
    handles.points.(currentpoint).pointmarker = ...
        plot(handles.points.(currentpoint).r(1),handles.points.(currentpoint).r(2),strcat(pointsymbol,'-k'),'MarkerFaceColor','w');
    uistack(handles.points.(currentpoint).pointmarker,'top');
    if or(strcmp(handles.points.(currentpoint).constrainttype,'spring'),strcmp(handles.points.(currentpoint).constrainttype,'torsional spring'))
        if strcmp(handles.points.(currentpoint).constrainttype,'torsional spring')
        if strcmp(handles.unit,'kN'), unit = '\frac{1}{kNm}';
        else unit = '\frac{l}{EI}'; end
        else
        if strcmp(handles.unit,'kN'), unit = '\frac{m}{kN}';
        else unit = '\frac{l^3}{EI}'; end
        end
        handles.points.(char(handles.pointnames(i))).stiffnesstext = text(handles.points.(currentpoint).r(1)+0.3,handles.points.(currentpoint).r(2)-0.3,{['$c_f =' num2str(handles.points.(currentpoint).stiffness) unit '$']},'Interpreter','latex');
    end
    handles.points.(currentpoint).textbox = text(handles.points.(currentpoint).r(1)+0.03*(handles.imscale)^0.8,handles.points.(currentpoint).r(2)-0.06*(handles.imscale)^0.8,strcat('$\raisebox{.5pt}{\textcircled{\raisebox{-.9pt}{',num2str(handles.points.(currentpoint).pointnr),'}}}$'),'Interpreter','latex');

    uistack(handles.points.(currentpoint).textbox,'top');
end
result = handles;
end