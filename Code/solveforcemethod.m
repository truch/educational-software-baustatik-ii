function handles = solveforcemethod(beam,handles)

format rat; % returns two integer matrices 

syms x M0(x) M1(x) M2(x) Mtot(x) q(x)

pa = handles.beams.(beam).beampoints(1);
pb = handles.beams.(beam).beampoints(2);
EI = handles.beams.(beam).EI;
ca = handles.points.(pointname(handles.beams.(beam).beampoints(1))).constrainttype;
cb = handles.points.(pointname(handles.beams.(beam).beampoints(2))).constrainttype;
l = handles.beams.(beam).l;
neighbours_a = numel(handles.points.(pointname(handles.beams.(beam).beampoints(1))).neighbours);
neighbours_b = numel(handles.points.(pointname(handles.beams.(beam).beampoints(2))).neighbours);
F = cell2mat(handles.beams.(beam).F);
qa = handles.beams.(beam).q1(1);
a = handles.beams.(beam).q1(2);
qb = handles.beams.(beam).q2(1);
b = handles.beams.(beam).q2(2);
qangle = handles.beams.(beam).qangle;
    qa = qa*cosd(qangle);
    qb = qb*cosd(qangle);
M = handles.beams.(beam).M(1);
sm = handles.beams.(beam).M(2);
cf_a = handles.points.(pointname(handles.beams.(beam).beampoints(1))).stiffness;           
cf_b = handles.points.(pointname(handles.beams.(beam).beampoints(2))).stiffness;
angleroller_a = handles.points.(pointname(handles.beams.(beam).beampoints(1))).constraintangle;
angleroller_b = handles.points.(pointname(handles.beams.(beam).beampoints(2))).constraintangle;
knottype_a = handles.points.(pointname(handles.beams.(beam).beampoints(1))).knottype;
knottype_b = handles.points.(pointname(handles.beams.(beam).beampoints(2))).knottype;
sh = handles.beams.(beam).hinge;
t = handles.beams.(beam).t;

S.pa = num2str(pa);
S.pb = num2str(pb);
S.point1 = strcat('point', num2str(pa));
S.point2 = strcat('point', num2str(pb));
S.point_1 = strcat('point', {' '}, num2str(pa));
S.point_2 = strcat('point', {' '}, num2str(pb));
S.delta1 = S.pa;
S.delta2 = S.pb;
S.nrX1 = strcat('X', num2str(pa));
S.nrX2 = strcat('X', num2str(pb));
S.nrX_1 = strcat('X_{', num2str(pa),'}');
S.nrX_2 = strcat('X_{', num2str(pb),'}');
S.M_1 = strcat('M_{', num2str(pa),'}');
S.M_2 = strcat('M_{', num2str(pb),'}');
S.M_1max = strcat('M_{', num2str(pa),',max}');
S.M_2max = strcat('M_{', num2str(pb),',max}');

S.intconst1 = [];
S.intconst2 = [];
intsol1 = [];
S.intsol2 = [];
S.cfval10 = [];
S.cftxt10 = [];
S.cfsol10 = [];
if F(1,1) ~= 0
    totalloads = 2 + numel(F(:,1));
else
    totalloads = 2;
end

S.l = l;
S.spl = F(:,2);
S.a = a;
S.b = b;
S.sm = sm;
S.sh = sh;
S.Mvalue = M;
F0 = F;

if (strcmp(ca,'none') || strcmp(ca, 'pin') || strcmp(ca, 'roller')) && strcmp(knottype_a,'connection') && neighbours_a > 1
    ca = 'fixed';
end

if strcmp(cb,'none') && strcmp(knottype_b,'connection') && neighbours_b > 1
    cb = 'fixed';
end

        if neighbours_a ~=1 
            cf_a = 0;
            h.explainspringdeformation_a = strcat(' EXPLAIN SPRING DEFORMATION...................');
            if strcmp(ca,'roller')
                h.explainconstraintchange_a = strcat('Because the first point has',{' '},'the coinstraint must be changed to a pin, because for the angle-balancing method the first knot is hold and the ...............................');
                ca = 'pin';
            end
        end

        if neighbours_b ~=1 
            cf_b = 0;
            h.explainspringdeformation_b = strcat(' EXPLAIN SPRING DEFORMATION...................');            
            if strcmp(cb,'roller')
                h.explainconstraintchange_b = strcat('Because the first point has',{' '},'the coinstraint must be changed to a pin, because for the angle-balancing method the first knot is hold and the ...............................');
                cb = 'pin';
            end
        end
        
        if strcmp(ca,'spring')
            cf_as = cf_a;
            cf_at = 0;
        elseif strcmp(ca,'torsional spring')
            cf_at = cf_a;
            cf_as = 0;
        else 
            cf_as = 0;
            cf_at = 0;
        end
              
        if strcmp(cb,'spring')
            cf_bs = cf_b;
            cf_bt = 0;
        elseif strcmp(cb,'torsional spring')
            cf_bt = cf_b;
            cf_bs = 0;
        else 
            cf_bs = 0;
            cf_bt = 0;            
        end
        S.cf_as = cf_as;
        S.cf_bs = cf_bs;
        S.cf_at = cf_at;
        S.cf_bt = cf_bt;
        
        if strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t)
               ca = 'none';
               knottype_a = 'free';
               h.explainconstraintchange_b = strcat('Because the first point has',{' '},'the coinstraint must be changed to a pin, because for the angle-balancing method the first knot is hold and the ...............................');               
        end
        
        if strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t)
               cb = 'none';
               knottype_b = 'free';
               h.explainconstraintchange_b = strcat('Because the first point has',{' '},'the coinstraint must be changed to a pin, because for the angle-balancing method the first knot is hold and the ...............................');               
        end
        
S.newconstrainta = ca;
S.newconstraintb = cb;
S.newknottypea = knottype_a;
S.newknottypeb = knottype_b;
        
% check if the beam is static determinated or not
N = 0;
N = unknowns(ca, N, angleroller_a, t,neighbours_a,knottype_a);
N = unknowns(cb, N, angleroller_b, t,neighbours_b,knottype_b);
S.N.constraints = N;
N = N-2; % 2 equations (M,V)

if sh>0
    N = N-1;
end
S.N.end = N;

if N < 0 
    msgbox('The system is a mechanism (N < 0).','Ische nite!','error');
end

if N == 2 
    S.releaseconstraint = strcat('Release a constraint at', {' '}, S.point_1,' and', {' '}, S.point_2,'.');
    S.pointreleased = strcat(S.point_1, {' '},' and', {' '}, S.point_2);
    S.rotationpoint = [1, 2];
    S.M1max = [-1,1];
elseif N == 1 && (strcmp(cb,'fixed') || strcmp(cb, 'torsional spring'))
    S.releaseconstraint = strcat('Release a constraint at ', {' '}, S.point_2, '.');
    S.pointreleased = S.point_1;
    S.rotationpoint = [2];
    S.M1max = [1, 0];
elseif N == 1 
    S.releaseconstraint = strcat('Release a constraint at ', {' '}, S.point_1, '.');
    S.pointreleased = S.point_2;
    S.rotationpoint = [1];
    S.M1max = [-1, 0];
else
    S.releaseconstraint = ' ';
    S.pointreleased = 0;
    S.rotationpoint = 0;
    S.M1max = 0;
end

if sh < 0 && N >= 0
    
    F = [zeros(2,3); F];
            
    [M1, M2, delta11, delta22, delta12] = moment12(ca, N, l, sh, cf_as, cf_bs, cf_at, cf_bt,EI);
    [M0, Vatot, Vbtot, diagrams] = solvemoment(l,F,a,b,qa,qb,sm,M,knottype_a,knottype_b);
    
    S.Vaidet = Vatot;
    S.Vbidet = Vbtot;
    
    delta10 = int(M0(x)*M1(x)/EI,x,0,l);
    delta20 = int(M0(x)*M2(x)/EI,x,0,l) - M0(0)*cf_at;
    
    if xor(strcmp(ca,'spring'),strcmp(cb,'spring'))
            delta10 = int(M0(x)*M1(x)/EI,x,0,l) - sum(Vbtot)*cf_bs/l;
            delta20 = int(M0(x)*M2(x)/EI,x,0,l) + sum(Vatot)*cf_as/l;
    end
      
syms s12 
syms t12 
syms s21

eqns = [1 == s12*delta11 + t12*delta12, ...
             0 == s12*delta12 + t12*delta22];
[S.s12, S.t12] = solve(eqns, [s12 t12]);

eqns = [0 == t12*delta11 + s21*delta12, ...
             1 == t12*delta12 + s21*delta22];
[S.s21, S.t12] = solve(eqns, [s21 t12]);

S.t12 = abs(S.t12);

syms X1
syms X2

eqns = [0 == delta10 + X1*delta11 + X2*delta12, ...
             0 == delta20 + X1*delta12 + X2*delta22];
[S.X1, S.X2] = solve(eqns, [X1 X2]);

S.Mtot(x) = M0(x) + S.X1*M1(x) + S.X2*M2(x);
S.M0(x) = M0(x);

S.Vatotdet = sum(Vatot);
S.Vbtotdet = sum(Vbtot);
S.Vatotindet  = sum(Vatot) + 1/l*S.X1 + 1/(l)*S.X2;
S.Vbtotindet  = sum(Vbtot) - 1/l*S.X1 - 1/(l)*S.X2;

elseif N >= 0
    
    Fleft = [0, 0, 0] ; Fright = [0, 0, 0] ;
    qaleft = 0; qaright = 0; qbleft = 0; qbright = 0;
    aleft = 0; aright = 0; bleft = 0; bright= 0;
    Mleft = 0; Mright = 0;
    
    if any(F(:,1)~=0)
        Fleft = F.*(F(:,2)<sh); %find out which point loads are on the left side of the beam
        Fleft(Fleft(:,2)==0,:) = []; %clear point loads of the right side from the loads matrix => matrix with points loads only of the left side
        if isempty(Fleft)
            Fleft = [0 0 0];
        end
        
        Fright = F.*(F(:,2)>sh);
        if any(Fright(:,1)) ~= 0
            Fright(Fright(:,2)==0,:) = [];
            if isempty(Fright)
                Fright = [0 0 0];
            end
            Fright(:,2) = Fright(:,2) - sh; % move all pointloads so that the hinge (sh) is the new beginning of the beam 
        end
        
          % sh = load position
          if strcmp(ca,'fixed') || strcmp(ca,'torsional spring')    
            Fhinge = F.*(F(:,2)==sh); 
            Fhinge(Fhinge(:,2)==0,:) = []; 
            if Fleft(1,1) == 0 && numel(Fleft(:,1)) > 1
                Fleft = [];
            end
            Fleft = [Fhinge;Fleft];
          else
            Fhinge = F.*(F(:,2)==sh); 
            Fhinge(Fhinge(:,2)==0,:) = []; 
            if Fright(1,1) == 0
                Fright = [];
            end
            Fhinge(:,2) = Fhinge(:,2) - sh;
            Fright = [Fhinge;Fright];
          end
          
           fr = numel(Fright(:,1));
           fl = numel(Fleft(:,1));
    else
        fr = 1; 
        fl = 1;
    end

     if not(qa==0 && qb==0)
         
        q(x) = (qb-qa)/(b-a)*(x-a)+qa;
        
         if (a < sh) && (b <= sh)
             qaleft = qa;
             qbleft = qb;
             qaright = 0;
             qbright = 0;
             aleft = a;
             bleft = b;
             aright = 0;
             bright = 0;
         elseif (a < sh) && (b > sh)
             qaleft = qa;
             qbleft = q(sh);
             qaright = q(sh);
             qbright = qb;
             aleft = a;
             bleft = sh;
             aright = 0;
             bright = b-sh;
         else %if (a > sh) && (b > sh)
             qaleft = 0;
             qbleft = 0;
             qaright = qa;
             qbright = qb;
             aleft = 0;
             bleft = 0;
             aright = a-sh;
             bright = b-sh;
         end
     end
     
     if M~=0
         if sh > sm
             Mleft = M;
             Mright = 0;
         elseif sh < sm
             Mleft = 0;
             Mright = M;
         else % sh = sm
              if strcmp(ca,'fixed') || strcmp(ca,'torsional spring')    
                 Mleft = M;
                 Mright = 0;
              else
                  Mleft = 0;
                  Mright = M;
              end
         end
     end
              
     if strcmp(ca,'fixed') || strcmp(ca,'torsional spring')              
         Fright = [zeros(2,3); zeros(fl,3); Fright]; % add new row at the end with zeros, so that the point load function will work 
         
         [M0_right, Vatot, Vbtot, diagrams_right] = solvemoment(l-sh,Fright,aright,bright,qaright,qbright,sm-sh,Mright,'hinge','hinge');
         
         S.Vbidet = Vbtot;    
         
         Fleft = [Vatot(1:2), ones(2,1)*sh, zeros(2,1); Fleft; Vatot(3+fl:numel(Vatot)), ones(fr,1)*sh, zeros(fr,1)];
         
         [M0_left, Vatot, ~, diagrams_left] = solvemoment(sh,Fleft,aleft,bleft,qaleft,qbleft,sm,Mleft,knottype_a,'free');
         
         S.Vaidet = Vatot;
     else
         Fleft = [zeros(2,3); zeros(fr,3); Fleft]; % add new row at the end with zeros, so that the point load function will work 
         
         [M0_left, Vatot, Vbtot, diagrams_left] = solvemoment(sh,Fleft,aleft,bleft,qaleft,qbleft,sm,Mleft,'hinge','hinge');
         
         S.Vaidet = Vatot;

         Fright = [Vbtot(1:2), zeros(2,1), zeros(2,1); Fright; Vbtot(3+fr:numel(Vbtot)), ones(fl,1)*0, zeros(fl,1)];

         [M0_right, ~, Vbtot, diagrams_right] = solvemoment(l-sh,Fright,aright,bright,qaright,qbright,sm-sh,Mright,'free',knottype_b);
         
         S.Vbidet = Vbtot;

     end
     
     if ~(all(F(:,1)) == 0)
         Fright(:,2) = Fright(:,2) + sh;
         
         if S.rotationpoint == 2
%              if fr == 1 && Fright(numel(Fright(:,1)), 1) == 0
%                  fr = 0;
%              end
% 
%              if fl == 1 && Fleft(3, 1) == 0
%                  fl = 0;
%              end
             F = double([Fleft(3:fl+2,:); Fright(fl+3:fl+fr+2,:)]);
         else
            F = double([Fright(fl+2:fl+fr+2,:); Fleft(3:fr+2,:)]);
         end
         if any(F(:,1)~=0)
            F(F(:,1)==0,:) = [];
        end
         F = mat2cell(F,ones(1,numel(F(:,1))),ones(1,numel(F(1,:))));
%                 for i = 1: numel(F)
%                     F{i} = num2str(F{i});
%                 end
         handles.beams.(beam).F = F;
         F0  = cell2mat(F);
     end
     
     M0(x) = piecewise(x>=0 & x<sh, M0_left(x), x>=sh & x<=l, M0_right(x-sh));   
          
     syms dleft(x) dright(x)

      if any(F0(:,1)) == 0
          diagrams.pl = piecewise(x>=0 & x<=l, 0); 
      else         
          for i=1:numel(F0(:,1))
              dleft(x) = diagrams_left.pl(i);
              dright(x) = diagrams_right.pl(i);
              diagrams.pl(i) = piecewise(x>=0 & x<sh, dleft(x), x>=sh & x<=l, dright(x-sh));  
          end
      end
     
     if qa==0 && qb==0
         diagrams.dl = piecewise(x>=0 & x<=l, 0); 
     else
         dleft(x) = diagrams_left.dl;
         dright(x) = diagrams_right.dl;
         diagrams.dl = piecewise(x>=0 & x<sh, dleft(x), x>=sh & x<=l, dright(x-sh));  
     end
     
     if M==0
         diagrams.m = piecewise(x>=0 & x<=l, 0); 
     else
         dleft(x) = diagrams_left.m;
         dright(x) = diagrams_right.m; 
         diagrams.m = piecewise(x>=0 & x<sh, dleft(x), x>=sh & x<=l, dright(x-sh));  
     end
     
        [M1, M2, delta11, delta22, delta12] = moment12(ca, N, l, sh, cf_as, cf_bs, cf_at, cf_bt,EI);
        delta10 = 0;     
        
     if xor((strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) , (strcmp(cb,'fixed') || strcmp(cb,'torsional spring')))
        delta20 = 0;
     else
        delta20 = int(M0(x)*M2(x)/EI,x,0,l) + cf_at*M0(0)*M2(0);
     end
     
syms s12 
syms t12 
syms s21

     S.s21 = solve(1 == s21*delta22, s21);
     S.t12 = abs(S.s21 * M2(0));
     
syms X1
syms X2

eqns = [0 == delta10 + X1*delta11 + X2*delta12, ...
             0 == delta20 + X1*delta12 + X2*delta22];
[S.X1, S.X2] = solve(eqns, [X1 X2]);

S.Mtot(x) = M0(x) + S.X1*M1(x) + S.X2*M2(x);
S.M0(x) = M0(x);

     
    % mirror system
     can = cb; cbn = ca; cf_asn = cf_bs; cf_atn = cf_bt; cf_bsn = cf_as; cf_btn = cf_at; shn = l-sh;
     
     if xor((strcmp(can,'fixed') || strcmp(can,'torsional spring')) , (strcmp(cbn,'fixed') || strcmp(cbn,'torsional spring')))
        [M1m, M2m, delta11m, delta22m, delta12m] = moment12(can, N, l, shn, cf_asn, cf_bsn, cf_atn, cf_btn, EI);
     else
        [M1m, M2m, delta11m, delta22m, delta12m] = moment12(can, N, l, shn, cf_asn, cf_bsn, cf_atn, cf_btn, EI);
     end
     S.s12 = solve(1 == s12*delta22m, s12);

S.Vatotdet = sum(S.Vaidet);
S.Vbtotdet = sum(S.Vbidet);
S.Vatotindet  = sum(S.Vaidet) + 1/sh*S.X1 + 1/(l-sh)*S.X2;
S.Vbtotindet  = sum(S.Vbidet) - 1/sh*S.X1 - 1/(l-sh)*S.X2;
end

if isempty(S.s12), S.s12 = 0; end
if isempty(S.s21), S.s21 = 0; end
if isempty(S.t12), S.t12 = 0; end
if isempty(S.X1), S.X1 = 0; end
if isempty(S.X2), S.X2 = 0; end

S.M1 = M1;
S.M2 = M2;
S.delta10 = delta10;
S.delta20 = delta20;
S.delta11 = delta11;
S.delta22 = delta22;
S.delta12 = delta12;

M000(x) = piecewise(x>=0 & x<=l, 0);

if S.N.end == 1
    if isequal(S.M1(x), M000(x))
        S.change = true;
        [S.X1, S.X2] = deal(S.X2, S.X1);
        [S.M1, S.M2] = deal(S.M2, S.M1);
        [delta11, delta22] = deal(delta22, delta11);
        [S.cf_as, S.cf_bs] = deal(S.cf_bs, S.cf_as);
        [S.cf_at, S.cf_bt] = deal(S.cf_bt, S.cf_at);
        [S.delta1, S.delta2] = deal(S.delta2, S.delta1);
        [S.nrX1, S.nrX2] = deal(S.nrX2, S.nrX1);
        [S.nrX_1, S.nrX_2] = deal(S.nrX_2, S.nrX_1);
        [S.M_1, S.M_2] = deal(S.M_2, S.M_1);
        [S.M_1max, S.M_2max] = deal(S.M_2max, S.M_1max);
        [S.delta10, S.delta20] = deal(S.delta20, S.delta10);
        [S.delta11, S.delta22] = deal(S.delta22, S.delta11);
    end
end

for i = 1 : totalloads
    if i == 1
        M = diagrams.dl(1);
        M(x) = M;
    elseif i == 2
        M = diagrams.m(1);
        M(x) = M;
    else
        M(x) = diagrams.pl(i-2);
    end

    if S.N.end == 2
        S.intconst2 = [S.intconst2; abs(int(M(x)/S.M1max(2)*S.M2(x),0,l))];
        S.intsol2 = [S.intsol2; int(M(x)*S.M2(x),0,l)];
    else
        S.intconst2 = [S.intconst2; 0];
        S.intsol2 = [S.intsol2; 0];
    end

    if cf_as ~= 0 && N == 1
        if cf_bt ~= 0
            S.cfval11 = strcat('+ \underbrace{(', checkfrac(double(S.M2(l))), ')^2}_{\mathrm{M_{', S.point2,', ', S.nrX1,'}}} \cdot',checkfrac(cf_bt),'+ \underbrace{\left(\frac{1}{L}\right)^2}_{\mathrm{F_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_as), '}_{\mathrm{c_{f,spring}}}');
        else
            S.cfval11 = strcat('+ \underbrace{\left(\frac{1}{L}\right)^2}_{\mathrm{F_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_as), '}_{\mathrm{c_{f,spring}}}');
        end
        S.cfval10 = [S.cfval10; {strcat('+ \underbrace{', checkfrac(S.Vaidet(i)), '}_{\mathrm{F_{', S.point1,', 0}}} \cdot \underbrace{\frac{1}{L}}_{\mathrm{F_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_as), '}_{\mathrm{c_{f,spring}}}')}];
        S.cfsol10 = [S.cfsol10; S.Vaidet(i)*1/l*cf_as];
        S.cfsol11 = 0;
        S.cfval22 = [];
        S.cfsol22 = 0;
    elseif cf_bs ~= 0 && N == 1
        if cf_at ~= 0
            S.cfval11 = strcat('+ \underbrace{\left(', checkfrac(double(S.M1(0))), '\right)^2}_{\mathrm{M_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_at),'}_{\mathrm{c_{f,torsional spring}}}+ \underbrace{\left(-\frac{1}{L}\right)^2}_{\mathrm{F_{', S.point2,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_bs), '}_{\mathrm{c_{f,spring}}}');
        else
            S.cfval11 = strcat('+ \underbrace{\left(-\frac{1}{L}\right)^2}_{\mathrm{F_{', S.point2,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_bs), '}_{\mathrm{c_f, spring}}');
        end
        S.cfval10 = [S.cfval10; {strcat('+ \underbrace{', checkfrac(S.Vbidet(i)), '}_{\mathrm{F_{', S.point2,', 0}}} \cdot \underbrace{-\frac{1}{L}}_{\mathrm{F_{', S.point2,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_bs), '}_{\mathrm{c_{f,spring}}}')}];
        S.cfsol10 = [S.cfsol10; S.Vbidet(i)*-1/l*cf_bs];
        S.cfsol11 = delta11;
        S.cfval22 = [];
        S.cfsol22 = 0;
    elseif cf_at ~= 0 && N == 1 && S.rotationpoint(1) == 1
        S.cfval10 = [S.cfval10; {' '}];
        S.cfsol10 = [S.cfsol10; 0];
        S.cfval11 = strcat('+ \underbrace{(', checkfrac(double(S.M1(0))), ')^2}_{\mathrm{M_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_at), '}_{\mathrm{c_{f,torsional spring}}}');
        S.cfsol11 = double(S.M1(0))*cf_at;
        S.cfval22 = [];
        S.cfsol22 = 0;
    elseif cf_at ~= 0 && S.rotationpoint(1) == 2
        S.cfval10 = [S.cfval10; {strcat('+ \underbrace{', checkfrac(double(M(0))), '}_{\mathrm{M_{', S.point1,', 0}}}\cdot \underbrace{', checkfrac(S.M1(0)), ' }_{\mathrm{M_{', S.point1,', ', S.nrX1,'}}}\cdot \underbrace{',checkfrac(cf_at), '}_{\mathrm{c_{f,torsional spring}}}')}];
        S.cfsol10 = [S.cfsol10; M(0)*S.M1(0)*cf_at];
        S.cfval11 = strcat('+ \underbrace{(', checkfrac(double(S.M1(0))), ')^2}_{\mathrm{M_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_at), '}_{\mathrm{c_{f,torsional spring}}}');
        S.cfsol11 = double(S.M1(0))*cf_at;
        S.cfval22 = [];
        S.cfsol22 = 0;
    elseif cf_bt ~= 0 && N == 1
        S.cfval10 = [S.cfval10; {' '}];
        S.cfsol10 = [S.cfsol10; 0];
        S.cfval11 = strcat('+ \underbrace{(', num2str(double(S.M1(l))), ')^2}_{\mathrm{M_{', S.point2,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_bt), '}_{\mathrm{c_{f,torsional spring}}}');
        S.cfsol11 = double(S.M2(l))*cf_bt;
        S.cfval22 = [];
        S.cfsol22 = 0;
    elseif cf_bt ~= 0 && N == 2
        S.cfval10 = [S.cfval10; {' '}];
        S.cfsol10 = [S.cfsol10; 0];
        S.cfval11 = [];
        S.cfsol11 = 0;
        S.cfval22 = strcat('+ \underbrace{(', num2str(double(S.M2(l))), ')^2}_{\mathrm{M_{', S.point2,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_bt), '}_{\mathrm{c_{f,torsional spring}}}');
        S.cfsol22 = double(S.M2(l))*cf_bt;
    elseif cf_at ~= 0 && cf_bt ~= 0 && N == 2
        S.cfval10 = [S.cfval10; {' '}];
        S.cfsol10 = [S.cfsol10; 0];
        S.cfval11 = strcat('+ \underbrace{(', num2str(double(S.M1(0))), ')^2}_{\mathrm{M_{', S.point1,', ', S.nrX1,'}}} \cdot \underbrace{',checkfrac(cf_at), '}_{\mathrm{c_{f,torsional spring}}}');
        S.cfsol11 = double(S.M1(0))*cf_at;
        S.cfval22 = strcat('+ \underbrace{(', num2str(double(S.M2(l))), ')^2}_{\mathrm{M_{', S.point2,', ', S.nrX2,'}}} \cdot \underbrace{',checkfrac(cf_bt), '}_{\mathrm{c_{f,torsional spring}}}');
        S.cfsol22 = double(S.M2(l))*cf_bt;
    else
        S.cfval10 = [S.cfval10; {' '}];
        S.cfsol10 = [S.cfsol10; 0];
        S.cfval11 = [];
        S.cfsol11 = 0;
        S.cfval22 = [];
        S.cfsol22 = 0;
    end
end

S.M1max_l = S.M1(0);
S.M1max_r = S.M1(l);
S.M2max_r = S.M2(l);

if sm ~= 0
    S.M1max_sm = S.M1(sm);
    S.M2max_sm = S.M2(sm);
    if sh > 0
        S.M1max_sh = S.M1(sh);
    end
end

S.intconst11 = abs(int(S.M1(x)^2,0,l));
S.intsol11 = S.delta11;
S.intconst22 = abs(int(S.M2(x)^2,0,l));
S.intsol22 = S.delta22;
S.intconst12 = abs(int(S.M1(x)*S.M2(x)/S.M1(0)/S.M2(l)/l,0,l));
S.intsol12 = S.delta12;

S.M1X1 = S.M1 * S.X1;
S.M2X2 = S.M2 * S.X2;

%Moment line for the static indetermined structure
for i=1:numel(F0(:,1))
    diagrams01.pl(i) = diagrams.pl(i) + S.M1X1;
    diagrams02.pl(i) = diagrams.pl(i) + S.M2X2;
    diagramstot.pl(i) = diagrams.pl(i) + S.M1X1 + S.M2X2;
end
diagrams01.dl = diagrams.dl + S.M1X1;
diagrams02.dl = diagrams.dl + S.M2X2;
diagramstot.dl = diagrams.dl + S.M1X1 + S.M2X2;
diagrams01.m = diagrams.m + S.M1X1;
diagrams02.m = diagrams.m + S.M2X2;
diagramstot.m = diagrams.m + S.M1X1 + S.M2X2;

S.diagrams01 = diagrams01;
S.diagrams02 = diagrams02;
S.diagramstot = diagramstot;

%Values of the max./min. Moment of the point loads for the primary
%structure and the indetermined structure
if F0(1,1) ~= 0
    for i=1:numel(F0(:,1))
        Mload0(x) = diagrams.pl(i);
        S.Mloads0(i) = Mload0(S.spl(i));
        if N > 0
            Mloadtot(x) = diagramstot.pl(i);
            S.Mloadstot(i) = Mloadtot(S.spl(i));
            S.Mloadstotat0(i) = Mloadtot(0);
            S.Mloadstotatl(i) = Mloadtot(l);
        end
    end
end

if a~=0 || b~=0
    S.xmaxmin0 = double(solve(diff(diagrams.dl(1)) == 0, x));
    Msecdiff(x) = diff(diff(diagrams.dl(1)));
    if isempty(S.xmaxmin0)
        if strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring')
            S.xmaxmin0 = 0;
        else
            S.xmaxmin0 = l;
        end
    end
    S.M0xmaxmin = ones(1,numel(S.xmaxmin0));
    S.highlow0 = ones(1,numel(S.xmaxmin0));
    S.M0dl(x) = diagrams.dl(1);
    for i=1:numel(S.xmaxmin0)
        S.M0xmaxmin(i) = double(S.M0dl(S.xmaxmin0(i)));
        if numel(S.xmaxmin0) > 1
            if Msecdiff(S.xmaxmin0(i)) > 0
                S.highlow0(i) = 1; %high point
            else
                S.highlow0(i) = -1; %low point
            end
        else
            S.highlow0(i) = 0;
        end
    end        
end

if M ~= 0
    Mvalue0(x) = diagrams.m(1);
    S.Mvalue0at0 = Mvalue0(0);
    S.Mvalue0atl = Mvalue0(l);
end

S.Mtot_0 = S.Mtot(0);
S.Mtot_l = S.Mtot(l);

S.diagrams = diagrams;

handles.beams.(beam).S = S;
end
             % L�SCHEN:
%              figure (1) ;
%              clf; hold on; fplot(-S.Mtot(x),[0,l],'Linewidth',2);fplot(0, [0,l],'-k');
%                           
%              
%              fplot(-diff(M0(x)),[0,l],'Linewidth',2);fplot(0, [0,l],'-k')
%              
%              xmaxmin = double(solve(diff(S.Mtot(x)) == 0,x));
%              for i=1:numel(xmaxmin)
%                  Mmaxmin = S.Mtot(xmaxmin(i));
%                  if Mmaxmin >= 0
%                     text(xmaxmin(i),-Mmaxmin,{' ',' ', ['$' latex(S.Mtot(xmaxmin)) '$ $ql^2$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top')
%                  else
%                      text(xmaxmin(i),-Mmaxmin,{['$' latex(S.Mtot(xmaxmin)) '$ $ql^2$'],' ',' '},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom')
%                  end
%              end

                  
% end