function result = distributedload(handles)

set(getpoints,'currentaxes',handles.axes1);
hold on;

%%Script for plotting the loads of an
%%inclined beam with an arbitrary angle subjected to a trapezoidal load

if strcmp(handles.unit,'kN'), unit = 'kN/m';
elseif strcmp(handles.unit,'q'), unit = 'q';
elseif strcmp(handles.unit,'Q'), unit = 'Q/l';
else unit = 'M/l^2'; end

qa = handles.beams.(char(handles.beamnames(handles.temp))).q1(1);
qb = handles.beams.(char(handles.beamnames(handles.temp))).q2(1);

if or(qa~=0, qb~=0)
    ra = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r;
    rb = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).r;
    t = handles.beams.(char(handles.beamnames(handles.temp))).t;
    n = handles.beams.(char(handles.beamnames(handles.temp))).n;
    l = handles.beams.(char(handles.beamnames(handles.temp))).l;
    sa=handles.beams.(char(handles.beamnames(handles.temp))).q1(2);
    sb=handles.beams.(char(handles.beamnames(handles.temp))).q2(2);

    rb=ra+sb*t;
    ra=ra+sa*t;

    rab = rb-ra;    % vector connecting the two ends of the beam

    l  = norm(rab); % length of the beam computed as the norm of the vector connecting its endpoints

    %%Definition of the applied trapezoidal load
    % Value of the distributed trapezoidal node at a point s defined as an
    % inline function

    q = @(s) qa + (qb-qa)*s/l;

    % Computation of the load at different points along the beam
    % An interval of length l/20 is used to define the points

    m = ceil(l/(handles.imscale/25));
    si = 0:(l/m):l;  % Length along the beam
    qi = q(si);     % Evaluation of the load using the defined inline function

    %%Plotting of loads

    % Definition of the scale used for plotting loads
    scale1 = 0.15*l/max(abs(qi));

    % Unit vectors tangent and normal to the beam
    t = rab/l;

    if handles.qdirection == 1
        n = [t(2); -t(1)];
    elseif handles.qdirection == 2
        n = [0; -1];
    elseif handles.qdirection == 3
        n = [1;0];
    end
    
    handles.beams.(char(handles.beamnames(handles.temp))).qdirection = n;

    if (qa>=0) && (qb>=0)
            rqt = ra + t*si - 0.1*n - scale1*n*abs(qi); % points defining the top of the distributed load
            rqb = ra + t*si - 0.1*n; % points defining the bottom of the distributed load (used for plotting the arrows)

            patch_q = [ra-0.1*n rqt rb-0.1*n]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1,'bottom');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*n(1)*abs(qi),scale1*n(2)*abs(qi),0,'r');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1 = text(rqt(1,1)+0.005*handles.imscale^0.8,rqt(2,1)+0.05*handles.imscale^0.8,{['$' checkfrac(qa) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2 = text(rqt(1,numel(si))+0.005*handles.imscale^0.8,rqt(2,numel(si))+0.05*handles.imscale^0.8,{['$' checkfrac(qb) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1.Color = 'red';
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2.Color = 'red';

    elseif (qa<0) && (qb<0)
            rqt = ra + t*si + 0.1*n + scale1*n*abs(qi); % points defining the top of the distributed load
            rqb = ra + t*si + 0.1*n; % points defining the bottom of the distributed load (used for plotting the arrows)

            patch_q = [ra+0.1*n rqt rb+0.1*n]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1,'bottom');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*-n(1)*abs(qi),scale1*-n(2)*abs(qi),0,'r');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1 = text(rqt(1,1)+0.005*handles.imscale^0.8,rqt(2,1)+0.05*handles.imscale^0.8,{['$' checkfrac(qa) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2 = text(rqt(1,numel(si))+0.005*handles.imscale^0.8,rqt(2,numel(si))+0.05*handles.imscale^0.8,{['$' checkfrac(qb) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1.Color = 'red';
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2.Color = 'red';

    else
        if qa<0
        %select only positive loads and set all negatives loads to 0
            qi1=qi;

            rqt = ra + t*si(qi1<0) +0.1*n + scale1*n*abs(qi1(qi1<0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi1<0) + 0.1*n; % points defining the bottom of the distributed load (used for plotting the arrows)
            
            rbneu=ra+t*si(numel(qi1(qi1<0)));
            patch_q = [ra+0.1*n rqt rbneu+0.1*n]; % Coordinates of points defining the patch used to visualize the load
            
            % Plotting of the load.

            % Plot a patch representing the load
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1,'bottom');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*-n(1)*abs(qi1(qi1<0)),scale1*-n(2)*abs(qi1(qi1<0)),0,'r');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1 = text(rqt(1,1)+0.005*handles.imscale^0.8,rqt(2,1)+0.05*handles.imscale^0.8,{['$' checkfrac(qa) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1.Color = 'red'; 

        %select only negative loads and set all positive loads to 0    
            qi2=qi;

            raneu = rb - t*si(numel(qi2(qi2>=0)));
            rqt = ra + t*si(qi2>=0) - 0.1*n - scale1*n*abs(qi2(qi2>=0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi2>=0) - 0.1*n; % points defining the bottom of the distributed load (used for plotting the arrows)
            
            patch_q = [raneu-0.1*n rqt rb-0.1*n]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load
            
            % Plot a patch representing the load
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch2 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch2,'bottom');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qquiver2 = quiver(rqt(1,:),rqt(2,:),scale1*n(1)*abs(qi2(qi2>=0)),scale1*n(2)*abs(qi2(qi2>=0)),0,'r');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2 = text(rqt(1,numel(rqt(1,:)))+0.005*handles.imscale^0.8,rqt(2,numel(rqt(1,:)))+0.05*handles.imscale^0.8,{['$' checkfrac(qb) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2.Color = 'red';
            
        else %if qb<0
         %select only positive loads and set all negative loads to 0
            qi1=qi;

            rqt = ra + t*si(qi1>=0) - 0.1*n - scale1*n*abs(qi1(qi1>=0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi1>=0) - 0.1*n; % points defining the bottom of the distributed load (used for plotting the arrows)

            rbneu=ra+t*si(numel(qi1(qi1>=0)));
            patch_q = [ra-0.1*n rqt rbneu-0.1*n]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch1,'bottom');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*n(1)*abs(qi1(qi1>=0)),scale1*n(2)*abs(qi1(qi1>=0)),0,'r');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1 = text(rqt(1,1)+0.005*handles.imscale^0.8,rqt(2,1)+0.05*handles.imscale^0.8,{['$' checkfrac(qa) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext1.Color = 'red';

        %select only negative loads and set all positive loads to 0    
            qi2=qi;

            raneu = rb - t*si(numel(qi2(qi2<0)));
            rqt = ra + t*si(qi2<0) + 0.1*n + scale1*n*abs(qi2(qi2<0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi2<0) + 0.1*n; % points defining the bottom of the distributed load (used for plotting the arrows)

            patch_q = [raneu+0.1*n rqt rb+0.1*n]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch2 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).loads.qpatch2,'bottom');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qquiver2 = quiver(rqt(1,:),rqt(2,:),scale1*-n(1)*abs(qi2(qi2<0)),scale1*-n(2)*abs(qi2(qi2<0)),0,'r');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2 = text(rqt(1,numel(rqt(1,:)))+0.005*handles.imscale^0.8,rqt(2,numel(rqt(1,:)))+0.05*handles.imscale^0.8,{['$' checkfrac(qb) '\ ' unit '$']},'Interpreter','latex');
            handles.beams.(char(handles.beamnames(handles.temp))).loads.qtext2.Color = 'red';
        end
    end
result = handles;
end