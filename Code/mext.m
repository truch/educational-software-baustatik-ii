function result = mext(handles)

if strcmp(handles.unit,'kN'), unit = 'kNm';
elseif strcmp(handles.unit,'q'), unit = 'ql^2';
elseif strcmp(handles.unit,'Q'), unit = 'Ql';
else unit = 'M'; end

if handles.beams.(char(handles.beamnames(handles.temp))).M(1) ~= 0
    M = handles.beams.(char(handles.beamnames(handles.temp))).M(1);
    s = handles.beams.(char(handles.beamnames(handles.temp))).M(2);
    t = handles.beams.(char(handles.beamnames(handles.temp))).t;
    n = handles.beams.(char(handles.beamnames(handles.temp))).n;
    ra = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r;
    th = linspace( pi/2, -pi, 100);
    R = 0.03*(handles.imscale)^0.8;
    x = R*cos(th) + ra(1) + t(1)*s;
    y = R*sin(th) + ra(2) + t(2)*s;
    handles.beams.(char(handles.beamnames(handles.temp))).loads.Mstar = plot(ra(1)+t(1)*s,ra(2)+t(2)*s,'*-r');
    handles.beams.(char(handles.beamnames(handles.temp))).loads.Mimg = plot(x,y,'r','LineWidth',1.5);
    handles.beams.(char(handles.beamnames(handles.temp))).loads.Mtext = text(ra(1) + t(1)*0.6*s - R*n(1),ra(2) + t(2)*0.6*s - R*n(2), {['$' checkfrac(M) '\ ' unit '$']},'Interpreter','latex');
    handles.beams.(char(handles.beamnames(handles.temp))).loads.Mtext.Color = 'red';
    if sign(M) == 1 %M>0
        mpoly = ra +t*s+[0;R];
        spoly = mpoly + 0.5*R*[1/sqrt(2);1/sqrt(2)];
        epoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
        x=[spoly(1),mpoly(1),epoly(1)];
        y=[spoly(2),mpoly(2),epoly(2)];
    handles.beams.(char(handles.beamnames(handles.temp))).loads.Mpoly = plot(x,y,'r','LineWidth',1.5);
    elseif sign(M) ==-1 %M<0
        mpoly = ra + t*s + [-R;0];
        spoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
        epoly = mpoly + 0.5*R*[-1/sqrt(2);-1/sqrt(2)];
        x=[spoly(1),mpoly(1),epoly(1)];
        y=[spoly(2),mpoly(2),epoly(2)];
     handles.beams.(char(handles.beamnames(handles.temp))).loads.Mpoly = plot(x,y,'r','LineWidth',1.5);
    end
end
    result = handles;
end