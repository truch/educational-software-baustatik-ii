function result = MomentSup(handles)
%From the beam-endmoments of the system, this function computes the
%corresponding moment lines which are then superponed with 
for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    l = handles.beams.(beam).l;
    pa = handles.beams.(beam).beampoints(1);
    pb = handles.beams.(beam).beampoints(2);
    syms Msup(x)
    if pa < pb %positive side is down
        Ma = -handles.points.(pointname(pa)).(pointname(pb)).M - handles.beams.(beam).S.M0(0);
        Mb = handles.points.(pointname(pb)).(pointname(pa)).M - handles.beams.(beam).S.M0(l);
    else
        Ma = handles.points.(pointname(pa)).(pointname(pb)).M;
        Mb = -handles.points.(pointname(pb)).(pointname(pa)).M;
    end
    Msup(x) = Ma + (Mb-Ma)/l*x;
    handles.beams.(beam).Mx = Msup + handles.beams.(beam).S.M0;
end
result = handles;
end