function name = phiname(point)
%point is a struct with field pointnr as double
name = strcat('phi',num2str(point.pointnr));
end