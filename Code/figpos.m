function [nleft, nbottom, nwidth, nheight] = figpos(h, nheight, str, number, pl)

set(h.obj.(strcat('text',num2str(number-1))), 'units','pixels');

if findstr(str,'text') == 1
    nleft = 10;
    nbottom = h.obj.(strcat('text',num2str(number-1))).Position(2) - h.obj.(strcat('text',num2str(number-1))).Position(4) - 20;
    nwidth = h.Fig.Position(3) - 20;
% elseif findstr(str,'momentpl') == 1
%     if mod(pl,2) == 1 %ungerade Zahl: 1, 3, 4
%         nwidth = h.Fig.Position(3)/2 - 5;
%     else %gerade Zahlen
%         nwidth = h.Fig.Position(3)/2 + 5;
%     end
% elseif findstr(str,'momentdl') == 1
%     nwidht = h.Fig.Position(3) - 10;
end

nleft = nleft / h.Fig.Position(3);
nbottom = nbottom / h.Fig.Position(4);
nwidth = nwidth / h.Fig.Position(3);
nheight = nheight / h.Fig.Position(4);
end
