function result = deletepoint(handles,point)
%Was machen wenn handles.totalpoints = 0?
handles.oldtotal = handles.totalpoints;
if handles.modify && strcmp(handles.addpoint.Visible,'off')
    set(handles.addpoint,'Visible','on')
end
if isfield(handles.points.(point),'textbox')
    delete(handles.points.(point).textbox);
end
if isfield(handles.points.(point),'loads')
loadfields = fieldnames(handles.points.(point).loads);
for i = 1 : numel(loadfields)
    currentload = char(loadfields(i));
    delete(handles.points.(point).loads.(currentload));
end
end
if isfield(handles.points.(point),'stiffnesstext')
    delete(handles.points.(point).stiffnesstext);
end
delete(handles.points.(point).pointmarker);
if handles.points.(point).F ~= 0
    for i = 1 : totalpoints-1
        max = 0;
        cforce = norm(handles.points.(pointname(i)).F);
        if cforce > max
            max = cforce;
        end
        handles.Fmax = max;
    end
end
%Reset the gui

handles.pushbutton4.Visible = 'off';
    handles.addpoint.Visible = 'on';
    %%%%%%%%%%%%%%%%%
    handles.text2.String = 'You are at point.';
    set(handles.info2,'Visible','on');
    set(handles.pointnr,'Visible','On');
    set(handles.edit1, 'Visible','On'); 
  set(handles.edit2, 'Visible','On'); 
  set(handles.text4,'Visible','On');
  set(handles.text5,'Visible','On');
  set(handles.numberofneighbours, 'Visible','On'); 
  set(handles.neighboursok, 'Visible','On'); 
  set(handles.neighbours, 'Visible','On'); 
  set(handles.axes2, 'Visible','On');
  set(handles.axes2.Children,'Visible','on');
  set(handles.edit8, 'Visible','On'); 
  set(handles.edit12, 'Visible','On');  
  set(handles.edit29, 'Visible','On'); 
  set(handles.edit31, 'Visible','On'); 
  set(handles.edit32, 'Visible','On'); 
  set(handles.addpoint, 'Visible','On'); 
  set(handles.edit12, 'Visible','On');
  
    handles.text6.Visible = 'on';
    handles.text7.Visible = 'on';
    handles.text8.Visible = 'on';
    handles.text10.Visible = 'on';
    handles.text11.Visible = 'on';
    handles.text12.Visible = 'on';
    %%%%%%%%%%%%%%%%%%%

%set all of the editfields according to their necessary content.
    set(handles.pointnr,'string',num2str(handles.points.(point).pointnr));
    set(handles.edit1,'string',num2str(handles.points.(point).r(1)));
    set(handles.edit2,'string',num2str(handles.points.(point).r(2)));
    set(handles.numberofneighbours,'string',num2str(numel(handles.points.(point).neighbours)));
    tablesize = handles.points.(point).neighbours;
    set(handles.neighbours,'Data',tablesize);
    %Constraintdial
    if strcmp(handles.points.(point).constrainttype,'fixed')
        handles = cdial_set('fixed','connection',handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.(point).constrainttype,'pin')
        handles = cdial_set('pin',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.(point).constrainttype,'roller')
        handles = cdial_set('roller',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        set(handles.text34,'String','angle');
        if handles.points.(point).constraintangle == 1000
        	set(handles.edit8,'String','');
        else
            set(handles.edit8,'String',num2str(handles.points.(point).constraintangle));
        end
    elseif strcmp(handles.points.(point).constrainttype,'torsional spring')
        handles = cdial_set('torsional spring',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[1/kNm]';
        else, unit = '[l/EI]'; end
        set(handles.text34,'string',strcat('cf ',unit));
        set(handles.edit8,'String',num2str(handles.points.(point).stiffness));
    elseif strcmp(handles.points.(point).constrainttype,'spring')
        handles = cdial_set('spring','hinge',handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[m/kN]';
        else, unit = '[l^3/EI]'; end
        set(handles.text34,'string',strcat('cf ',unit));
        set(handles.edit8,'String',num2str(handles.points.(point).stiffness));
    elseif strcmp(handles.points.(point).constrainttype,'none')
        handles = cdial_set('none',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    end
    set(handles.edit29,'string',num2str(handles.points.(point).F(1)));
    set(handles.edit31,'string',num2str(handles.points.(point).F(2)));
    set(handles.edit32,'string',num2str(handles.points.(point).M));
    set(handles.edit12,'string',num2str(handles.points.(point).phi));

handles.totalpoints = handles.totalpoints-1;
result = handles;
end