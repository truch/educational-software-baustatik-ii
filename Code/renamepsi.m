function result = renamepsi(sol,varstosolve,handles)
solnames = fieldnames(sol);
for i = 1 : numel(solnames)
    psistr = char(solnames(i));
    xindex = findstr(psistr,'x');
    pleft = str2double(psistr(4 : xindex-1));
    pright = str2double(psistr(xindex + 1 : numel(psistr)));
    handles.beams.(beamname(pleft,pright)).psisym = sol.(psistr);
end
pointnames = fieldnames(handles.points);
for i = 1 : numel(pointnames)
    pointvars = symvar(handles.points.(char(pointnames(i))).v(1));
    othervars = symvar(handles.points.(char(pointnames(i))).v(2));
    for k = 1 : numel(othervars)
        if not(ismember(othervars(k),pointvars))
            pointvars = [pointvars othervars(k)];
        end
    end
    for j = 1 : numel(varstosolve)
    if ismember(varstosolve(j),pointvars)
        handles.points.(char(pointnames(i))).v =... 
        subs(handles.points.(char(pointnames(i))).v,varstosolve(j),...
            (sol.(string(varstosolve(j)))));
    end
    end
end
result = handles;
end