function bool = iscollinear(v1,v2)
if not(isempty(v1)) && not(isempty(v2))
syms lambda
lambda = solve(v1 == v2*lambda, lambda);
bool = not(isempty(lambda));
else bool = false;
end
end