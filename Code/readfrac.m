function number = readfrac(string)
slashpos = findstr('/',string);
if not(isempty(slashpos))
    numerator = str2double(string(1:slashpos-1));
    denominator = str2double(string(slashpos+1:numel(string)));
    number = numerator/denominator;
else
    number = str2double(string);
end