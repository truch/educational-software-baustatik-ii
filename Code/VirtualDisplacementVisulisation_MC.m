function [h,Fig] = VirtualDisplacementVisulisation_MC(var,handles,h,Fig)
%Generates the multiplechoice dialog for the virtual displacement
%mechanism related to angle "var". Like this, it generates two wrong images
%and a right one. Correct is graphic 3.
h.graficpanel.Position(4) = h.height - 50;
heightgrafic = h.graficpanel.Position(4)/4;
widthgrafic = 180;
left = h.graficpanel.Position(3)/2-widthgrafic/2;
height = h.graficpanel.Position(4);
set(h.questionpanel.Children,'Visible','Off');          
a = [1, 2, 3];
a = a(randperm(length(a)));
pos1 = height-heightgrafic*a(1)-20*a(1)-90;
pos2 = height-heightgrafic*a(2)-20*a(2)-90;
pos3 = height-heightgrafic*a(3)-20*a(3)-90;
h.multiplechoice.txt = axes('Parent', h.graficpanel,'units','pixels','Position',[20 height-35 300 20],'Color',h.graficpanel.BackgroundColor,'XLim',[0 1],'YLim',[0,1],'xtick',[],'ytick',[],'xcolor',h.graficpanel.BackgroundColor,'ycolor',h.graficpanel.BackgroundColor);
text(0,0,strcat('virtual mechanism related to', {' '},latexpsi(char(string(var)))),'interpreter','latex','VerticalAlignment','bottom');
   
h.multiplechoice1 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos1+heightgrafic-10, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice2 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos2+heightgrafic-10, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice3 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos3+heightgrafic-10, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);

% GRAFIC 1
%Modify getpoints
set(getpoints,'currentaxes',handles.axes1);
scale = handles.imscale/20;
for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if strcmp(handles.points.(point).constrainttype,'none')
        wrongvector = rot90vec(handles.points.(point).vdisp)*scale/2;
        handles.points.(point).new_r = handles.points.(point).r + handles.points.(point).vdisp*scale + wrongvector;
    else
        wrongvector = [0;0];
        handles.points.(point).new_r = handles.points.(point).r + handles.points.(point).vdisp*scale;
    end
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','off');
        end
    end
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') + handles.points.(point).vdisp(1)*scale + wrongvector(1));
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') + handles.points.(point).vdisp(2)*scale + wrongvector(2));  
    set(handles.points.(point).textbox,'Color',[0.85,0.85,0.85]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','Off');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','off');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    pleft_r = handles.points.(pointname(pleft_nr)).new_r;
    pright_r = handles.points.(pointname(pright_nr)).new_r;
    handles.beams.(beam).displaced = ...
        plot([pleft_r(1),pright_r(1)],[pleft_r(2),pright_r(2)],'Color',[0 0 0],'LineWidth',handles.beamwidth);
    set(handles.beams.(beam).image,'Color',[0.85,0.85,0.85]);
    set(handles.beams.(beam).dashes,'Color',[0.85,0.85,0.85]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') + [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') + [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);
    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') + [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') + [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end        

%Copy graphic to the multiplechoicepanel
        h.multiplechoice.grafic1 = copyobj(handles.axes1,Fig);
        h.multiplechoice.grafic1.Parent = h.graficpanel;
        h.multiplechoice.grafic1.Units = 'pixels';
        h.multiplechoice.grafic1.Position = [left, pos1+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic1,'xtick',[]);
        set(h.multiplechoice.grafic1,'ytick',[]);
        set(h.multiplechoice.grafic1,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic1,'ycolor',[0.9294    0.9529    0.9882]);

for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if strcmp(handles.points.(point).constrainttype,'none')
        wrongvector = rot90vec(handles.points.(point).vdisp)*scale/2;
    else
        wrongvector = [0;0];
    end
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','on');
        end
    end
    delete(handles.points.(point).new_r);
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') - handles.points.(point).vdisp(1)*scale - wrongvector(1));
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') - handles.points.(point).vdisp(2)*scale - wrongvector(2));
    set(handles.points.(point).textbox,'Color',[0 0 0]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','On');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','on');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    delete(handles.beams.(beam).displaced);
    set(handles.beams.(beam).image,'Color',[0,0,0]);
    set(handles.beams.(beam).dashes,'Color',[0,0,0]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') - [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') - [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);

    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') - [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') - [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end


% GRAFIC 2                

%Modify getpoints
set(getpoints,'currentaxes',handles.axes1);
scale = handles.imscale/20;
for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if strcmp(handles.points.(point).constrainttype,'none')
        wrongvector = -rot90vec(handles.points.(point).vdisp)*scale/2;
        handles.points.(point).new_r = handles.points.(point).r + handles.points.(point).vdisp*scale + wrongvector;
    else
        wrongvector = [0;0];
        handles.points.(point).new_r = handles.points.(point).r + handles.points.(point).vdisp*scale;
    end
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','off');
        end
    end
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') + handles.points.(point).vdisp(1)*scale + wrongvector(1));
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') + handles.points.(point).vdisp(2)*scale + wrongvector(2));  
    set(handles.points.(point).textbox,'Color',[0.85,0.85,0.85]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','Off');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','off');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    pleft_r = handles.points.(pointname(pleft_nr)).new_r;
    pright_r = handles.points.(pointname(pright_nr)).new_r;
    handles.beams.(beam).displaced = ...
        plot([pleft_r(1),pright_r(1)],[pleft_r(2),pright_r(2)],'Color',[0 0 0],'LineWidth',handles.beamwidth);
    set(handles.beams.(beam).image,'Color',[0.85,0.85,0.85]);
    set(handles.beams.(beam).dashes,'Color',[0.85,0.85,0.85]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') + [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') + [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);
    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') + [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') + [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end        

%Copy graphic to the multiplechoicepanel
        h.multiplechoice.grafic2 = copyobj(handles.axes1,Fig);
        h.multiplechoice.grafic2.Parent = h.graficpanel;
        h.multiplechoice.grafic2.Units = 'pixels';
        h.multiplechoice.grafic2.Position = [left, pos2+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic2,'xtick',[]);
        set(h.multiplechoice.grafic2,'ytick',[]);
        set(h.multiplechoice.grafic2,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic2,'ycolor',[0.9294    0.9529    0.9882]);

for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if strcmp(handles.points.(point).constrainttype,'none')
        wrongvector = -rot90vec(handles.points.(point).vdisp)*scale/2;
    else
        wrongvector = [0;0];
    end
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','on');
        end
    end
    delete(handles.points.(point).new_r);
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') - handles.points.(point).vdisp(1)*scale - wrongvector(1));
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') - handles.points.(point).vdisp(2)*scale - wrongvector(2));
    set(handles.points.(point).textbox,'Color',[0 0 0]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','On');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','on');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    delete(handles.beams.(beam).displaced);
    set(handles.beams.(beam).image,'Color',[0,0,0]);
    set(handles.beams.(beam).dashes,'Color',[0,0,0]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') - [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') - [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);

    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') - [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') - [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end



% GRAFIC 3        
%Modify getpoints
set(getpoints,'currentaxes',handles.axes1);
scale = handles.imscale/20;
for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','off');
        end
    end
    handles.points.(point).new_r = handles.points.(point).r + handles.points.(point).vdisp*scale;
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') + handles.points.(point).vdisp(1)*scale);
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') + handles.points.(point).vdisp(2)*scale);  
    set(handles.points.(point).textbox,'Color',[0.85,0.85,0.85]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','Off');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','off');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    pleft_r = handles.points.(pointname(pleft_nr)).new_r;
    pright_r = handles.points.(pointname(pright_nr)).new_r;
    handles.beams.(beam).displaced = ...
        plot([pleft_r(1),pright_r(1)],[pleft_r(2),pright_r(2)],'Color',[0 0 0],'LineWidth',handles.beamwidth);
    set(handles.beams.(beam).image,'Color',[0.85,0.85,0.85]);
    set(handles.beams.(beam).dashes,'Color',[0.85,0.85,0.85]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') + [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') + [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);
    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') + [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') + [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end        

%Copy graphic to the multiplechoicepanel
        h.multiplechoice.grafic3 = copyobj(handles.axes1,Fig);
        h.multiplechoice.grafic3.Parent = h.graficpanel;
        h.multiplechoice.grafic3.Units = 'pixels';
        h.multiplechoice.grafic3.Position = [left, pos3+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic3,'xtick',[]);
        set(h.multiplechoice.grafic3,'ytick',[]);
        set(h.multiplechoice.grafic3,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic3,'ycolor',[0.9294    0.9529    0.9882]);

for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','on');
        end
    end
    delete(handles.points.(point).new_r);
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') - handles.points.(point).vdisp(1)*scale);
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') - handles.points.(point).vdisp(2)*scale);
    set(handles.points.(point).textbox,'Color',[0 0 0]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','On');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','on');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    delete(handles.beams.(beam).displaced);
    set(handles.beams.(beam).image,'Color',[0,0,0]);
    set(handles.beams.(beam).dashes,'Color',[0,0,0]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') - [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') - [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);

    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') - [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') - [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end
set(0,'currentfigure',Fig)
h.mc.help1 = [];
h.mc.help2 = [];

end