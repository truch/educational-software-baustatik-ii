function [p1, p2] = readpsivar(psiname)
xpos = findstr('x',psiname);
p1str = psiname(4 : xpos-1);
p2str = psiname(xpos + 1 : numel(psiname));
p1 = str2double(p1str);
p2 = str2double(p2str);
end