function varargout = getpoints(varargin)
% GETPOINTS MATLAB code for getpoints.fig
%      GETPOINTS, by itself, creates a new GETPOINTS or raises the existing
%      singleton*.
%
%      H = GETPOINTS returns the handle to a new GETPOINTS or the handle to
%      the existing singleton*.
%
%      GETPOINTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GETPOINTS.M with the given input arguments.
%
%      GETPOINTS('Property','Value',...) creates a new GETPOINTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before getpoints_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to getpoints_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help getpoints

% Last Modified by GUIDE v2.5 18-Jun-2018 16:53:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @getpoints_OpeningFcn, ...
                   'gui_OutputFcn',  @getpoints_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before getpoints is made visible.
function getpoints_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to getpoints (see VARARGIN)

% PC's active screen size
screen_size = get(0,'ScreenSize');
pc_width  = screen_size(3);
pc_height = screen_size(4);
%Matlab also does not consider the width of the border:
window_border  = 5;
%new position:
height = pc_height - 300;
width = height*1.5;
bottom = pc_height/2 - height/2 + 10;
left   = pc_width/2 - width/2  - window_border/2;

mainfigure = gcf;
mainfigure.Units = 'Pixels';
width = mainfigure.Position(3);
height = mainfigure.Position(4);
mainfigure.Position(1) = pc_width/2 - width/2;
mainfigure.Position(2) = pc_height/2 - height/2 + 50;
% Choose default command line output for getpoints
handles.output = hObject;

if not(isfield(handles,'interactive'))
    format rat
    handles.interactive = true;
    handles.togglebutton3.Value = 1;
    [system,unitdlg] = load_dialog;
    handles.unit = unitdlg;
    axes(handles.axes2);
    handles.axes2.XLim = [0,60];
    handles.axes2.YLim = [0,6];
    hold on
%Please change:
    constraintborderwidth = 1;
%%%%%%%
    constraintborder = patch([.25,.25,59.75,59.75,.25,.25,59.75],[3,6,6,0,0,3,3],'w');
    set(constraintborder,'FaceColor','none');
    set(constraintborder,'EdgeColor',[.3,.75,.93]);
    set(constraintborder,'LineWidth',constraintborderwidth);
    fixed_data = imread('small_fixed.PNG');
    roller_data = imread('small_roller.PNG');
    pin_data = imread('small_pin.PNG');
    spring_data = imread('small_spring.PNG');
    torsional_data = imread('small_torsional.PNG');
    none_data = imread('small_none.PNG');
    rigid_data = imread('small_rigid.PNG');
    hinge_data = imread('small_hinge.PNG');
    free_data = imread('small_free.PNG');
%Please change:
    selectpatchcolor = 1/255*[50,205,50];
    selectlinewidth = 1.3;
%%%%%%%%%
    handles.constraintimages.fixed_im = image([0.5,9.5],[5.7,3.3],fixed_data);
    setappdata(handles.constraintimages.fixed_im,'type','fixed');
    set(handles.constraintimages.fixed_im, 'buttonDownFcn',@cdial_eval);
    handles.constraintimages.fixed.patch = patch([0.5,9.5,9.5,0.5],[3.3,3.3,5.7,5.7],'b');
    set(handles.constraintimages.fixed.patch,'FaceColor','none');
    set(handles.constraintimages.fixed.patch,'EdgeColor',selectpatchcolor);
    set(handles.constraintimages.fixed.patch,'LineWidth', selectlinewidth);
    set(handles.constraintimages.fixed.patch,'Visible','off');

    handles.constraintimages.roller_im = image([11.5,19.5],[5.7,3.3],roller_data);
    setappdata(handles.constraintimages.roller_im,'type','roller');
    set(handles.constraintimages.roller_im, 'buttonDownFcn',@cdial_eval);
    handles.constraintimages.roller.patch = patch([11.5,19.5,19.5,11.5],[3.3,3.3,5.7,5.7],'b');
    set(handles.constraintimages.roller.patch,'FaceColor','none');
    set(handles.constraintimages.roller.patch,'EdgeColor',selectpatchcolor);
    set(handles.constraintimages.roller.patch,'LineWidth',selectlinewidth);
    set(handles.constraintimages.roller.patch,'Visible','off');

    handles.constraintimages.pin_im = image([21.5,29.5],[5.7,3.3],pin_data);
    setappdata(handles.constraintimages.pin_im,'type','pin');
    set(handles.constraintimages.pin_im, 'buttonDownFcn',@cdial_eval);
    handles.constraintimages.pin.patch = patch([21.5,29.5,29.5,21.5],[3.3,3.3,5.7,5.7],'b');
    set(handles.constraintimages.pin.patch,'FaceColor','none');
    set(handles.constraintimages.pin.patch,'EdgeColor',selectpatchcolor);
    set(handles.constraintimages.pin.patch,'LineWidth',selectlinewidth);
    set(handles.constraintimages.pin.patch,'Visible','off');

    handles.constraintimages.spring_im = image([31.5,39.5],[5.7,3.3],spring_data);
    setappdata(handles.constraintimages.spring_im,'type','spring');
    set(handles.constraintimages.spring_im, 'buttonDownFcn',@cdial_eval);
    handles.constraintimages.spring.patch = patch([31.5,39.5,39.5,31.5],[3.3,3.3,5.7,5.7],'b');
    set(handles.constraintimages.spring.patch,'FaceColor','none');
    set(handles.constraintimages.spring.patch,'EdgeColor',selectpatchcolor);
    set(handles.constraintimages.spring.patch,'LineWidth',selectlinewidth);
    set(handles.constraintimages.spring.patch,'Visible','off');

    handles.constraintimages.torsional_im = image([41.5,49.5],[5.7,3.3],torsional_data);
    setappdata(handles.constraintimages.torsional_im,'type','torsional spring');
    set(handles.constraintimages.torsional_im, 'buttonDownFcn',@cdial_eval);
    handles.constraintimages.torsional.patch = patch([41.5,49.5,49.5,41.5],[3.3,3.3,5.7,5.7],'b');
    set(handles.constraintimages.torsional.patch,'FaceColor','none');
    set(handles.constraintimages.torsional.patch,'EdgeColor',selectpatchcolor);
    set(handles.constraintimages.torsional.patch,'LineWidth',selectlinewidth);
    set(handles.constraintimages.torsional.patch,'Visible','off');

    handles.constraintimages.none_im = image([51.5,59.5],[5.7,3.3],none_data);
    setappdata(handles.constraintimages.none_im,'type','none');
    set(handles.constraintimages.none_im, 'buttonDownFcn',@cdial_eval);
    handles.constraintimages.none.patch = patch([51.5,59.5,59.5,51.5],[3.3,3.3,5.7,5.7],'b');
    set(handles.constraintimages.none.patch,'FaceColor','none');
    set(handles.constraintimages.none.patch,'EdgeColor',selectpatchcolor);
    set(handles.constraintimages.none.patch,'LineWidth',selectlinewidth);
    set(handles.constraintimages.none.patch,'Visible','off');

    handles.connectionimages.rigid_im = image([5,20],[2.7,0.3],rigid_data);
    setappdata(handles.connectionimages.rigid_im,'type','fixed connection');
    set(handles.connectionimages.rigid_im, 'buttonDownFcn',@cdial_eval);
    set(handles.connectionimages.rigid_im, 'Visible','off');
    handles.connectionimages.rigid.patch = patch([5,20,20,5],[2.7,2.7,0.3,0.3],'b');
    set(handles.connectionimages.rigid.patch,'FaceColor','none');
    set(handles.connectionimages.rigid.patch,'EdgeColor',selectpatchcolor);
    set(handles.connectionimages.rigid.patch,'LineWidth',selectlinewidth);
    set(handles.connectionimages.rigid.patch,'Visible','off');

    handles.connectionimages.hinge_im = image([22.5,37.5],[2.7,0.3],hinge_data);
    setappdata(handles.connectionimages.hinge_im,'type','hinge');
    set(handles.connectionimages.hinge_im, 'buttonDownFcn',@cdial_eval);
    set(handles.connectionimages.hinge_im, 'Visible','off');
    handles.connectionimages.hinge.patch = patch([22.5,37.5,37.5,22.5],[2.7,2.7,0.3,0.3],'b');
    set(handles.connectionimages.hinge.patch,'FaceColor','none');
    set(handles.connectionimages.hinge.patch,'EdgeColor',selectpatchcolor);
    set(handles.connectionimages.hinge.patch,'LineWidth',selectlinewidth);
    set(handles.connectionimages.hinge.patch,'Visible','off');

    handles.connectionimages.free_im = image([40,55],[2.7,0.3],free_data);
    setappdata(handles.connectionimages.free_im,'type','free');
    set(handles.connectionimages.free_im, 'buttonDownFcn',@cdial_eval);
    set(handles.connectionimages.free_im, 'Visible','off');
    handles.connectionimages.free.patch = patch([40,55,55,40],[2.7,2.7,0.3,0.3],'b');
    set(handles.connectionimages.free.patch,'FaceColor','none');
    set(handles.connectionimages.free.patch,'EdgeColor',selectpatchcolor);
    set(handles.connectionimages.free.patch,'LineWidth',selectlinewidth);
    set(handles.connectionimages.free.patch,'Visible','off');

if strcmp(handles.unit,'kN')
    set(handles.text4,'String','x-coordinate: [m]');
    set(handles.text5,'String','y-coordinate: [m]');
    set(handles.text10,'String','Point load F: [kN]');
    set(handles.text11,'String','Moment: [kNm]');
    set(handles.text12,'String','Initial beam rotation: [rad]');
    set(handles.text16,'String','Flexural stiffness: [kNm^2]');
    set(handles.text26,'String','q1: [kN/m]');
    set(handles.text27,'String','q2: [kN/m]');
    set(handles.text21,'String','Moment: [kNm]');
    set(handles.text22,'String','initial beam rotation: [rad]');
elseif strcmp(handles.unit,'q')
    set(handles.text4,'String','x-coordinate: [L]');
    set(handles.text5,'String','y-coordinate: [L]');
    set(handles.text10,'String','Point load F: [qL]');
    set(handles.text11,'String','Moment: [qL^2]');
    set(handles.text12,'String','Initial beam rotation: [qL^3/EI]');
    set(handles.text16,'String','Flexural stiffness: [EI]');
    set(handles.text26,'String','q1: [q]');
    set(handles.text27,'String','q2: [q]');
    set(handles.text21,'String','Moment: [qL^2]');
    set(handles.text22,'String','Initial beam rotation: [qL^3/EI]');
elseif strcmp(handles.unit,'Q')
    set(handles.text4,'String','x-coordinate: [L]');
    set(handles.text5,'String','y-coordinate: [L]');
    set(handles.text10,'String','Point load F: [Q]');
    set(handles.text11,'String','Moment: [QL]');
    set(handles.text12,'String','Initial beam rotation: [QL^2/EI]');
    set(handles.text16,'String','Flexural stiffness: [EI]');
    set(handles.text26,'String','q1: [Q/L]');
    set(handles.text27,'String','q2: [Q/L]');
    set(handles.text21,'String','Moment: [QL]');
    set(handles.text22,'String','Initial beam rotation: [QL^2/EI]');
else
    set(handles.text4,'String','x-coordinate: [L]');
    set(handles.text5,'String','y-coordinate: [L]');
    set(handles.text10,'String','Point load F: [M/L]');
    set(handles.text11,'String','Moment: [M]');
    set(handles.text12,'String','Initial beam rotation: [ML/EI]');
    set(handles.text16,'String','Flexural stiffness: [EI]');
    set(handles.text26,'String','q1: [M/L^2]');
    set(handles.text27,'String','q2: [M/L^2]');
    set(handles.text21,'String','Moment: [M]');
    set(handles.text22,'String','Initial beam rotation: [ML/EI]');
end
if not(strcmp(system,'custom'))
    rawdata = load(system);
    fn = fieldnames(rawdata.savestruct);
    for i = 1 : numel(fn)
        if not(strcmp(char(fn(i)),'temp'))
        handles.(char(fn(i))) = rawdata.savestruct.(char(fn(i)));
        end
    end
answer = questdlg('Modify constraints?', ...
	'Constraints', ...
	'Yes','No','No');

switch answer
    case 'Yes'
        answer = true;
    case 'No'
        answer = false;
end
handles.modify = answer;
if not(handles.modify)
    handles = pointsfromfile(handles);
    handles.addpoint.Visible = 'off';
    handles = pushbutton4(handles);
else
    handles.togglebutton5.Visible = 'on';
    handles.pushbutton4.Visible = 'off';
    handles.totalpointstotal = handles.totalpoints;
    %set all of the editfields according to their necessary content.
    i=1;
    set(handles.pointnr,'string',num2str(i));
    set(handles.edit1,'string',num2str(handles.points.point1.r(1)));
    set(handles.edit2,'string',num2str(handles.points.point1.r(2)));
    set(handles.numberofneighbours,'string',num2str(numel(handles.points.point1.neighbours)));
    tablesize = handles.points.point1.neighbours;
    set(handles.neighbours,'Data',tablesize);
    %Constraintdial
    if strcmp(handles.points.point1.constrainttype,'fixed')
        handles = cdial_set('fixed','connection',handles.points.point1.neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.point1.constrainttype,'pin')
        handles = cdial_set('pin',handles.points.point1.knottype,handles.points.point1.neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.point1.constrainttype,'roller')
        handles = cdial_set('roller',handles.points.point1.knottype,handles.points.point1.neighbours,handles);
        set(handles.edit8,'Visible','On');
        set(handles.text34,'String','angle');
        if handles.points.point1.constraintangle == 1000
        	set(handles.edit8,'String','');
        else
            set(handles.edit8,'String',num2str(handles.points.point1.constraintangle));
        end
    elseif strcmp(handles.points.point1.constrainttype,'torsional spring')
        handles = cdial_set('torsional spring',handles.points.point1.knottype,handles.points.point1.neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[1/kNm]';
        else unit = '[l/EI]'; end
        set(handles.text34,'String',strcat('cf',unit));
        set(handles.edit8,'String',num2str(handles.points.point1.stiffness));
    elseif strcmp(handles.points.point1.constrainttype,'spring')
        handles = cdial_set('spring','hinge',handles.points.point1.neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[m/kN]';
        else unit = '[l^3/EI]'; end
        set(handles.text34,'String',strcat('cf',unit));
        set(handles.edit8,'String',num2str(handles.points.point1.stiffness));
    elseif strcmp(handles.points.point1.constrainttype,'none')
        handles = cdial_set('none',handles.points.point1.knottype,handles.points.point1.neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    end
    set(handles.edit29,'string','0');
    set(handles.edit31,'string','0');
    set(handles.edit32,'string','0');
    set(handles.edit12,'string','0');
    handles.togglebutton5.Visible = 'off';
end
else
handles.modify = false;
end
handles.theory = true;
handles.changebeam = false;
handles.togglebutton2.Value = 1;
handles.usegrid = false;
handles.defineneighbours = false;
handles.changepoint = false;
end

% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = getpoints_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edit1_Callback(hObject, eventdata, handles)



function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)

function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function numberofneighbours_Callback(hObject, eventdata, handles)


function numberofneighbours_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function neighboursok_Callback(hObject, eventdata, handles)
neighbourspos = get(handles.neighbours, 'Position');
if str2double(get(handles.numberofneighbours,'string')) > 4
    neighbourspos(2) = 21;
    neighbourspos(4) = 2.5;
else
    neighbourspos(2) = 22;
    neighbourspos(4) = 1.6;
end
handles.neighbours.Position = neighbourspos;
tablesize=zeros(1,str2double(get(handles.numberofneighbours,'string')));
if isfield(handles,'points')
    for i = 1:numel(handles.pointnames)
        if ismember(str2double(get(handles.pointnr,'string')),handles.points.(char(handles.pointnames(i))).neighbours) && not(ismember(i,tablesize))
            addn = i;
            h=tablesize(tablesize==0);
            if numel(h)>1
            addn(numel(h))=0;
            end
            tablesize(tablesize==0)=addn;
        end
    end
end

set(handles.neighbours,'Data',tablesize);


function neighbours_CellSelectionCallback(hObject, eventdata, handles)

function edit8_Callback(hObject, eventdata, handles)

function edit8_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit29_Callback(hObject, eventdata, handles)


function edit29_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit31_Callback(hObject, eventdata, handles)


function edit31_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit32_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edit32_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit12_Callback(hObject, eventdata, handles)

function edit12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function addpoint_Callback(hObject, eventdata, handles)
%saving all the data to the nested structure handles.point   

n = get(handles.neighbours,'data');
if numel(n) == 0
    uiwait(msgbox('The neighbours are not defined.','Error!','modal'));
else
for i = 1 : numel(n)
    if n(i) == str2double(get(handles.pointnr,'string'))
        neighbourslogic = false;
    else neighbourslogic = true;
    end
end

if neighbourslogic
if isfield(handles,'temp')
if isfield(handles.temp,'constrainttype')
if isfield(handles.temp,'knottype')
if not(isnan(readfrac(handles.edit8.String))) || isempty(handles.edit8.String)
if not(strcmp(handles.temp.constrainttype,'roller')&& numel(n)>1 && isempty(handles.edit8.String))
if not(strcmp(handles.temp.constrainttype,'torsional spring')&&isempty(handles.edit8.String))
if not(strcmp(handles.temp.constrainttype,'spring')&& isempty(handles.edit8.String))
if all(handles.neighbours.Data ~= 0)
    handles=savepoint(handles);
else, uiwait(msgbox('All of the numbers of the neighbours must be non zero','Error!','modal'));
end
else, uiwait(msgbox('Please define a resilience cf for the spring','Error!','modal'));
end
else, uiwait(msgbox('Please define a resilience cf for the torsional spring','Error!','modal'));
end
else, uiwait(msgbox('Please define a direction for the roller','Error!','modal'));
end
else 
    if strcmp(handles.temp.constrainttype,'spring') || strcmp(handles.temp.constrainttype,'torsional spring')
    uiwait(msgbox('You can only insert numeric values for the resilience of the spring.','Error!','modal'));
    else 
    uiwait(msgbox('You can only insert numeric angles for the direction of the roller.','Error!','modal'));
    end
end
else, uiwait(msgbox('No connection defined','Error!','modal'));
end
else, uiwait(msgbox('No constraint defined','Error!','modal'));
end
else, uiwait(msgbox('No constraint or connection defined','Error!','modal'));
end
else, uiwait(msgbox('One neighbour point has the same number as the current point','Error!','modal'));
end
end
guidata(hObject, handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pushbutton4_Callback(hObject, eventdata, handles)
if strcmp(handles.points.(pointname(1)).constrainttype, 'roller') && strcmp(handles.points.(pointname(2)).constrainttype, 'roller') &&...
        (handles.points.(pointname(1)).constraintangle == handles.points.(pointname(2)).constraintangle ||...
        abs(handles.points.(pointname(1)).constraintangle - handles.points.(pointname(2)).constraintangle) == 180 ||...
        abs(handles.points.(pointname(1)).constraintangle - handles.points.(pointname(2)).constraintangle) == 820)
    uiwait(msgbox({'You have implemented a mechanism (two collinear rollers). Any component of forces parallel to the beam cannot be transferred.',...
        ' ', 'Click on "change point" and choose another constraint or change the angle of the roller.'},'Error!','modal'));
else        
  handles=pushbutton4(handles);
end
guidata(hObject, handles);

function edit13_Callback(hObject, eventdata, handles)

function edit13_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit14_Callback(hObject, eventdata, handles)
    if str2double(get(handles.edit14,'string')) > str2double(get(handles.text37,'string'))
        uiwait(msgbox('Choose the position s smaller than the length of the beam!','Position of distributed load','modal'));
    end

function edit14_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit18_Callback(hObject, eventdata, handles)

function edit18_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit19_Callback(hObject, eventdata, handles)
    if str2double(get(handles.edit19,'string')) > str2double(get(handles.text37,'string'))
        uiwait(msgbox('Choose the position s smaller than the length of the beam!','Position of distributed load','modal'));
    end
    
function edit19_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit21_Callback(hObject, eventdata, handles)

function edit21_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit22_Callback(hObject, eventdata, handles)
    if str2double(get(handles.edit22,'string')) > str2double(get(handles.text37,'string'))
        uiwait(msgbox('Choose the position s smaller than the length of the beam!','Position of distributed load','modal'));
    end
    
function edit22_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit24_Callback(hObject, eventdata, handles)

function edit24_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit25_Callback(hObject, eventdata, handles)
    if str2double(get(handles.edit25,'string')) > str2double(get(handles.text37,'string'))
        uiwait(msgbox('Choose the position s smaller than the length of the beam!','Moment position','modal'));
    end

function edit25_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit26_Callback(hObject, eventdata, handles)

function edit26_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit27_Callback(hObject, eventdata, handles)

function edit27_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function radiobutton2_Callback(hObject, eventdata, handles)
        if get(handles.radiobutton2,'Value') == 1
            set(handles.text35, 'Visible','On');
            set(handles.edit14, 'Visible','On');
        else
            set(handles.text35, 'Visible','Off');
            set(handles.edit14, 'Visible','Off');
        end
guidata(hObject, handles);
        
        
% function radiobutton3_Callback(hObject, eventdata, handles)
% guidata(hObject, handles);


function pushbutton5_Callback(hObject, eventdata, handles)

if strcmp(get(handles.edit26,'string'),'0') || isempty(get(handles.edit26,'string'))
    set(handles.text33, 'Visible','On');
    tablesize = [0 0 0];
    set(handles.uitable5,'Data',tablesize);
else
        if handles.changebeam
        previous = handles.uitable5.Data;
        if numel(previous(:,1)) > str2double(handles.edit26.String)        
            tablesize=cell(str2double(get(handles.edit26,'string')),3);
        elseif numel(previous(:,1)) == str2double(handles.edit26.String)
           tablesize = previous;
        else
            addcell = cell(str2double(handles.edit26.String)-numel(previous(:,1)),3);
            for i = 1 : numel(addcell)
                addcell{i} = num2str(addcell{i});
            end
            tablesize = [previous;addcell];
        end
        else
            if iscell(handles.uitable5.Data) && not(isempty(handles.uitable5.Data))
                previous = handles.uitable5.Data;
                if numel(previous(:,1)) <= str2double(handles.edit26.String)
                    addcell = cell(str2double(handles.edit26.String)-numel(previous(:,1)),3);
                    for i = 1 : numel(addcell)
                        addcell{i} = num2str(addcell{i});
                    end                    
                tablesize = [previous;addcell];
                else
                    tablesize = cell(str2double(get(handles.edit26,'string')),3);
                end
            else
                tablesize = cell(str2double(get(handles.edit26,'string')),3);
            end
        end
                set(handles.text33, 'Visible','Off');            
        set(handles.uitable5,'Data',tablesize);
end
guidata(hObject, handles);

function radiobutton4_Callback(hObject, eventdata, handles)
if get(handles.radiobutton4,'value')==1
    set(handles.radiobutton5,'value',0);
    set(handles.radiobutton6,'value',0);
end
guidata(hObject, handles);


function radiobutton5_Callback(hObject, eventdata, handles)
if get(handles.radiobutton5,'value')==1
    set(handles.radiobutton4,'value',0);
    set(handles.radiobutton6,'value',0);
end
guidata(hObject, handles);


function radiobutton6_Callback(hObject, eventdata, handles)
if get(handles.radiobutton6,'value')==1
    set(handles.radiobutton5,'value',0);
    set(handles.radiobutton4,'value',0);
end
guidata(hObject, handles);


function pushbutton6_Callback(hObject, eventdata, handles)
    % check: Is a direction of q chosen if q~=0?
if handles.radiobutton2.Value == 0 || handles.radiobutton2.Value == 1 && round(handles.beams.(char(handles.beamnames(handles.temp))).l, 7) == handles.beams.(char(handles.beamnames(handles.temp))).l
if ((get(handles.radiobutton4,'value')==1 || get(handles.radiobutton5,'value')==1 || get(handles.radiobutton6,'value')==1)&&(str2double(get(handles.edit18,'string'))~=0||str2double(get(handles.edit21,'string'))~=0))||(str2double(get(handles.edit18,'string'))==0&&str2double(get(handles.edit21,'string'))==0)
if not(isempty(get(handles.edit13,'string'))) && not(str2double(get(handles.edit13,'string'))==0) %check if there is a stiffness for the beam
if (isempty(get(handles.edit14,'string')) || strcmp(get(handles.edit14,'string'), 'NaN')) ||...
        readfrac_parameter(get(handles.edit14,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l)<= handles.beams.(char(handles.beamnames(handles.temp))).l &&...
        readfrac_parameter(get(handles.edit19,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l)<= handles.beams.(char(handles.beamnames(handles.temp))).l &&...
        round(readfrac_parameter(get(handles.edit22,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l),4)<= round(handles.beams.(char(handles.beamnames(handles.temp))).l,4) &&... 
        readfrac_parameter(get(handles.edit25,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l)<= handles.beams.(char(handles.beamnames(handles.temp))).l
  
        handles = pushbutton6(handles); %executes the function for pushbutton 6
else
    uiwait(msgbox('All positions s should be smaller than the length of the beam!','Position s','modal'));
end

else
    uiwait(msgbox({'A beam must have a stiffness.' 'Give a number for EI.'},'Stiffness','modal'));
end

else
    uiwait(msgbox('Please choose a direction of your distributed load q.','Distributed load','modal'))
end

else
       uiwait(msgbox('A hinge on a inclined beam with this specific length is not supported due to a numeric error.','Error','modal')) 
end
guidata(hObject, handles);

function pushbutton7_Callback(hObject, eventdata, handles)
for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    handles.beams.(beam).image.Color = [0 0 0];
    handles.beams.(beam).dashes.Color = [0 0 0];
    setappdata(handles.beams.(beam).image,'blue',false);
end
handles = solution_kinematic(getpoints,handles);

% %handles = angles(handles);
% %Kinematic analysis
% handles = locklines(handles);
% handles = findpsi(handles);
% foundpsi = testpsi(handles);
% if foundpsi %system is displaceable
% handles.beamdone = [{}];
% % Find a fixpoint to start a path from
% for j = 1 : numel(handles.pointnames)
%     if handles.points.(char(handles.pointnames(j))).fix
%         startpoint = handles.points.(char(handles.pointnames(j))).pointnr;
%         break;
%     end
% end
% 
% handles = initpath(startpoint,1,handles);
% handles = initmatch(1,handles);
% % for i = 1 : numel(fieldnames(handles.paths))
% %     disp(handles.psieqn.(eqname(i)));
% % end
% 
% nvar = countvar(handles.psieqn);
% if not(nvar == 0)
% handles.validpsieqn = valideqn(handles.psieqn);
% neq = numel(handles.validpsieqn);
% dgr = nvar-neq;
% varstosolve = symvar(handles.validpsieqn);
% 
% [sol,varstosolve] = psidialog(dgr,varstosolve,handles);
% if not(isstruct(sol))
%     sol = struct(char(string(varstosolve)),sol);
% end
% handles.varstosolve = varstosolve;
% handles.varindep = symvar(handles.validpsieqn);
% for i = 1 : numel(varstosolve)
% handles.varindep(handles.varindep == varstosolve(i)) = [];
% end
% 
% handles = renamepsi(sol,varstosolve,handles);
% handles.dgr = dgr;
% else
%     handles.varindep = [];
%     for p = 1 : numel(handles.beamnames)
%         if handles.beams.(char(handles.beamnames(p))).haspsi
%             handles.varindep = [handles.varindep handles.beams.(char(handles.beamnames(p))).psisym];
%         end
%     end
% end
% % Hier Ausgabe des Grads der Verschieblichkeit
% % Hier Ausgabe der miteinander verkn�pften Stabdrehwinkel (varstosolve sind
% % die abh�ngigen Stabdrehwinkel)
% end
% % Find the knots having phis
% handles = findphi(handles);
% 
% %Formulate all of the point equilibrium-equations
% handles = pointequilibrium(handles);
% 
% handles.displeqn = [];
% if foundpsi
% %Formulate the displacement-equilibria:
% for i = 1 : numel(handles.varindep)
%     handles = displacementequilibrium(handles.varindep(i),handles);
% end
% disp(handles.displeqn); %Just for testing purposes
% end
% disp(handles.pointeqn); %Just for testing purposes
% if not(isempty(handles.pointeqn))
% solangles = solve([handles.displeqn;handles.pointeqn]);
% if not(isstruct(solangles))
%     if not(isempty(handles.displeqn))
%     solangles = struct(char(string(symvar(handles.displeqn))),solangles);
%     elseif not(isempty(handles.pointeqn))
%     solangles = struct(char(string(symvar(handles.pointeqn))),solangles);
%     end
% end
% names = fieldnames(solangles);
% %This is just for testing purposes: 
% for i = 1 : numel(names)
%     disp(names(i));
%     disp(solangles.(char(names(i))));
% end
% % Substitute all the symbolic variables by their actual values
% for p = 1 : numel(handles.pointnames)
%    current = handles.points.(char(handles.pointnames(p))).pointnr;
%    neighbours = handles.points.(char(handles.pointnames(p))).neighbours;
%    for n = 1 : numel(neighbours)
%        for i = 1 : numel(names)
%            syms (char(names(i)))
%            handles.points.(pointname(current)).(pointname(neighbours(n))).M = ...
%                subs(handles.points.(pointname(current)).(pointname(neighbours(n))).M,...
%                eval(char(names(i))),solangles.(char(names(i))));
%        end
%    end
% end
% end
% %Superpone the Moment lines M0 and the beam-end-moments calculated by the
% %displacement method.
% handles = MomentSup(handles);
% 
% %Plot the final moment lines:
% handles = finalmoments(handles);
guidata(hObject, handles)





%function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit28 as text
%        str2double(get(hObject,'String')) returns contents of edit28 as a double


% --- Executes during object creation, after setting all properties.
%function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in save_geometry.
function save_geometry_Callback(hObject, eventdata, handles)
% hObject    handle to save_geometry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Save the structure
set(getpoints,'currentaxes',handles.axes1);
savestruct.temp = handles.temp;
savestruct.totalpoints = handles.totalpoints;
savestruct.points = handles.points;
savestruct.pointnames = handles.pointnames;
for i = 1 : numel(savestruct.pointnames)
    point = char(savestruct.pointnames(i));
    if isfield(savestruct.points.(point),'pvec')
        savestruct.points.(point) = rmfield(savestruct.points.(point),'pvec');
    end
    if isfield(savestruct.points.(point),'tvec')
        savestruct.points.(point) = rmfield(savestruct.points.(point),'tvec');
    end
    if isfield(savestruct.points.(point),'newfix')
        savestruct.points.(point) = rmfield(savestruct.points.(point),'newfix');
    end
    if isfield(savestruct.points.(point),'fix')
        savestruct.points.(point) = rmfield(savestruct.points.(point),'fix');
    end
    if isfield(savestruct.points.(point),'hasphi')
        savestruct.points.(point) = rmfield(savestruct.points.(point),'hasphi');
    end
    if isfield(savestruct.points.(point),'equation')
        savestruct.points.(point) = rmfield(savestruct.points.(point),'equation');
    end
    for j = 1 : numel(savestruct.points.(point).neighbours)
        nb = savestruct.points.(point).neighbours(j);
        if isfield(savestruct.points.(point),pointname(nb))
            savestruct.points.(point) = rmfield(savestruct.points.(point),pointname(nb));
        end
    end
end
savestruct.scalexupper = handles.scalexupper;
savestruct.scaleyupper = handles.scaleyupper;
savestruct.scalexlower = handles.scalexlower;
savestruct.scaleylower = handles.scaleylower;
savestruct.imscale = handles.imscale;
savestruct.Fmax = handles.Fmax;
name = savedialog;
save(name,'savestruct');
set(0,'currentfigure',getpoints);
set(getpoints,'currentaxes',handles.axes1);
% Generate the figure
for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','off');
        end
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','off');
        end
    end
%     if isfield(handles.beams.(beam),'Mplot')
%         set(handles.beams.(beam).Mplot,'Visible','Off');
%     end
%     if isfield(handles.beams.(beam),'labels')
%         labelnames = fieldnames(handles.beams.(beam).labels);
%         for j = 1 : numel(labelnames)
%             currentlabel = char(labelnames(j));
%             set(handles.beams.(beam).labels.(currentlabel),'Visible','Off')
%         end
%     end  
end

systemfigure = getframe(handles.axes1);
systemimage = frame2im(systemfigure);
imwrite(systemimage,strcat(name,'.png'));

for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','on');
        end
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','on');
        end
    end
%     if isfield(handles.beams.(beam),'Mplot')
%         set(handles.beams.(beam).Mplot,'Visible','On');
%     end
%     if isfield(handles.beams.(beam),'labels')
%         labelnames = fieldnames(handles.beams.(beam).labels);
%         for j = 1 : numel(labelnames)
%             currentlabel = char(labelnames(j));
%             set(handles.beams.(beam).labels.(currentlabel),'Visible','On')
%         end
%     end  
end    
guidata(hObject,handles)


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.togglebutton2.Value == 1
    handles.theory = true;
    handles.togglebutton2.BackgroundColor = [212, 244, 141]/255;
else
    handles.theory = false;
    handles.togglebutton2.BackgroundColor = [0.95,0.95,0.95];
end
guidata(hObject,handles);% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on button press in togglebutton3.
function togglebutton3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.togglebutton3.Value == 1
    handles.interactive = true;
    handles.togglebutton3.BackgroundColor = [212, 244, 141]/255;
else
    handles.interactive = false;
    handles.togglebutton3.BackgroundColor = [0.95,0.95,0.95];
end
guidata(hObject,handles);
% Hint: get(hObject,'Value') returns toggle state of togglebutton3


% --- Executes on button press in togglebutton4.
function togglebutton4_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.togglebutton4.Value == 1
    handles.changebeam = true;
    handles.togglebutton4.BackgroundColor = [242, 255, 219]/255;
else
    handles.changebeam = false;
    handles.togglebutton4.BackgroundColor = [0.95,0.95,0.95];
end
guidata(hObject,handles)
% Hint: get(hObject,'Value') returns toggle state of togglebutton4


% --- Executes on button press in togglebutton5.
function togglebutton5_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if handles.togglebutton5.Value == 1
        handles.usegrid = true;
        handles.defineneighbours = false;
        handles.togglebutton5.BackgroundColor = [0.73,0.92,1];
        [origin,xlimits,ylimits,meshwidth] = griddialog;
        handles = drawgrid(origin,xlimits,ylimits,meshwidth,handles);
        if not(isfield(handles,'gridpointnr'))
            handles.gridpointnr = 0;
        end
  handles.imscale = abs(xlimits(2)-xlimits(1))+2;
  set(handles.info2,'Visible','off');
  set(handles.edit1, 'Visible','Off'); 
  set(handles.edit2, 'Visible','Off'); 
  set(handles.numberofneighbours, 'Visible','Off'); 
  set(handles.neighboursok, 'Visible','Off'); 
  set(handles.neighbours, 'Visible','Off'); 
  set(handles.axes2, 'Visible','Off');
  set(handles.axes2.Children,'Visible','off');
  set(handles.edit8, 'Visible','Off'); 
  set(handles.edit12, 'Visible','Off');  
  set(handles.edit29, 'Visible','Off'); 
  set(handles.edit31, 'Visible','Off'); 
  set(handles.edit32, 'Visible','Off'); 
  set(handles.addpoint, 'Visible','Off'); 
  set(handles.pushbutton4, 'Visible','Off'); 
  set(handles.edit12, 'Visible','Off');
  
    handles.text6.Visible = 'off';
    handles.text7.Visible = 'off';
    handles.text8.Visible = 'off';
    handles.text10.Visible = 'off';
    handles.text11.Visible = 'off';
    handles.text12.Visible = 'off';
    else
        handles.togglebutton5.BackgroundColor = [0.95,0.95,0.95];
    end

guidata(hObject,handles)
% Hint: get(hObject,'Value') returns toggle state of togglebutton5


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
point = pointname(handles.gridpointnr);
handles.points.(point).r(1) = readfrac(handles.edit1.String);
handles.points.(point).r(2) = readfrac(handles.edit2.String);
delete(handles.points.(point).pointmarker);
delete(handles.points.(point).textbox);
handles.points.(point).pointmarker = ...
    plot(handles.points.(point).r(1),handles.points.(point).r(2),'x-k');
uistack(handles.points.(point).pointmarker,'top');
handles.points.(point).textbox = text(handles.points.(point).r(1)+0.03*(handles.imscale)^0.8,handles.points.(point).r(2)-0.06*(handles.imscale)^0.8,strcat('$\raisebox{.5pt}{\textcircled{\raisebox{-.9pt}{',num2str(handles.points.(point).pointnr),'}}}$'),'Interpreter','latex');
set(handles.pushbutton14,'Visible','off')
set(handles.edit1,'Visible','off');
set(handles.edit2,'Visible','off');
guidata(hObject,handles)


% --- Executes on button press in togglebutton6.
function togglebutton6_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.togglebutton6.Value == 1
    handles.togglebutton6.BackgroundColor = [0.73,0.92,1];
    handles.defineneighbours = true;
    handles.text2.String = 'Match the points!';
    handles.text2.OuterPosition(3) = 25;
    handles.pointnr.Visible = 'off';
    handles.firstclick = false;
    handles.secondclick = false;
    handles.firstclicknr = 0;
    handles.text4.Visible = 'off';
    handles.text5.Visible = 'off';
    handles.edit1.Visible = 'off';
    handles.edit2.Visible = 'off';
    handles.pushbutton14.Visible = 'off';
    handles.pushbutton15.Visible = 'on';
else
    handles.togglebutton6.BackgroundColor = [.95,.95,.95];
    handles.defineneighbours = false;
end
guidata(hObject,handles)
% Hint: get(hObject,'Value') returns toggle state of togglebutton6


% --- Executes on button press in togglebutton7.
function togglebutton7_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.togglebutton7.Value == 1
    handles.togglebutton7.BackgroundColor = [0.73,0.92,1];
    handles.changepoint = true;
else
    handles.togglebutton7.BackgroundColor = [.95,.95,.95];
    handles.changepoint = false;
end
guidata(hObject,handles)
% Hint: get(hObject,'Value') returns toggle state of togglebutton7


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allneighbours = true;
for i = 1 : numel(handles.pointnames)
    point = pointname(i);
    if not(isfield(handles.points.(point),'neighbours'))
        allneighbours = false;
        break;
    end
end
if allneighbours
for i = 1 : numel(handles.xline)
    delete(handles.xline(i))
end
for i = 1 : numel(handles.yline)
    delete(handles.yline(i));
end

handles.togglebutton5.Visible = 'off';
handles.togglebutton6.Visible = 'off';
handles.pushbutton15.Visible = 'off';
    set(handles.info2,'Visible','on');
    handles.text2.String = 'You are at point:';
    handles.pointnr.Visible = 'on';
    handles.text4.Visible = 'on';
    handles.text5.Visible = 'on';
    handles.edit1.Visible = 'on';
    handles.edit2.Visible = 'on';
    handles.text6.Visible = 'on';
    handles.text7.Visible = 'on';
    handles.text8.Visible = 'on';
    handles.text10.Visible = 'on';
    handles.text11.Visible = 'on';
    handles.text12.Visible = 'on';
  set(handles.numberofneighbours, 'Visible','On'); 
  set(handles.neighboursok, 'Visible','On'); 
  set(handles.neighbours, 'Visible','On'); 
  set(handles.axes2, 'Visible','On');
  set(handles.axes2.Children,'Visible','on')
  set(handles.edit8, 'Visible','On'); 
  set(handles.edit12, 'Visible','On'); 
  set(handles.edit29, 'Visible','On'); 
  set(handles.edit31, 'Visible','Off'); 
  set(handles.edit32, 'Visible','On'); 
  set(handles.addpoint, 'Visible','On'); 
  set(handles.pushbutton4, 'Visible','On'); 
  set(handles.edit12, 'Visible','On');
    
    
handles.modify = true;
    handles.pushbutton4.Visible = 'off';
    handles.totalpointstotal = handles.totalpoints;
    %set all of the editfields according to their necessary content.
    i=1;
    set(handles.pointnr,'string',num2str(i));
    set(handles.edit1,'string',num2str(handles.points.point1.r(1)));
    set(handles.edit2,'string',num2str(handles.points.point1.r(2)));
    set(handles.numberofneighbours,'string',num2str(numel(handles.points.point1.neighbours)));
    tablesize = handles.points.point1.neighbours;
    set(handles.neighbours,'Data',tablesize);
    %Constraintdial
    handles = cdial_set('','',[],handles);
    set(handles.text34,'String','');
    set(handles.edit8,'Visible','Off');
    set(handles.edit29,'string','0');
    set(handles.edit31,'string','0');
    set(handles.edit32,'string','0');
    set(handles.edit12,'string','0');
    handles.points.point1.MarkerEdgeColor = [255 192 0]/255;
    handles.points.point1.textbox.Color = [255 192 0]/255;
else
    uiwait(msgbox('There is at least one point that is not matched with any other.','Error!','modal'));
end
guidata(hObject,handles)


% --- Executes on button press in info.
function info_Callback(hObject, eventdata, handles)
% hObject    handle to info (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
infobutton;
guidata(hObject,handles);


% --- Executes on button press in info2.
function info2_Callback(hObject, eventdata, handles)
% hObject    handle to info2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
infobutton2;
guidata(hObject,handles);
