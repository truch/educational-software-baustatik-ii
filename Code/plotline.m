function linepl = plotline(pvec,tvec)
linepl = line([pvec(1)-tvec(1)/2,pvec(1)+tvec(1)/2],[pvec(2)-tvec(2)/2,pvec(2)+tvec(2)/2]);
end