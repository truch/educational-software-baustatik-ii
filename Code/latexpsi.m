function string = latexpsi(input)
% Generates the latex string $psi\{2,3}$ from an input of type 'psi2x3'.
% Input must be of type string.
xpos = findstr(input,'x');
firstIndex = input(4 : xpos-1);
secondIndex = input(xpos+1:numel(input));
string = strcat('$\psi_{',firstIndex,',',secondIndex,'}$');
end