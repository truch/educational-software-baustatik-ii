function result = findpsi(handles)
%Initialisierung
k = 1;
current = 1;
r = handles.points.point1.r;
if isfield(handles.points.point1,'pvec')
pvec = handles.points.point1.pvec;
tvec = handles.points.point1.tvec;
else
    pvec = [];
    tvec = [];
end
matching_p = pvec;
matching_t = tvec;
neighbours = handles.points.point1.neighbours;
if not(isempty(pvec))
nlines = numel(pvec(1,:));
else
    nlines = 0;
end
if nlines >= 2
    handles.points.(pointname(current)).newfix = false;
    handles.points.(pointname(current)).fix = true;
end
for i = numel(neighbours)
    if isfield(handles.points.(pointname(neighbours(i))),'pvec')
    pvecn = handles.points.(pointname(neighbours(i))).pvec;
    tvecn = handles.points.(pointname(neighbours(i))).tvec;
    for j = 1 : numel(pvecn(1,:))
        if isonline(pvecn(:,j),tvecn(:,j),r)
            if not(isempty(matching_t))
            for x = 1 : numel(matching_t(1,:))
                foundcollinear = false;
                if iscollinear(matching_t(:,x),tvecn(:,j))
                    foundcollinear = true;
                    break;
                end
            end
            else
                foundcollinear = false;
            end
                if not(foundcollinear)
                nlines = nlines + 1;
                matching_p = [matching_p pvecn(:,j)];
                matching_t = [matching_t tvecn(:,j)];
                end
        end
    end
    end
end
nlines = nlines + otherline(k,handles); %looking for hidden locklines at initialisation
if nlines >= 2
    handles.points.point1.fix = true;
    if not(isfield(handles.points.(pointname(current)),'newfix')) || not(handles.points.(pointname(current)).newfix)
            handles.points.(pointname(current)).newfix = true; %New fixpoint fond.
            %Create new lockline in the direction of all of the proceeding
            %beams if not already existing.
        for o = 1 : numel(neighbours)
            if isfield(handles.points.(pointname(neighbours(o))),'tvec')
            for p = 1 : numel(handles.points.(pointname(neighbours(o))).tvec(1,:))
                stop = false;
            if iscollinear(handles.beams.(beamname(current,neighbours(o))).t, handles.points.(pointname(neighbours(o))).tvec(:,p))
                stop = true;
                break;
            end
            end
            else
                stop = false;
            end
            if isfield(handles.points.(pointname(current)),'tvec')
            for p = 1 : numel(handles.points.(pointname(current)).tvec(1,:))
                stop2 = false;
                if iscollinear(handles.beams.(beamname(current,neighbours(o))).t, handles.points.(pointname(current)).tvec(:,p))
                stop2 = true;
                break;
                end
            end
            else
                stop2 = false;
            end

            if not(stop) && not(stop2)
                if isfield(handles.points.(pointname(current)),'pvec')
                handles.points.(pointname(current)).pvec = [handles.points.(pointname(current)).pvec r];
                handles.points.(pointname(current)).tvec = [handles.points.(pointname(current)).tvec handles.beams.(beamname(current,neighbours(o))).t];
                else
                handles.points.(pointname(current)).pvec = r;
                handles.points.(pointname(current)).tvec = handles.beams.(beamname(current,neighbours(o))).t;
                end
            end
        end
        end
elseif nlines == 1 || nlines == 0
    handles.points.point1.fix = false;
end


handles = findotherpsi(k,matching_p,matching_t,handles);

    % go back along the same path through the system after the recursion
    % 1) on how many locklines is "current" now?
    for s = 1 : numel(neighbours)
        if neighbours(s)~=k
        if isfield(handles.points.(pointname(neighbours(s))),'pvec')
        for d = 1 : numel(handles.points.(pointname(neighbours(s))).pvec(1,:))

            if handles.points.(pointname(neighbours(s))).newfix 
                pvecn = handles.points.(pointname(neighbours(s))).pvec;
                tvecn = handles.points.(pointname(neighbours(s))).tvec;
                if isonline(pvecn(:,d),tvecn(:,d),r)
                    if not(isempty(matching_t))
                    for x = 1 : numel(matching_t(1,:))
                        foundcollinear = false;
                        if iscollinear(matching_t(:,x),tvecn(:,d))
                            foundcollinear = true;
                            break;
                        end
                    end
                    else
                        foundcollinear = false;
                    end
                    if not(foundcollinear)
                    nlines = nlines + 1;
                    matching_p = [matching_p pvecn(:,d)];
                    matching_t = [matching_t tvecn(:,d)];
                    end
                end
            end
        end
        end
        end
    end
        % 2) Resetting the psis:
        for s = 1 : numel(neighbours)
            if nlines >= 1
            if isfield(handles.points.(pointname(neighbours(s))),'newfix') && handles.points.(pointname(neighbours(s))).newfix
                handles.points.(pointname(current)).fix = true;
                handles.points.(pointname(current)).newfix = true;
                handles.beams.(beamname(current,neighbours(s))).haspsi = false;
            end
            if nlines == 1 %collinear roller or free end (psi exists again, but "current" remains fix.
            if not(isempty(matching_t)) && iscollinear(matching_t,handles.beams.(beamname(current,neighbours(s))).t)
                handles.beams.(beamname(current,neighbours(s))).haspsi = true;
            end
            end
            end
        end



result = handles;
end