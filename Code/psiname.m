function name = psiname(beam)
name = strcat('psi',num2str(beam.beampoints(1)),'x',num2str(beam.beampoints(2)));
end