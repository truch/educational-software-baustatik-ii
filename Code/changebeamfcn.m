function result = changebeamfcn(handles,beam)
%Allows to change the constraints of a beam.
%Set gui to these values
for i = 1 : numel(handles.beamnames)
    if strcmp(beam,char(handles.beamnames(i)))
        handles.temp = i;
    end
end
handles.beams.(beam).changethisbeam = true;
handles.pushbutton6.Visible = 'On';
if strcmp(handles.unit,'kN'), unitl = 'm';
        else, unitl = 'L'; end
        set(handles.text14, 'String', 'You are at beam:');
        set(handles.text15, 'String', num2str(handles.beams.(beam).beampoints(1)));
        set(handles.text23, 'Visible', 'On');
        set(handles.text36, 'Visible', 'On');
        set(handles.text25, 'String', num2str(handles.beams.(beam).beampoints(2)));
        set(handles.text37,'String', strcat(num2str(handles.beams.(beam).l),unitl));
        set(handles.edit13,'String',checkfracstring(handles.beams.(beam).EI));
%Set hinge dialog
        if handles.beams.(beam).hinge == -1
            set(handles.edit14,'String','');
            set(handles.edit14,'Visible','off');
            set(handles.radiobutton2,'Value',0);
            set(handles.text35,'Visible','off');
        else
            set(handles.edit14,'String',checkfracstring(handles.beams.(beam).hinge));
            set(handles.edit14,'Visible','on');
            set(handles.radiobutton2,'Value',1);
            set(handles.text35,'Visible','off');
        end
% Set Point load dialog
        if cell2mat(handles.beams.(beam).F(1,1)) == 0
            set(handles.edit26,'string','');
            tablesize = [{'0'} {'0'} {'0'}];
            set(handles.uitable5,'Data',tablesize);
            set(handles.text33,'Visible','On');
        else
            set(handles.edit26,'String',num2str(numel(handles.beams.(beam).F(:,1))))
            tablesize = handles.beams.(beam).F;
            for i = 1 : numel(tablesize)
                tablesize{i} = checkfracstring(tablesize{i});
            end
            set(handles.uitable5,'Data',tablesize);
            set(handles.text33,'Visible','Off');
        end
% Set distributed load dlg
        qangle = handles.beams.(beam).qangle;
        if dot([cosd(qangle) -sind(qangle);sind(qangle) cosd(qangle)]*handles.beams.(beam).n,[1;0])==0 %q is vertical
        set(handles.radiobutton4,'Value',0);
        set(handles.radiobutton5,'Value',1);
        set(handles.radiobutton6,'Value',0);
        elseif dot([cosd(qangle) -sind(qangle);sind(qangle) cosd(qangle)]*handles.beams.(beam).n,[0;1])==0 %q is horizontal
        set(handles.radiobutton4,'Value',0);
        set(handles.radiobutton5,'Value',0);
        set(handles.radiobutton6,'Value',1);
        else %q is normal to the beam
        set(handles.radiobutton4,'Value',1);
        set(handles.radiobutton5,'Value',0);
        set(handles.radiobutton6,'Value',0);
        end
  
        set(handles.edit18,'String',checkfracstring(handles.beams.(beam).q1(1)));
        set(handles.edit19,'String',checkfracstring(handles.beams.(beam).q1(2)));
        set(handles.edit21,'String',checkfracstring(handles.beams.(beam).q2(1)));
        if handles.beams.(beam).q2(2) == handles.beams.(beam).l
            set(handles.edit22,'String','L');
        else
            set(handles.edit22,'String',checkfracstring(handles.beams.(beam).q2(2)));
        end
        
% Moment dlg
        set(handles.edit24,'String',checkfracstring(handles.beams.(beam).M(1)));
        set(handles.edit25,'String',checkfracstring(handles.beams.(beam).M(2)));
        set(handles.edit27,'String',checkfracstring(handles.beams.(beam).psi));    

%Plot normal Vector of the first beam
r1=handles.points.(pointname(handles.beams.(beam).beampoints(1))).r(1);
r2=handles.points.(pointname(handles.beams.(beam).beampoints(1))).r(2);
pn1 = handles.beams.(beam).n(1)*0.2*handles.imscale^0.8;
pn2 = handles.beams.(beam).n(2)*0.2*(handles.imscale)^0.8;
handles.nimg=quiver(r1-pn1/2,r2-pn2/2,1.5*pn1,1.5*pn2,'k');
handles.ntext=text(r1+pn1+0.05*handles.imscale^0.8,r2+pn2+0.05*(handles.imscale)^0.8,'n');

%Plot tangential vector of the first beam
pt1 = handles.beams.(beam).t(1)*0.2*handles.imscale^0.8;
pt2 = handles.beams.(beam).t(2)*0.2*handles.imscale^0.8;
handles.timg=quiver(r1,r2,pt1,pt2,'k');
handles.ttext=text(r1+pt1+0.05*handles.imscale^0.8,r2+pt2+0.05*(handles.imscale)^0.8,'t');


%plot positive direction of the angle (semi-circle)
    %Find the starting angle (alpha(x direction)=0)
    if pn1 >= 0 && pn2 >= 0             %first sector
        alpha = atand(pt2/pt1);
    elseif pn1 < 0 && pn2 >= 0
        alpha = 180 + atand(pt2/pt1);   %second sector
    elseif pn1 < 0 && pn2 < 0
        alpha = 180 + atand(pt2/pt1);   %third sector
    elseif pn1 >= 0 && pn2 <= 0
        alpha = 360 + atand(pt2/pt1);   %fourth sector
    end
    
    %prepare the circle
    th = linspace( alpha - 90, alpha + 90, 100);
    R = 0.25*norm([pn1;pn2]);
    x = -R*cosd(th) + r1;
    y = -R*sind(th) + r2;
    
    %prepare the arrow
    mpoly = [r1;r2] + [0.25*pn1; 0.25*pn2];
    spoly = mpoly + 0.4*R*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*[-1/sqrt(2);-1/sqrt(2)];
    epoly = mpoly + 0.4*R*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*[-1/sqrt(2);1/sqrt(2)];
    px=[spoly(1),mpoly(1),epoly(1)];
    py=[spoly(2),mpoly(2),epoly(2)];
    handles.acircplot = plot(x,y,'k');
    handles.apolyplot =  plot(px,py,'k');
    handles.atext = text(r1 - 0.5*pt1 ,r2 - 0.5*pt2, '\alpha');

%ALSO DELETE ALL GRAPHICS AND RESET BEAM TO STATUS BEFORE PUSHBUTTON6 AT
%THE BEGINNING OF PUSHBUTTON6 IF HANDLES.CHANGEBEAM
%folgendes alles in pushbutton6 einbauen. Aufpassen handles.temp

result = handles;
end