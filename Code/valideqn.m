function eqn = valideqn(struct)
fn = fieldnames(struct);
eqn = [];
syms x
for i = 1 : numel(fn)
    for j = 1 : numel(struct.(char(fn(i))))
        if struct.(char(fn(i)))(j) ~= (0*x == 0)
        if not(isempty(eqn))
        for k = 1 : numel(eqn)
            found = false;
            if struct.(char(fn(i)))(j) == eqn(k)
                found = true;
                break;
            end
        end
        if not(found)
            eqn = [eqn; struct.(char(fn(i)))(j)];
        end
        else
            eqn = struct.(char(fn(i)))(j);
        end
        end
    end
end   
end