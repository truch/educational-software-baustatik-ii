function n = otherline(k,handles) %not easy do detect other lockline
n = 0;
neighbours = handles.points.(pointname(k)).neighbours;
for i = 1 : numel(neighbours)
    tki = handles.beams.(beamname(k,neighbours(i))).t;
    n = n + checkotherline(neighbours,i,tki,k,handles,n);
end