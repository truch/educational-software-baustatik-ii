%testscript
    pointnrstr = '1';
    pointnrvalue = str2double(pointnrstr);
    handles.totalpoints=pointnrvalue;
    pointstruct = point1;
    pointnr = 'pointnr';
    rvalue= [1;1];
    r='r';
    neighboursvalue = [2];
    neighbours = 'neighbours';
    constrainttypevalue = 'pin';
    constrainttype = 'constrainttype';
    if strcmp(constrainttype,'roller')
        constraintanglevalue = str2double(get(handles.edit8,'string'));
    else constraintanglevalue = 0;
    end
    constraintangle = 'constraintangle';
    knottypevalue = 'hinge';
    knottype = 'knottype';
    Fvalue = [0;0];
    F='F';
    Mvalue = 0;
    M='M';
    phivalue = 0;
    phi='phi';
    point=struct(pointnr,pointnrvalue,r,rvalue,neighbours,neighboursvalue,constrainttype,constrainttypevalue,constraintangle,constraintanglevalue,knottype,knottypevalue,F,Fvalue,M,Mvalue,phi,phivalue);
    handles.(pointstruct)=point;
% second
    pointnrstr = '2';
    pointnrvalue = str2double(pointnrstr);
    handles.totalpoints=pointnrvalue;
    pointstruct = point2;
    pointnr = 'pointnr';
    rvalue= [4;1];
    r='r';
    neighboursvalue = [1 3];
    neighbours = 'neighbours';
    constrainttypevalue = 'pin';
    constrainttype = 'constrainttype';
    if strcmp(constrainttype,'roller')
        constraintanglevalue = str2double(get(handles.edit8,'string'));
    else constraintanglevalue = 0;
    end
    constraintangle = 'constraintangle';
    knottypevalue = 'connection';
    knottype = 'knottype';
    Fvalue = [0;0];
    F='F';
    Mvalue = 0;
    M='M';
    phivalue = 0;
    phi='phi';
    point=struct(pointnr,pointnrvalue,r,rvalue,neighbours,neighboursvalue,constrainttype,constrainttypevalue,constraintangle,constraintanglevalue,knottype,knottypevalue,F,Fvalue,M,Mvalue,phi,phivalue);
    handles.(pointstruct)=point;
% point 3
pointnrstr = '3';
    pointnrvalue = str2double(pointnrstr);
    handles.totalpoints=pointnrvalue;
    pointstruct = point3;
    pointnr = 'pointnr';
    rvalue= [4;1];
    r='r';
    neighboursvalue = [1 3];
    neighbours = 'neighbours';
    constrainttypevalue = 'pin';
    constrainttype = 'constrainttype';
    if strcmp(constrainttype,'roller')
        constraintanglevalue = str2double(get(handles.edit8,'string'));
    else constraintanglevalue = 0;
    end
    constraintangle = 'constraintangle';
    knottypevalue = 'connection';
    knottype = 'knottype';
    Fvalue = [0;0];
    F='F';
    Mvalue = 0;
    M='M';
    phivalue = 0;
    phi='phi';
    point=struct(pointnr,pointnrvalue,r,rvalue,neighbours,neighboursvalue,constrainttype,constrainttypevalue,constraintangle,constraintanglevalue,knottype,knottypevalue,F,Fvalue,M,Mvalue,phi,phivalue);
    handles.(pointstruct)=point;