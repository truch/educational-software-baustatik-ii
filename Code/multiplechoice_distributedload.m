function h = multiplechoice_distributedload(Fig, h, M)

set(0, 'currentfigure', Fig);

l = h.beam.l;
pa = pointname(h.beam.beampoints(1));
pb = pointname(h.beam.beampoints(2));
ca = h.S.newconstrainta;
cb = h.S.newconstraintb;
angleroller_a = h.points.(pa).constraintangle;
angleroller_b = h.points.(pb).constraintangle;
t = [1;0];
qaold = h.beam.q1(1);
qbold = h.beam.q2(1);
qangle = h.beam.qangle;
qa = qaold*cosd(qangle);
qb = qbold*cosd(qangle);
sa = h.beam.q1(2);
sb = h.beam.q2(2);
syms x
M(x) = M;
if numel(h.S.xmaxmin0) > 1, highlows = true; else,  highlows = false; end
xmaxmin = h.S.xmaxmin0(1);
sh = h.S.sh;

colorM = [0.5 0.8 1];

heightgrafic = 65/214*h.screendim(4) - 15240/107;
widthgrafic = 15/134*h.screendim(3) + 540/67;
left = h.graficpanel.Position(3)/2-widthgrafic/2;
height = h.graficpanel.Position(4);
                       
a = [1, 2, 3];
a = a(randperm(length(a)));
b = 0.09*h.screendim(4);
pos1 = height-heightgrafic*a(1)-20*a(1)-b;
pos2 = height-heightgrafic*a(2)-20*a(2)-b;
pos3 = height-heightgrafic*a(3)-20*a(3)-b;

h.multiplechoice.txt = uicontrol('Parent',h.graficpanel,...
       'Style','text',...
       'Position',[20 height-35 300 20],...
       'String','Choose the right moment line:',...
        'HorizontalAlignment','left',...
        'FontSize',12,...
        'FontName','Calibri',...
        'FontWeight','bold',...
        'Background',h.graficpanel.BackgroundColor);
   
h.multiplechoice1 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos1+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice2 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos2+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice3 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos3+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);


    N(x) = -M(x);
    P(x) = diff(M(x));

    if qa == 0 && qb == 0 % if projection of load is = 0 (no moment) --> this generates a moment as a parable
        q = max(abs(qaold), abs(qbold));
        R(x) = q*(x-sa);
        Va = R(sb)*(l-sa-(sb-sa)/2)/l;
        Vb = R(sb) - Va;
        N(x) = piecewise(x>=0 & x<sa, Va*x, x>=sa & x<sb, Va*x - R(x)*(x-sa)/2, x>=sb & x<=l, Vb*(l-sb) - Vb*(x-sb));
        P(x) = piecewise(x>=0 & x<sa, 0, x>=sa & x<sb, q, x>=sb & x<=l, 0);
    elseif strcmp(ca, 'none') || (strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t)) && randi([1, 2], 1) == 1
            N(x) = M(x) + piecewise(x>=0 & x<=l, -M(l));
    elseif strcmp(cb, 'none') || (strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t)) && randi([1, 2], 1) == 1
            N(x) = M(x) + piecewise(x>=0 & x<=l, -M(0));
    elseif sh > 0 && sh ~= l/2 && randi([1, 2], 1) == 1
        P(x) = solvemoment(l,zeros(3, 3),sa,sb,qaold,qbold,0,0,'hinge','hinge');
    elseif sign(qaold) ~= sign(qbold) && sign(qaold) ~= 0 && sign(qbold) ~= 0
        diagr = randi([1, 3], 1);
        root = solve(M(x) == 0);
        if diagr == 1 && numel(root) > 2
            P(x) = piecewise(x>=0 & x<root(2), qaold, x>=root(2) & x<=l, qbold);
        elseif diagr == 2 || (diagr == 1 && numel(root) <= 2)
            equ(x) = qaold + (qbold-qaold)/l*x;
            zero = solve(equ(x) == 0);
            if strcmp(ca,'fixed') || strcmp(ca,'torsional spring')
                [P2(x), Va] = solvemoment(l-zero,zeros(3, 3),zero,sb-zero,0,qbold,0,0,'hinge','hinge');
                P1(x) = solvemoment(zero,[zeros(2, 3); [Va, zero, 0]],sa,zero,qaold,0,0,0,'fixed','free');
                P(x) = piecewise(x>=0 & x<zero, P1(x), x>=zero & x<=l, P2(x-zero));
            elseif strcmp(cb,'fixed') || strcmp(cb,'torsional spring')
                [P1(x), ~, Vb] = solvemoment(zero,zeros(3, 3),sa,zero,qaold,0,0,0,'hinge','hinge');
                P2(x) = solvemoment(l-zero,[zeros(2, 3); [Vb, 0, 0]],zero,sb-zero,0,qbold,0,0,'free','fixed');
                P(x) = piecewise(x>=0 & x<zero, P1(x), x>=zero & x<=l, P2(x-zero));
            end
        end        
    end    
                                       
% GRAFIC 1
% generate false answers
        h.multiplechoice.grafic1 = axes('Parent', h.graficpanel,'units','pixels');
        h.multiplechoice.grafic1.Position = [left, pos1+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic1,'xtick',[]);
        set(h.multiplechoice.grafic1,'ytick',[]);
        set(h.multiplechoice.grafic1,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic1,'ycolor',[0.9294    0.9529    0.9882]);
        hold on;        
                
        fplot(h.multiplechoice.grafic1,-N(x),[0,l],'color',colorM,'Linewidth',2);         
        fplot(h.multiplechoice.grafic1, 0, [0,l],'-k'); %line at x=0 to distinguish
        if isnan(N(0)), border0 = -N(0+1/10^10); else, border0 = -N(0); end
        if isnan(N(l)), borderl = -N(l-1/10^10); else, borderl = -N(l); end
        if double(N(0)) ~= 0, plot(h.multiplechoice.grafic1, [0,0],[0,border0],'color',colorM,'Linewidth',2); end
        if double(N(l)) ~= 0, plot(h.multiplechoice.grafic1, [l,l],[borderl,0],'color',colorM,'Linewidth',2); end
        h.mc.help1 = [];
        if highlows
            h.multiplechoice.grafic1.YLim = [-double(max(N(solve(diff(N(x),x)==0)))), -double(min(N(solve(diff(N(x),x)==0))))]; 
        end

% GRAFIC 2           
% generate false answers     
        h.multiplechoice.grafic2 = axes('Parent', h.graficpanel, 'units','pixels');
        h.multiplechoice.grafic2.Position = [left, pos2+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic2,'xtick',[]);
        set(h.multiplechoice.grafic2,'ytick',[]);
        set(h.multiplechoice.grafic2,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic2,'ycolor',[0.9294    0.9529    0.9882]);
        hold on;
        
        fplot(h.multiplechoice.grafic2, -P(x),[0,l],'color',colorM,'Linewidth',2); 
        fplot(h.multiplechoice.grafic2, 0, [0,l],'-k'); %line at x=0 to distinguish        
        if isnan(P(0)), border0 = -P(0+1/10^10); else, border0 = -P(0); end
        if isnan(P(l)), borderl = -P(l-1/10^10); else, borderl = -P(l); end
        if double(P(0)) ~= 0, plot(h.multiplechoice.grafic2, [0,0],[0,border0],'color',colorM,'Linewidth',2); end
        if double(P(l)) ~= 0, plot(h.multiplechoice.grafic2, [l,l],[borderl,0],'color',colorM,'Linewidth',2); end
        h.mc.help2 = [];
        if highlows
            limit = [-double(max(P(solve(diff(P(x),x)==0,x)))), -double(min(P(solve(diff(P(x),x)==0,x))))];
            if ~isempty(limit) && limit(1) ~= limit(2)
                h.multiplechoice.grafic2.YLim =  limit;
            end
        end

% GRAFIC 3 (the right one)       
        h.multiplechoice.grafic3 = axes('Parent', h.graficpanel, 'units','pixels');
        h.multiplechoice.grafic3.Position = [left, pos3+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic3,'xtick',[]);
        set(h.multiplechoice.grafic3,'ytick',[]);
        set(h.multiplechoice.grafic3,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic3,'ycolor',[0.9294    0.9529    0.9882]);  
        hold on;
       
        fplot(h.multiplechoice.grafic3,-(M(x)),[0,l],'color',colorM,'Linewidth',2); 
        fplot(h.multiplechoice.grafic3, 0, [0,l],'-k'); %line at x=0 to distinguish 
        if M(0) ~= 0, plot(h.multiplechoice.grafic3, [0,0],[0,-M(0)],'color',colorM,'Linewidth',2); end
        if M(l) ~= 0, plot(h.multiplechoice.grafic3, [l,l],[-M(l),0],'color',colorM,'Linewidth',2); end
                
end
