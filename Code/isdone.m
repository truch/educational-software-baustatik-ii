function logic = isdone(beamname, handles)
if not(isempty(handles.beamdone))
for j = 1 : numel(handles.beamdone)
    logic = false;
    if strcmp(beamname,handles.beamdone(j))
        logic = true;
        break;
    end
end
else
    logic = false;
end
end