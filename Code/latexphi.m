function string = latexphi(input)
index = input(4 : numel(input));
string = strcat('$\phi_{',index,'}$');
end