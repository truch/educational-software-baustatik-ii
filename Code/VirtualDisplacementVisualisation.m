function [h,Fig] = VirtualDisplacementVisualisation(var,handles,h,Fig)
%This function visualises the virtual degree of displacement related to var
%The whole system is copied to a new figure and the virtual displacement of
%the rigid beams is plotted.
set(getpoints,'currentaxes',handles.axes1);
scale = handles.imscale/20;
for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','off');
        end
    end
    handles.points.(point).new_r = handles.points.(point).r + handles.points.(point).vdisp*scale;
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') + handles.points.(point).vdisp(1)*scale);
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') + handles.points.(point).vdisp(2)*scale);  
    set(handles.points.(point).textbox,'Color',[0.85,0.85,0.85]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','Off');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','off');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    pleft_r = handles.points.(pointname(pleft_nr)).new_r;
    pright_r = handles.points.(pointname(pright_nr)).new_r;
    handles.beams.(beam).displaced = ...
        plot([pleft_r(1),pright_r(1)],[pleft_r(2),pright_r(2)],'Color',[0 0 0],'LineWidth',handles.beamwidth);
    set(handles.beams.(beam).image,'Color',[0.85,0.85,0.85]);
    set(handles.beams.(beam).dashes,'Color',[0.85,0.85,0.85]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') + [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') + [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);
    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') + [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') + [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end
    xpos = handles.axes1.XLim;
    xpos = xpos(1);
    ypos = handles.axes1.YLim;
    ypos = ypos(2);
handles.mechimtitle = text(xpos,ypos,strcat('Virtual mechanism belonging to',{' '},latexpsi(char(string(var)))),'HorizontalAlignment','Left','VerticalAlignment','top','Interpreter','Latex');
    
    h.number = 1 + h.number;
    h.name = strcat('image',num2str(h.number)); 
    h.obj.(h.name) = copyobj(handles.axes1,Fig);
    hold on

    h.obj.(h.name).Units = 'Pixels';
    h.obj.(h.name).Parent = h.(h.panel);
    width = h.width - 40;
    height = 300;
    distfromprev = 10;
    distfromnext = 10;
    h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
    bottom = h.obj.(h.name).Position(2);

%figure (handles.DisplVisFignr)

%set(getpoints,'currentaxes',handles.axes1);

set(handles.mechimtitle,'Visible','Off')

for i = 1 : numel(handles.pointnames)
    point = char(handles.pointnames(i));
    if isfield(handles.points.(point),'loads')
        loadnames = fieldnames(handles.points.(point).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.points.(point).loads.(currentload),'Visible','on');
        end
    end
    delete(handles.points.(point).new_r);
    set(handles.points.(point).pointmarker,'XData',get(handles.points.(point).pointmarker,'XData') - handles.points.(point).vdisp(1)*scale);
    set(handles.points.(point).pointmarker,'YData',get(handles.points.(point).pointmarker,'YData') - handles.points.(point).vdisp(2)*scale);
    set(handles.points.(point).textbox,'Color',[0 0 0]);
    if isfield(handles.points.(point),'stiffnesstext')
    set(handles.points.(point).stiffnesstext,'Visible','On');
    end
end

for i = 1 : numel(handles.beamnames)
    beam = char(handles.beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        loadnames = fieldnames(handles.beams.(beam).loads);
        for j = 1 : numel(loadnames)
            currentload = char(loadnames(j));
            set(handles.beams.(beam).loads.(currentload),'Visible','on');
        end
    end
    pleft_nr = handles.beams.(beam).beampoints(1);
    pright_nr = handles.beams.(beam).beampoints(2);
    delete(handles.beams.(beam).displaced);
    set(handles.beams.(beam).image,'Color',[0,0,0]);
    set(handles.beams.(beam).dashes,'Color',[0,0,0]);
    if isfield(handles.beams.(beam),'leftim')
        set(handles.beams.(beam).leftim,'XData',get(handles.beams.(beam).leftim,'Xdata') - [double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale double(handles.points.(pointname(pleft_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).leftim,'YData',get(handles.beams.(beam).leftim,'Ydata') - [double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale double(handles.points.(pointname(pleft_nr)).vdisp(2))*scale]);

    end
    if isfield(handles.beams.(beam),'rightim')
        set(handles.beams.(beam).rightim,'XData',get(handles.beams.(beam).rightim,'Xdata') - [double(handles.points.(pointname(pright_nr)).vdisp(1))*scale double(handles.points.(pointname(pright_nr)).vdisp(1))*scale]);
        set(handles.beams.(beam).rightim,'YData',get(handles.beams.(beam).rightim,'Ydata') - [double(handles.points.(pointname(pright_nr)).vdisp(2))*scale double(handles.points.(pointname(pright_nr)).vdisp(2))*scale]);
    end    
end

if bottom < 5
    set(h.(h.panel).Children,'Visible','Off');
    set(h.(h.panel),'Visible','Off');
    h.panelnr = h.panelnr + 1;
    h.panel = strcat('panel',num2str(h.panelnr));
    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white','Parent',Fig); %[0.9, 0.9, 0.9]);
    h.lastnronpage = h.number - 1;
    h.obj.(h.name).Parent = h.(h.panel);
    
    h.obj.(h.name).Position(2) = h.height - height - 20;
else
    h.obj.(h.name).Position(2) = h.lastbottompos - distfromprev - height;
end
    h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
end