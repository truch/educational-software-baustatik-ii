function result = unitangle(value,unit_in)
%Approximates the value of the angle and adds, in case of too small values
%the unit of mrad or micro-rad
if strcmp(unit_in,'kN'), unit = '\mathrm{rad}';
elseif strcmp(unit_in,'q'), unit = '\mathrm{\frac{ql^3}{EI}}';
elseif strcmp(unit_in,'Q'), unit = '\mathrm{\frac{Ql^2}{EI}}';
else unit = '\mathrm{\frac{Ml}{EI}}'; end

if contains(checkfrac(value), 'frac') || value == 0
    result = strcat(checkfrac(value), {' '}, unit);
else    
    if abs(double(value)) < 1/10
        newval = value*1000;
        if strcmp(unit_in,'kN'), unit = '\mathrm{mrad}';
        elseif strcmp(unit_in,'q'), unit = '\cdot{}10^{-3}\mathrm{\frac{ql^3}{EI}}';
        elseif strcmp(unit_in,'Q'), unit = '\cdot{}10^{-3}\mathrm{\frac{Ql^2}{EI}}';
        else unit = '\cdot{}10^{-3}\mathrm{\frac{Ml}{EI}}'; end
        if abs(newval) < 1/10
            newval = newval*1000;
            if strcmp(unit_in,'kN'), unit = '\mathrm{\mu rad}';
            elseif strcmp(unit_in,'q'), unit = '\cdot{}10^{-6}\mathrm{\frac{ql^3}{EI}}';
            elseif strcmp(unit_in,'Q'), unit = '\cdot{}10^{-6}\mathrm{\frac{Ql^2}{EI}}';
            else unit = '\cdot{}10^{-6}\mathrm{\frac{Ml}{EI}}'; end
        end
        result = strcat(num2str(round(double(newval),4)), {' '}, unit);
    else
    result = strcat(num2str(round(double(value),4)), {' '}, unit);
    end
end
end